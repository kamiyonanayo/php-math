<?php

declare(strict_types=1);

namespace Kamiyonanayo\Math;

class UnitTestHelper
{
    public static function getCsvData(string $path): array
    {
        $ret = [];
        $handle = fopen($path, "r");
        if ($handle !== false) {
            for (;;) {
                $data = self::_fgetcsv($handle);
                if ($data === false) {
                    break;
                }
                $ret[] = $data;
            }
        }

        return $ret;
    }

    private static function _fgetcsv($handle, $length = 0, $delimiter = ',', $enclosure = '"', $escape = '')
    {
        if (PHP_VERSION_ID >= 70400) {
            return fgetcsv($handle, $length, $delimiter, $enclosure, $escape);
        } else {
            return fgetcsv($handle, $length, $delimiter, $enclosure);
        }
    }

    public static function newInstance($cls, array $args = [])
    {
        $c = new \ReflectionClass($cls);
        $obj = $c->newInstanceWithoutConstructor();
        if ($c->hasMethod("__construct")) {
            $method = $c->getMethod("__construct");
            $method->setAccessible(true);
            $method->invokeArgs($obj, $args);
        }
        return $obj;
    }

    public static function getProperty($cls, string $name): \ReflectionProperty
    {
        $cls = new \ReflectionClass($cls);
        $prop = $cls->getProperty($name);
        $prop->setAccessible(true);
        return $prop;
    }

    public static function invokeMethod($cls, string $name, array $args = [])
    {
        $c = new \ReflectionClass($cls);
        $method = $c->getMethod($name);
        $method->setAccessible(true);
        return $method->invokeArgs(is_string($cls) ? null : $cls, $args);
    }

    public static function getPropValue($cls, string $name)
    {
        $prop = self::getProperty($cls, $name);
        return $prop->getValue(is_string($cls) ? null : $cls);
    }

    public static function setPropValue($cls, string $name, $value)
    {
        $prop = self::getProperty($cls, $name);
        $prop->setValue(is_string($cls) ? null : $cls, $value);
    }
}
