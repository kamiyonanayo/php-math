<?php

declare(strict_types=1);

namespace Kamiyonanayo\Math\Exception;

use Kamiyonanayo\Math\TestCaseBase;

class ArithmeticExceptionTest extends TestCaseBase
{
    /**
     * @covers \Kamiyonanayo\Math\Exception\ArithmeticException::RoundingNecessary
     */
    public function testRoundingNecessary()
    {
        $e = ArithmeticException::RoundingNecessary();
        $this->assertInstanceOf(ArithmeticException::class, $e);
        $this->assertSame("Rounding necessary", $e->getMessage());
    }

    /**
     * @covers \Kamiyonanayo\Math\Exception\ArithmeticException::DivisionUndefined
     */
    public function testDivisionUndefined()
    {
        $e = ArithmeticException::DivisionUndefined();
        $this->assertInstanceOf(ArithmeticException::class, $e);
        $this->assertSame("Division undefined", $e->getMessage());
    }

    /**
     * @covers \Kamiyonanayo\Math\Exception\ArithmeticException::DivisionByZero
     */
    public function testDivisionByZero()
    {
        $e = ArithmeticException::DivisionByZero();
        $this->assertInstanceOf(ArithmeticException::class, $e);
        $this->assertSame("Division by zero", $e->getMessage());
    }

}
