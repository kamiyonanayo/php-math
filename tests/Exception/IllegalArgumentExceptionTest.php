<?php

declare(strict_types=1);

namespace Kamiyonanayo\Math\Exception;

use Kamiyonanayo\Math\TestCaseBase;

class IllegalArgumentExceptionTest extends TestCaseBase
{

    /**
     * @covers \Kamiyonanayo\Math\Exception\IllegalArgumentException::InvalidContextErrorMode
     */
    public function testInvalidContextErrorMode()
    {
        $e = IllegalArgumentException::InvalidContextErrorMode(-99);
        $this->assertInstanceOf(IllegalArgumentException::class, $e);
        $this->assertSame('Invalid error mode "-99"', $e->getMessage());
    }

    /**
     * @covers \Kamiyonanayo\Math\Exception\IllegalArgumentException::InvalidRoundingMode
     */
    public function testInvalidRoundingMode()
    {
        $e = IllegalArgumentException::InvalidRoundingMode("abc");
        $this->assertInstanceOf(IllegalArgumentException::class, $e);
        $this->assertSame('Invalid rounding mode "abc"', $e->getMessage());
    }
}
