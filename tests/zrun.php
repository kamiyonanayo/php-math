<?php

// use Kamiyonanayo\Math\BigDecimal;
// use Kamiyonanayo\Math\Context;
// use Kamiyonanayo\Math\Internals\Helper;
// use Kamiyonanayo\Math\Numeric;
// use Kamiyonanayo\Math\RoundingMode;

use Brick\Math\BigDecimal as BrickMathBigDecimal;
use Kamiyonanayo\Math\BigDecimal;
use Kamiyonanayo\Math\BigDecimal as MathBigDecimal;
use Kamiyonanayo\Math\Internals\Helper;

require_once __DIR__ . "/../vendor/autoload.php";

// echo sprintf("%s:%s\n",__FILE__,__LINE__);

// $b1 = BigDecimal::valueOf("0");
// $b2 = BigDecimal::valueOf("0.00");

// var_dump($b1->equalsExact($b1));
// var_dump($b1->equalsExact($b2));

// var_dump(BigDecimal::valueOf("-0.10000000"));
var_dump(BigDecimal::valueOf("-0.010"));
var_dump(BigDecimal::valueOf("-0.010")->stripTrailingZeros());

