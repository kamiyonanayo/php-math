<?php

declare(strict_types=1);

namespace Kamiyonanayo\Math;

use Kamiyonanayo\Math\Exception\IllegalArgumentException;
use Kamiyonanayo\Math\Exception\MathRuntimeException;

class MathContextTest extends TestCaseBase
{
    protected function setUp(): void
    {
    }

    protected function tearDown(): void
    {
    }

    /**
     * @covers Kamiyonanayo\Math\MathContext::make
     * @covers Kamiyonanayo\Math\MathContext::__construct
     */
    public function testMake()
    {
        $context = MathContext::make();
        $this->assertSame(MathContext::DEFAULT_ERROR_MODE, UnitTestHelper::getPropValue($context, "error_mode"));
        $this->assertSame(null, UnitTestHelper::getPropValue($context, "last_exception"));

        foreach ([MathContext::ERROR_MODE_RETURN_NAN,MathContext::ERROR_MODE_RETURN_NULL,MathContext::ERROR_MODE_THROW_EXCEPTION] as $mode) {
            $context = MathContext::make($mode);
            $this->assertSame($mode, UnitTestHelper::getPropValue($context, "error_mode"));
            $this->assertSame(null, UnitTestHelper::getPropValue($context, "last_exception"));
        }

    }

    /**
     * @covers Kamiyonanayo\Math\MathContext::make
     * @covers Kamiyonanayo\Math\MathContext::__construct
     */
    public function testMakeIllegal()
    {
        $error_mode = 4;
        $this->expectExceptionObject(IllegalArgumentException::InvalidContextErrorMode($error_mode));
        MathContext::make($error_mode);
    }

    /**
     * @covers \Kamiyonanayo\Math\MathContext::setErrorMode
     */
    public function testSetErrorMode()
    {
        $context = MathContext::make();
        $prop = UnitTestHelper::getProperty($context, "error_mode");

        $this->assertSame(MathContext::DEFAULT_ERROR_MODE, $prop->getValue($context));

        $this->assertNull($context->setErrorMode(MathContext::ERROR_MODE_THROW_EXCEPTION));
        $this->assertSame(MathContext::ERROR_MODE_THROW_EXCEPTION, $prop->getValue($context));

        $this->assertNull($context->setErrorMode(MathContext::ERROR_MODE_RETURN_NULL));
        $this->assertSame(MathContext::ERROR_MODE_RETURN_NULL, $prop->getValue($context));

        $this->assertNull($context->setErrorMode(MathContext::ERROR_MODE_RETURN_NAN));
        $this->assertSame(MathContext::ERROR_MODE_RETURN_NAN, $prop->getValue($context));

        $this->assertNull($context->setErrorMode(MathContext::ERROR_MODE_RETURN_NULL));
        $this->assertSame(MathContext::ERROR_MODE_RETURN_NULL, $prop->getValue($context));

    }

    /**
     * @covers \Kamiyonanayo\Math\MathContext::setErrorMode
     */
    public function testSetErrorModeIllegal()
    {
        $context = MathContext::make();
        $error_mode = 4;
        $this->expectExceptionObject(IllegalArgumentException::InvalidContextErrorMode($error_mode));
        $this->assertNull($context->setErrorMode($error_mode));
    }

    /**
     * @covers \Kamiyonanayo\Math\MathContext::getErrorMode
     */
    public function testGetErrorMode()
    {

        $context = MathContext::make();
        $prop = UnitTestHelper::getProperty($context, "error_mode");

        $this->assertSame(MathContext::DEFAULT_ERROR_MODE, $context->getErrorMode());

        $prop->setValue($context, MathContext::ERROR_MODE_THROW_EXCEPTION);
        $this->assertSame(MathContext::ERROR_MODE_THROW_EXCEPTION, $context->getErrorMode());

        $prop->setValue($context, MathContext::ERROR_MODE_RETURN_NULL);
        $this->assertSame(MathContext::ERROR_MODE_RETURN_NULL, $context->getErrorMode());

        $prop->setValue($context, MathContext::ERROR_MODE_RETURN_NAN);
        $this->assertSame(MathContext::ERROR_MODE_RETURN_NAN, $context->getErrorMode());

    }

    /**
     * @covers \Kamiyonanayo\Math\MathContext::getLastError
     * @covers \Kamiyonanayo\Math\MathContext::getLastErrorMessage
     */
    public function testGetLastError()
    {

        $context = MathContext::make();
        $prop = UnitTestHelper::getProperty($context, "last_exception");

        $this->assertNull($context->getLastError());
        $this->assertSame("", $context->getLastErrorMessage());
        $prop->setValue($context, new MathRuntimeException("abc", 99));
        $ex = $context->getLastError();
        $this->assertInstanceOf(MathRuntimeException::class, $ex);
        $this->assertSame("abc", $ex->getMessage());
        $this->assertSame(99, $ex->getCode());
        $this->assertSame("abc", $context->getLastErrorMessage());
        $prop->setValue($context, null);
        $this->assertNull($context->getLastError());
        $this->assertSame("", $context->getLastErrorMessage());
    }

    /**
     * @covers \Kamiyonanayo\Math\MathContext::setError
     */
    public function testSetError()
    {
        $context = MathContext::make();
        $prop = UnitTestHelper::getProperty($context, "last_exception");

        $e = new MathRuntimeException("abc", 99);
        $this->assertNull($context->setError($e));
        $this->assertSame($e, $prop->getValue($context));
        $this->assertNull($context->setError(null));
        $this->assertSame(null, $prop->getValue($context));
    }

    /**
     * @covers \Kamiyonanayo\Math\MathContext::clearError
     */
    public function testClearError()
    {

        $context = MathContext::make();
        $prop = UnitTestHelper::getProperty($context, "last_exception");

        $e = new MathRuntimeException("abc", 99);
        $prop->setValue($context, $e);
        $this->assertSame($e, $prop->getValue($context));
        $this->assertNull($context->clearError());
        $this->assertSame(null, $prop->getValue($context));
    }
}
