<?php

declare(strict_types=1);

namespace Kamiyonanayo\Math;

class RoundingModeTest extends TestCaseBase
{

    public static function enumListDataProvider()
    {
        return [
            ["UNNECESSARY"],
            ["CEILING"],
            ["DOWN"],
            ["UP"],
            ["FLOOR"],
            ["HALF_UP"],
            ["HALF_DOWN"],
            ["HALF_EVEN"],
            ["HALF_ODD"],
            ["HALF_CEILING"],
            ["HALF_FLOOR"],
        ];
    }

    /**
     * @coversNothing
     */
    public function testConsts()
    {

        $expected  = [];
        foreach ($this->enumListDataProvider() as $v) {
            $value = $v[0];
            $expected[$value] = strtolower($value);
        }

        $cls = new \ReflectionClass(RoundingMode::class);
        $actual = $cls->getConstants();

        ksort($expected);
        ksort($actual);

        $this->assertSame($expected, $actual);
    }

    /**
     * @dataProvider enumListDataProvider
     * @covers \Kamiyonanayo\Math\RoundingMode::valueOf
     * @covers \Kamiyonanayo\Math\RoundingMode::__construct
     */
    public function testValueOf($value)
    {

        UnitTestHelper::setPropValue(RoundingMode::class, "instances", []);

        $r = RoundingMode::valueOf(strtolower($value));

        $this->assertInstanceOf(RoundingMode::class, $r);
        $this->assertSame(strtolower($value), $r->value());
        $this->assertNull(RoundingMode::valueOf($value));
        $this->assertSame($r, RoundingMode::valueOf($r));

    }

    /**
     * @dataProvider enumListDataProvider
     * @covers \Kamiyonanayo\Math\RoundingMode::UNNECESSARY
     * @covers \Kamiyonanayo\Math\RoundingMode::CEILING
     * @covers \Kamiyonanayo\Math\RoundingMode::DOWN
     * @covers \Kamiyonanayo\Math\RoundingMode::UP
     * @covers \Kamiyonanayo\Math\RoundingMode::FLOOR
     * @covers \Kamiyonanayo\Math\RoundingMode::HALF_UP
     * @covers \Kamiyonanayo\Math\RoundingMode::HALF_DOWN
     * @covers \Kamiyonanayo\Math\RoundingMode::HALF_EVEN
     * @covers \Kamiyonanayo\Math\RoundingMode::HALF_ODD
     * @covers \Kamiyonanayo\Math\RoundingMode::HALF_CEILING
     * @covers \Kamiyonanayo\Math\RoundingMode::HALF_FLOOR
     */
    public function testEnum($value)
    {
        $this->assertTrue(method_exists(RoundingMode::class, $value));
        /** @var RoundingMode */
        $obj = call_user_func([RoundingMode::class, $value]);
        $this->assertSame(strtolower($value), $obj->value());
    }

    /**
     * @dataProvider enumListDataProvider
     * @covers \Kamiyonanayo\Math\RoundingMode::__toString
     */
    public function testToString($value)
    {
        $this->assertSame(strtolower($value), (string)RoundingMode::valueOf(strtolower($value)));
    }

    /**
     * @dataProvider enumListDataProvider
     * @covers \Kamiyonanayo\Math\RoundingMode::value
     */
    public function testValue($value)
    {
        $r = RoundingMode::valueOf(strtolower($value));
        $this->assertSame(strtolower($value), $r->value());
    }
}
