<?php

declare(strict_types=1);

namespace Kamiyonanayo\Math\Traits;

class ComparisonTestComparable
{
    use Comparison;

    public $val;

    public function __construct($val)
    {
        $this->val = $val;
    }

    public function compareTo($that): ?int
    {
        if (is_null($this->val) || is_null($that)) {
            return null;
        }
        return $this->val <=> $that;
    }
}
