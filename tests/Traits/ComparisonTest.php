<?php

declare(strict_types=1);

namespace Kamiyonanayo\Math\Traits;

use Kamiyonanayo\Math\TestCaseBase;

class ComparisonTest extends TestCaseBase
{

    private static function make($val): ComparisonTestComparable
    {
        return new ComparisonTestComparable($val);
    }

    public static function equalToDataProvider()
    {
        return [
            [true, 1, 1],
            [false, 1, 2],
            [false, 2, 1],
            [false, 1, null],
            [false, null, 2],
            [false, null, null],
        ];
    }

    /**
     * @dataProvider equalToDataProvider
     * @covers \Kamiyonanayo\Math\Traits\Comparison::equalTo
     * @covers \Kamiyonanayo\Math\Traits\Comparison::eq
     */
    public function testEqualTo($expected, $op1, $op2)
    {
        $this->assertSame($expected, self::make($op1)->equalTo($op2));
        $this->assertSame($expected, self::make($op1)->eq($op2));
    }

    /**
     * @dataProvider equalToDataProvider
     * @covers \Kamiyonanayo\Math\Traits\Comparison::notEqualTo
     * @covers \Kamiyonanayo\Math\Traits\Comparison::ne
     */
    public function testNotEqualTo($expected, $op1, $op2)
    {
        $this->assertNotSame($expected, self::make($op1)->notEqualTo($op2));
        $this->assertNotSame($expected, self::make($op1)->ne($op2));
    }


    public static function compDataProvider()
    {
        return [
            // gt(>) ge(>=) lt(<) lte(<=)
            [[true, true, false, false], [2, 0]],
            [[true, true, false, false], [2, 1]],
            [[false, true, false, true], [2, 2]],
            [[false, false, true, true], [2, 3]],
            [[false, false, true, true], [2, 4]],
            [[false, false, true, true], [0, 2]],
            [[false, false, true, true], [1, 2]],
            [[false, true, false, true], [2, 2]],
            [[true, true, false, false], [3, 2]],
            [[true, true, false, false], [4, 2]],
            [[false, false, false, false], [null, 9]],
            [[false, false, false, false], [9, null]],
        ];
    }

    /**
     * @dataProvider compDataProvider
     * @covers \Kamiyonanayo\Math\Traits\Comparison::greaterThan
     * @covers \Kamiyonanayo\Math\Traits\Comparison::gt
     */
    public function testGreaterThan($expecteds, $ops)
    {
        $this->assertSame($expecteds[0], self::make($ops[0])->greaterThan($ops[1]));
        $this->assertSame($expecteds[0], self::make($ops[0])->gt($ops[1]));
    }

    /**
     * @dataProvider compDataProvider
     * @covers \Kamiyonanayo\Math\Traits\Comparison::greaterThanOrEqualTo
     * @covers \Kamiyonanayo\Math\Traits\Comparison::gte
     */
    public function testGreaterThanOrEqualTo($expecteds, $ops)
    {
        $this->assertSame($expecteds[1], self::make($ops[0])->greaterThanOrEqualTo($ops[1]));
        $this->assertSame($expecteds[1], self::make($ops[0])->gte($ops[1]));
    }

    /**
     * @dataProvider compDataProvider
     * @covers \Kamiyonanayo\Math\Traits\Comparison::lessThan
     * @covers \Kamiyonanayo\Math\Traits\Comparison::lt
     */
    public function testLessThan($expecteds, $ops)
    {
        $this->assertSame($expecteds[2], self::make($ops[0])->lessThan($ops[1]));
        $this->assertSame($expecteds[2], self::make($ops[0])->lt($ops[1]));
    }

    /**
     * @dataProvider compDataProvider
     * @covers \Kamiyonanayo\Math\Traits\Comparison::lessThanOrEqualTo
     * @covers \Kamiyonanayo\Math\Traits\Comparison::lte
     */
    public function testLessThanOrEqualTo($expecteds, $ops)
    {
        $this->assertSame($expecteds[3], self::make($ops[0])->lessThanOrEqualTo($ops[1]));
        $this->assertSame($expecteds[3], self::make($ops[0])->lte($ops[1]));
    }

    public static function betweenDataProvider()
    {
        return [
            // neq eq   a < b < c
            [[false, true], [1, 1, 1]],
            [[true, true], [1, 2, 3]],
            [[false, false], [3, 2, 2]],
            [[false, true], [1, 1, 2]],
            [[false, true], [2, 3, 3]],
            [[true, true], [1, 3, 6]],
            [[true, true], [2, 3, 6]],
            [[false, true], [3, 3, 6]],
            [[false, false], [4, 3, 6]],
            [[false, false], [5, 3, 6]],
            [[false, false], [6, 3, 6]],
            [[false, false], [0, 3, 0]],
            [[false, false], [0, 3, 1]],
            [[false, false], [0, 3, 2]],
            [[false, true], [0, 3, 3]],
            [[true, true], [0, 3, 4]],
            [[true, true], [0, 3, 5]],
            [[false, false], [null, 2, 3]],
            [[false, false], [1, 2, null]],
            [[false, false], [null, 2, null]],
            [[false, false], [1, null, 3]],
            [[false, false], [null, null, null]],
        ];
    }


    /**
     * @dataProvider betweenDataProvider
     * @covers \Kamiyonanayo\Math\Traits\Comparison::between
     * @covers \Kamiyonanayo\Math\Traits\Comparison::betweenExcluded
     * @covers \Kamiyonanayo\Math\Traits\Comparison::betweenIncluded
     */
    public function testBetween($expecteds, $ops)
    {
        list($min, $val, $max) = $ops;
        $this->assertSame($expecteds[0], self::make($val)->between($min, $max, false));
        $this->assertSame($expecteds[1], self::make($val)->between($min, $max, true));
        $this->assertSame($expecteds[0], self::make($val)->betweenExcluded($min, $max));
        $this->assertSame($expecteds[1], self::make($val)->betweenIncluded($min, $max));
    }
}
