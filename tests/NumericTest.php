<?php

declare(strict_types=1);

namespace Kamiyonanayo\Math;

use Kamiyonanayo\Math\Exception\MathRuntimeException;

class NumericTest extends TestCaseBase
{
    protected function setUp(): void
    {
        $this->clearContext();
    }
    protected function tearDown(): void
    {
        $this->clearContext();
    }

    /**
     * @covers \Kamiyonanayo\Math\Numeric::setContext
     */
    public function testSetContext()
    {

        UnitTestHelper::setPropValue(Numeric::class, "context", null);

        $context_a = MathContext::make(MathContext::ERROR_MODE_THROW_EXCEPTION);

        $context_1 = Numeric::getContext();

        $context_2 = Numeric::setContext($context_a);

        $this->assertSame($context_1, $context_2);

        $context_3 = Numeric::getContext();

        $this->assertSame($context_a, $context_3);

    }

    /**
     * @covers \Kamiyonanayo\Math\Numeric::getContext
     */
    public function testGetContext()
    {
        $prop = UnitTestHelper::getProperty(Numeric::class, "context");
        $this->assertNull($prop->getValue(null));
        $this->assertInstanceOf(MathContext::class, Numeric::getContext());
        $this->assertInstanceOf(MathContext::class, $prop->getValue(null));
    }

    /**
     * @covers \Kamiyonanayo\Math\Numeric::getLastError
     * @covers \Kamiyonanayo\Math\Numeric::getLastErrorMessage
     */
    public function testGetLastError()
    {

        UnitTestHelper::setPropValue(Numeric::class, "context", null);

        $this->assertNull(Numeric::getLastError());
        $this->assertSame("", Numeric::getLastErrorMessage());

        $e = new MathRuntimeException("abc", 99);
        Numeric::getContext()->setError($e);

        $ex = Numeric::getLastError();
        $this->assertInstanceOf(MathRuntimeException::class, $ex);
        $this->assertSame($e, $ex);
        $this->assertSame("abc", $ex->getMessage());
        $this->assertSame(99, $ex->getCode());
        $this->assertSame("abc", Numeric::getLastErrorMessage());

        Numeric::getContext()->setError(null);
        $this->assertNull(Numeric::getLastError());
        $this->assertSame("", Numeric::getLastErrorMessage());
    }

    /**
     * @covers \Kamiyonanayo\Math\Numeric::clearError
     */
    public function testClearError()
    {

        UnitTestHelper::setPropValue(Numeric::class, "context", null);

        $this->assertNull(Numeric::getLastError());
        $this->assertSame("", Numeric::getLastErrorMessage());

        $e = new MathRuntimeException("abc", 99);
        Numeric::getContext()->setError($e);

        $ex = Numeric::getLastError();
        $this->assertInstanceOf(MathRuntimeException::class, $ex);
        $this->assertSame($e, $ex);
        $this->assertSame("abc", $ex->getMessage());
        $this->assertSame(99, $ex->getCode());
        $this->assertSame("abc", Numeric::getLastErrorMessage());

        $this->assertNull(UnitTestHelper::invokeMethod(Numeric::class, "clearError", []));
        $this->assertNull(Numeric::getLastError());
        $this->assertSame("", Numeric::getLastErrorMessage());
    }

    /**
     * @covers \Kamiyonanayo\Math\Numeric::save_context
     */
    public function testSave_context()
    {

        $e = new MathRuntimeException("abc", 99);
        $context = Numeric::getContext();
        $context->setErrorMode(MathContext::ERROR_MODE_RETURN_NULL);
        $context->setError($e);

        $ex = Numeric::getLastError();
        $this->assertSame($e, $ex);

        $ret = Numeric::save_context(function () use ($context) {
            $context_in = Numeric::getContext();
            $this->assertNull($context_in->getLastError());
            $this->assertSame($context, $context_in);
            $this->assertSame(MathContext::ERROR_MODE_RETURN_NULL, $context_in->getErrorMode());

            $context_in->setErrorMode(MathContext::ERROR_MODE_THROW_EXCEPTION);
            $new_context = MathContext::make(MathContext::ERROR_MODE_THROW_EXCEPTION);
            Numeric::setContext($new_context);

            return "!!!call_save_context!!!";

        });
        $this->assertSame("!!!call_save_context!!!", $ret);

        $context_r = Numeric::getContext();
        $this->assertNotSame($context, $context_r);
        $this->assertNull($context_r->getLastError());
        $this->assertSame(MathContext::ERROR_MODE_RETURN_NULL, $context_r->getErrorMode());
    }

    /**
     * @covers \Kamiyonanayo\Math\Numeric::save_context
     */
    public function testSave_context_throw()
    {

        $e = new MathRuntimeException("abc", 99);
        $context = Numeric::getContext();
        $context->setErrorMode(MathContext::ERROR_MODE_THROW_EXCEPTION);
        $context->setError($e);

        $ex = Numeric::getLastError();
        $this->assertSame($e, $ex);

        $throw_exception = new MathRuntimeException("abc", 99);

        try {
            Numeric::save_context(function () use ($context, $throw_exception) {
                $context_in = Numeric::getContext();
                $this->assertNull($context_in->getLastError());
                $this->assertSame($context, $context_in);
                $this->assertSame(MathContext::ERROR_MODE_THROW_EXCEPTION, $context_in->getErrorMode());

                $context_in->setErrorMode(MathContext::ERROR_MODE_RETURN_NULL);
                $new_context = MathContext::make(MathContext::ERROR_MODE_RETURN_NULL);
                Numeric::setContext($new_context);

                throw  $throw_exception;
            });
        } catch (MathRuntimeException $th) {
            $this->assertSame($throw_exception, $th);
        }

        $context_r = Numeric::getContext();
        $this->assertNotSame($context, $context_r);
        $this->assertNull($context_r->getLastError());
        $this->assertSame(MathContext::ERROR_MODE_THROW_EXCEPTION, $context_r->getErrorMode());

    }
}
