<?php

declare(strict_types=1);

namespace Kamiyonanayo\Math\Internals;

use Kamiyonanayo\Math\TestCaseBase;

class HelperTest extends TestCaseBase
{

    /**
     * @covers \Kamiyonanayo\Math\Internals\Helper::convertString
     * @covers \Kamiyonanayo\Math\Internals\Helper::float_to_string
     */
    public function testConvertString()
    {
        $this->assertSame("", Helper::convertString(""));
        $this->assertSame("", Helper::convertString(null));
        $this->assertSame("1", Helper::convertString("1"));
        $this->assertSame("0", Helper::convertString(0));
        $this->assertSame("0.1", Helper::convertString(0.1));
        $this->assertSame("1234.5", Helper::convertString(1234.5));
        $this->assertSame("1", Helper::convertString(true));
        $this->assertSame("", Helper::convertString(false));
        $this->assertSame("", Helper::convertString([]));
        $this->assertSame("", Helper::convertString((object)["a" => 1]));
        $this->assertSame("a123", Helper::convertString(new class
        {
            public function __toString()
            {
                return "a123";
            }
        }));
        $this->assertSame("10000000", Helper::convertString(10000000.0));
        $this->assertSame("1.0E-24", Helper::convertString(0.000000000000000000000001));
        $this->assertSame("1.234567E+29", Helper::convertString(123456700000000000000000000000.0));
        $this->assertSame("NAN", Helper::convertString(NAN));
        $this->assertSame("INF", Helper::convertString(INF));
        $this->assertSame("-INF", Helper::convertString(-INF));
    }

    /**
     * @dataProvider parseDataProvider
     * @covers \Kamiyonanayo\Math\Internals\Helper::parse
     */
    public function testParse($value, $expected)
    {
        $this->assertSame($expected, Helper::parse($value));
    }

    public static function parseDataProvider()
    {
        $datas = [

            // valid
            ["INF", ["scale" => 0, "value" => "INF",]],
            ["+INF", ["scale" => 0, "value" => "INF",]],
            ["-INF", ["scale" => 0, "value" => "-INF",]],
            ["NAN", ["scale" => 0, "value" => "NAN",]],
            ["1", ["scale" => 0, "value" => "1",]],
            ["+1", ["scale" => 0, "value" => "1",]],
            ["-1", ["scale" => 0, "value" => "-1",]],
            ["0.1", ["scale" => 1, "value" => "1",]],
            ["+0.1", ["scale" => 1, "value" => "1",]],
            ["-0.1", ["scale" => 1, "value" => "-1",]],
            ["1.23", ["scale" => 2, "value" => "123",]],
            ["+1.23", ["scale" => 2, "value" => "123",]],
            ["-1.23", ["scale" => 2, "value" => "-123",]],
            ["0.90", ["scale" => 2, "value" => "90",]],
            ["+0.90", ["scale" => 2, "value" => "90",]],
            [".90", ["scale" => 2, "value" => "90",]],
            ["+.90", ["scale" => 2, "value" => "90",]],
            ["-.90", ["scale" => 2, "value" => "-90",]],
            ["0.", ["scale" => 0, "value" => "0",]],
            ["+0.", ["scale" => 0, "value" => "0",]],
            ["-0.", ["scale" => 0, "value" => "0",]],
            ["+.0", ["scale" => 1, "value" => "0",]],
            ["-.0", ["scale" => 1, "value" => "0",]],
            ["1.", ["scale" => 0, "value" => "1",]],
            ["+1.", ["scale" => 0, "value" => "1",]],
            ["-1.", ["scale" => 0, "value" => "-1",]],
            ["-0.010", ["scale" => 3, "value" => "-10",]],
            ["0.000000000000000000000000000010000000", ["scale" => 36, "value" => "10000000",]],
            ["-0.000000000000000000000000000010000000", ["scale" => 36, "value" => "-10000000",]],

            // valid E notation
            ["1e10", ["scale" => -10, "value" => "1",]],
            ["-1E-10", ["scale" => 10, "value" => "-1",]],

            ["0e0", ["scale" => 0, "value" => "0",]],
            ["0e1", ["scale" => -1, "value" => "0",]],
            ["0E-1", ["scale" => 1, "value" => "0",]],

            [".0e+1", ["scale" => 0, "value" => "0",]],
            [".0e+0", ["scale" => 1, "value" => "0",]],
            [".0e-1", ["scale" => 2, "value" => "0",]],

            ["12e0", ["scale" => 0, "value" => "12",]],
            ["12e+0", ["scale" => 0, "value" => "12",]],
            ["1.2E0", ["scale" => 1, "value" => "12",]],
            ["1.2E+0", ["scale" => 1, "value" => "12",]],
            ["0.12E0", ["scale" => 2, "value" => "12",]],
            ["0.12E+0", ["scale" => 2, "value" => "12",]],
            ["-12e0", ["scale" => 0, "value" => "-12",]],
            ["-12e+0", ["scale" => 0, "value" => "-12",]],
            ["-1.2E0", ["scale" => 1, "value" => "-12",]],
            ["-1.2E+0", ["scale" => 1, "value" => "-12",]],
            ["-0.12E0", ["scale" => 2, "value" => "-12",]],
            ["-0.12E+0", ["scale" => 2, "value" => "-12",]],

            ["0.0E0", ["scale" => 1, "value" => "0",]],
            ["0.0E+0", ["scale" => 1, "value" => "0",]],
            ["0.0E-0", ["scale" => 1, "value" => "0",]],

            ["1E1", ["scale" => -1, "value" => "1",]],
            ["1E+1", ["scale" => -1, "value" => "1",]],
            ["1E-1", ["scale" => 1, "value" => "1",]],

            ["-1E1", ["scale" => -1, "value" => "-1",]],
            ["-1E+1", ["scale" => -1, "value" => "-1",]],
            ["-1E-1", ["scale" => 1, "value" => "-1",]],

            ["0.1E1", ["scale" => 0, "value" => "1",]],
            ["0.1E+1", ["scale" => 0, "value" => "1",]],
            ["0.1E-1", ["scale" => 2, "value" => "1",]],

            ["0.1E1", ["scale" => 0, "value" => "1",]],
            ["0.1E+1", ["scale" => 0, "value" => "1",]],
            ["0.1E-1", ["scale" => 2, "value" => "1",]],

            ["1.2E+4", ["scale" => -3, "value" => "12",]],
            ["1.2E+3", ["scale" => -2, "value" => "12",]],
            ["1.2E+2", ["scale" => -1, "value" => "12",]],
            ["1.2E+1", ["scale" => 0, "value" => "12",]],
            ["1.2E+0", ["scale" => 1, "value" => "12",]],
            ["1.2E-1", ["scale" => 2, "value" => "12",]],
            ["1.2E-2", ["scale" => 3, "value" => "12",]],
            ["1.2E-3", ["scale" => 4, "value" => "12",]],
            ["1.2E-4", ["scale" => 5, "value" => "12",]],

            ["-1.2E+4", ["scale" => -3, "value" => "-12",]],
            ["-1.2E+3", ["scale" => -2, "value" => "-12",]],
            ["-1.2E+2", ["scale" => -1, "value" => "-12",]],
            ["-1.2E+1", ["scale" => 0, "value" => "-12",]],
            ["-1.2E+0", ["scale" => 1, "value" => "-12",]],
            ["-1.2E-1", ["scale" => 2, "value" => "-12",]],
            ["-1.2E-2", ["scale" => 3, "value" => "-12",]],
            ["-1.2E-3", ["scale" => 4, "value" => "-12",]],
            ["-1.2E-4", ["scale" => 5, "value" => "-12",]],

            ["987E+4", ["scale" => -4, "value" => "987",]],
            ["987E+3", ["scale" => -3, "value" => "987",]],
            ["987E+2", ["scale" => -2, "value" => "987",]],
            ["987E+1", ["scale" => -1, "value" => "987",]],
            ["987E+0", ["scale" => 0, "value" => "987",]],
            ["987E-1", ["scale" => 1, "value" => "987",]],
            ["987E-2", ["scale" => 2, "value" => "987",]],
            ["987E-3", ["scale" => 3, "value" => "987",]],
            ["987E-4", ["scale" => 4, "value" => "987",]],

            ["0000.0000E+0", ["scale" => 4, "value" => "0",]],
            ["+0000.0000E+0", ["scale" => 4, "value" => "0",]],
            ["-0000.0000E+0", ["scale" => 4, "value" => "0",]],

            ["-1.E+10", ["scale" => -10, "value" => "-1",]],
            ["+1.E-10", ["scale" => 10, "value" => "1",]],
            ["-.1E-10", ["scale" => 11, "value" => "-1",]],

            ["1e-" . PHP_INT_MAX, ["scale" => PHP_INT_MAX, "value" => "1",]],
            ["1E+" . PHP_INT_MAX, ["scale" => PHP_INT_MIN + 1, "value" => "1",]],

            [" \t\r\n 1 \t\r\n", ["scale" => 0, "value" => "1",]],

            // invalid
            ["", []],
            ["a", []],
            [".", []],
            [".e", []],
            ["0x123", []],
            ["+NAN", []],
            ["-NAN", []],
            ["nan", []],
            ["inf", []],
            ["+inf", []],
            ["-inf", []],
            ["*0.1", []],
            ["e1", []],
            [".e1", []],
            ["+.e1", []],
            ["-.e1", []],
            ["-.e1", []],
            ["1E", []],
            ["1e+9223372036854775808", []],
            ["1e-9223372036854775808", []],

            [" 1\t\r\n 1 \t\r\n", []],


        ];
        $test_data = [];
        foreach ($datas as $value) {
            $test_data[$value[0]] = $value;
        }
        return $test_data;
    }
}
