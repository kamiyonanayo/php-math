<?php

declare(strict_types=1);

namespace Kamiyonanayo\Math\Internals;

use Kamiyonanayo\Math\Exception\ArithmeticException;
use Kamiyonanayo\Math\Internals\Arithmetic\ArithmeticTestTrait;
use Kamiyonanayo\Math\RoundingMode;
use Kamiyonanayo\Math\TestCaseBase;
use Kamiyonanayo\Math\UnitTestHelper;

class IntegerCalculatorTest extends TestCaseBase
{

    use ArithmeticTestTrait;

    /**
     * @covers \Kamiyonanayo\Math\Internals\IntegerCalculator::get
     * @covers \Kamiyonanayo\Math\Internals\IntegerCalculator::__construct
     */
    public function testGet()
    {
        UnitTestHelper::setPropValue(IntegerCalculator::class, "calc", null);
        $c = IntegerCalculator::get();
        $this->assertTrue($c instanceof IntegerCalculator);
    }

    /**
     * @dataProvider addDataProvider
     * @covers \Kamiyonanayo\Math\Internals\IntegerCalculator::add
     */
    public function testAdd($expected, $a, $b)
    {
        $c = IntegerCalculator::get();
        $this->assertSame($expected, $c->add($a, $b));
    }

    /**
     * @dataProvider subDataProvider
     * @covers \Kamiyonanayo\Math\Internals\IntegerCalculator::sub
     */
    public function testSub($expected, $a, $b)
    {
        $c = IntegerCalculator::get();
        $this->assertSame($expected, $c->sub($a, $b));
    }

    /**
     * @dataProvider mulDataProvider
     * @covers \Kamiyonanayo\Math\Internals\IntegerCalculator::mul
     */
    public function testMul($expected, $a, $b)
    {
        $c = IntegerCalculator::get();
        $this->assertSame($expected, $c->mul($a, $b));
    }

    /**
     * @dataProvider divDataProvider
     * @covers \Kamiyonanayo\Math\Internals\IntegerCalculator::div
     */
    public function testDiv($expected, $a, $b)
    {
        $c = IntegerCalculator::get();
        $this->assertSame($expected, $c->div($a, $b));
    }


    /**
     * @dataProvider modDataProvider
     * @covers \Kamiyonanayo\Math\Internals\IntegerCalculator::mod
     */
    public function testMod($expected, $a, $b)
    {
        $c = IntegerCalculator::get();
        $this->assertSame($expected, $c->mod($a, $b));
    }

    /**
     * @dataProvider divRoundDataProvider
     * @covers \Kamiyonanayo\Math\Internals\IntegerCalculator::divRound
     */
    public function testDivRound($expected, $a, $b, $r)
    {
        $c = IntegerCalculator::get();
        if ($expected === "ArithmeticException") {
            $e = null;
            $this->assertSame("", $c->divRound($a, $b, RoundingMode::valueOf(strtolower($r)), $e));
            $expected = ArithmeticException::RoundingNecessary();
            $this->assertInstanceOf(ArithmeticException::class, $e);
            $this->assertSame($expected->getMessage(), $e->getMessage());
            $this->assertSame($expected->getCode(), $e->getCode());
        } else {
            $e = null;
            $this->assertSame($expected, $c->divRound($a, $b, RoundingMode::valueOf(strtolower($r)), $e));
            $this->assertNull($e);
        }
    }

    public static function divRoundDataProvider()
    {
        $ret = [];
        $csv_data = UnitTestHelper::getCsvData(__DIR__ . "/IntegerCalculatorTestDivRound.csv");
        $header2index = array_flip($csv_data[0]);
        $rounds = explode(",", "UP,DOWN,CEILING,FLOOR,HALF_UP,HALF_DOWN,HALF_EVEN,HALF_ODD,HALF_CEILING,HALF_FLOOR,UNNECESSARY");
        foreach (array_slice($csv_data, 1) as $row) {
            $a = $row[$header2index["a"]];
            $b = $row[$header2index["b"]];
            foreach ($rounds as $round) {
                $expected = $row[$header2index[$round]];
                $ret[] = [$expected, $a, $b, $round];
            }
        };
        return $ret;
    }

    /**
     * @dataProvider absDataProvider
     * @covers \Kamiyonanayo\Math\Internals\IntegerCalculator::abs
     */
    public function testAbs($expected, $a)
    {
        $c = IntegerCalculator::get();
        $this->assertSame($expected, $c->abs($a));
    }

    public static function absDataProvider()
    {
        return [
            ["", ""],
            ["-", "--"],
            ["0", "0"],
            ["1", "1"],
            ["1", "-1"],
            ["9223372036854775807", "9223372036854775807"],
            ["9223372036854775807", "-9223372036854775807"],
            ["9223372036854775808", "9223372036854775808"],
            ["9223372036854775808", "-9223372036854775808"],
        ];
    }

    /**
     * @dataProvider compareDataProvider
     * @covers \Kamiyonanayo\Math\Internals\IntegerCalculator::compare
     */
    public function testCompare($expected, $a, $b)
    {
        $c = IntegerCalculator::get();
        $this->assertSame($expected, $c->compare($a, $b));
    }

    public static function compareDataProvider()
    {
        return [
            [1, "1", "-1"],
            [-1, "-1", "1"],

            [1, "100", "10"],
            [-1, "-100", "-10"],

            [-1, "10", "100"],
            [1, "-10", "-100"],

            [0, "0", "0"],
            [1, "1", "0"],
            [-1, "-1", "0"],

            [-1, "9223372036854775806", "9223372036854775807"],
            [0, "9223372036854775807", "9223372036854775807"],
            [1, "9223372036854775808", "9223372036854775807"],

            [1, "-9223372036854775808", "-9223372036854775809"],
            [0, "-9223372036854775808", "-9223372036854775808"],
            [-1, "-9223372036854775808", "-9223372036854775807"],

            [0, str_repeat("9", 99) . "9", str_repeat("9", 99) . "9"],
            [1, str_repeat("9", 99) . "9", str_repeat("9", 99) . "8"],
            [-1, str_repeat("9", 99) . "8", str_repeat("9", 99) . "9"],


            //
            [0, "", ""],
            [1, "1", ""],
            [-1, "", "1"],
            [-1, "a", "b"],
            [1, "b", "a"],

        ];
    }
}
