<?php

declare(strict_types=1);

namespace Kamiyonanayo\Math\Internals\Arithmetic;

use Kamiyonanayo\Math\TestCaseBase;

class BCMathArithmeticTest extends TestCaseBase
{
    use ArithmeticTestTrait;

    /**
     * @coversNothing
     */
    public function testArithmetic()
    {
        $c = new BCMathArithmetic();
        $this->assertTrue($c instanceof Arithmetic);
    }

    /**
     * @dataProvider addDataProvider
     * @covers \Kamiyonanayo\Math\Internals\Arithmetic\BCMathArithmetic::add
     */
    public function testAdd($expected, $a, $b)
    {
        $c = new BCMathArithmetic();
        $this->assertSame($expected, $c->add($a, $b));
    }

    /**
     * @dataProvider subDataProvider
     * @covers \Kamiyonanayo\Math\Internals\Arithmetic\BCMathArithmetic::sub
     */
    public function testSub($expected, $a, $b)
    {
        $c = new BCMathArithmetic();
        $this->assertSame($expected, $c->sub($a, $b));
    }

    /**
     * @dataProvider mulDataProvider
     * @covers \Kamiyonanayo\Math\Internals\Arithmetic\BCMathArithmetic::mul
     */
    public function testMul($expected, $a, $b)
    {
        $c = new BCMathArithmetic();
        $this->assertSame($expected, $c->mul($a, $b));
    }

    /**
     * @dataProvider divDataProvider
     * @covers \Kamiyonanayo\Math\Internals\Arithmetic\BCMathArithmetic::div
     */
    public function testDiv($expected, $a, $b)
    {
        $c = new BCMathArithmetic();
        $this->assertSame($expected, $c->div($a, $b));
    }

    /**
     * @dataProvider modDataProvider
     * @covers \Kamiyonanayo\Math\Internals\Arithmetic\BCMathArithmetic::mod
     */
    public function testMod($expected, $a, $b)
    {
        $c = new BCMathArithmetic();
        $this->assertSame($expected, $c->mod($a, $b));
    }
}
