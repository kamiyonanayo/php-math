<?php

declare(strict_types=1);

namespace Kamiyonanayo\Math;

use PHPUnit\Framework\TestCase;

abstract class TestCaseBase extends TestCase
{
    public function clearContext()
    {
        UnitTestHelper::setPropValue(Numeric::class, "context", null);
    }
}
