<?php

declare(strict_types=1);

namespace Kamiyonanayo\Math;

use Kamiyonanayo\Math\Exception\ArithmeticException;
use Kamiyonanayo\Math\Exception\IllegalArgumentException;
use Kamiyonanayo\Math\Exception\MathRuntimeException;
use Kamiyonanayo\Math\Exception\NumberFormatException;

class BigDecimalTest extends TestCaseBase
{
    protected function setUp(): void
    {
        $this->clearContext();
    }
    protected function tearDown(): void
    {
        $this->clearContext();
    }

    private static function getBigDecimalValue(?BigDecimal $val, string $prop_name)
    {
        if (is_null($val)) {
            return null;
        }
        // $cls = new \ReflectionClass($val);
        // $prop = $cls->getProperty($prop_name);
        // $prop->setAccessible(true);
        return UnitTestHelper::getPropValue($val, $prop_name);
    }

    private function assertSameBigDecimalExact(?string $expected_value, ?int $expected_scale, ?int $expected_sign, ?BigDecimal $actual_val)
    {
        $actual_value = self::getBigDecimalValue($actual_val, "value");
        $actual_scale = self::getBigDecimalValue($actual_val, "scale");
        $actual_sign = self::getBigDecimalValue($actual_val, "sign");

        $expected_in = ['value' => $expected_value, 'scale' => $expected_scale, 'sign' => $expected_sign];
        $actual_in = ['value' => $actual_value, 'scale' => $actual_scale, 'sign' => $actual_sign];

        $this->assertSame($expected_in, $actual_in, json_encode([$expected_in, $actual_in]));
    }

    private function assertSameBigDecimalValue(?string $expected_value, ?int $expected_scale, ?BigDecimal $actual_val)
    {
        $actual_value = self::getBigDecimalValue($actual_val, "value");
        $actual_scale = self::getBigDecimalValue($actual_val, "scale");

        $expected_in = ['value' => $expected_value, 'scale' => $expected_scale];
        $actual_in = ['value' => $actual_value, 'scale' => $actual_scale];

        $this->assertSame($expected_in, $actual_in, json_encode([$expected_in, $actual_in]));
    }

    private function assertSameBigDecimal(?BigDecimal $expected, ?BigDecimal $actual)
    {

        $expected_value = self::getBigDecimalValue($expected, "value");
        $expected_scale = self::getBigDecimalValue($expected, "scale");
        $expected_sign = self::getBigDecimalValue($expected, "sign");

        $actual_value = self::getBigDecimalValue($actual, "value");
        $actual_scale = self::getBigDecimalValue($actual, "scale");
        $actual_sign = self::getBigDecimalValue($actual, "sign");

        $expected_in = ['value' => $expected_value, 'scale' => $expected_scale, 'sign' => $expected_sign];
        $actual_in = ['value' => $actual_value, 'scale' => $actual_scale, 'sign' => $actual_sign];

        $this->assertSame($expected_in, $actual_in, json_encode([$expected_in, $actual_in, (is_null($expected) ? "null" : (string)$expected), (is_null($actual) ? "null" : (string)$actual)]));
    }

    private static function getMathRuntimeException(): MathRuntimeException
    {
        static $e = null;
        if (is_null($e)) {
            $e = new MathRuntimeException("abc", 99);
        }
        return $e;
    }

    private static function initContextError(): void
    {
        BigDecimal::getContext()->setError(self::getMathRuntimeException());
    }

    private function assertContextErrorNull(): void
    {
        $this->assertNull(BigDecimal::getContext()->getLastError());
    }

    private function assertSameInitContextError(): void
    {
        $this->assertSame(self::getMathRuntimeException(), BigDecimal::getContext()->getLastError());
    }

    private function assertSameMathRuntimeException(?MathRuntimeException $expected, ?MathRuntimeException $actual): void
    {
        if (is_null($expected)) {
            $this->assertNull($actual);
        } else {
            $this->assertNotNull($actual);
            $this->assertSame(get_class($expected), get_class($actual));
            $this->assertSame($expected->getMessage(), $actual->getMessage());
            $this->assertSame($expected->getCode(), $actual->getCode());
        }
    }

    /**
     * @coversNothing
     */
    public function testConsts()
    {
        $this->assertSame("NAN", BigDecimal::VALUE_NaN);
        $this->assertSame("INF", BigDecimal::VALUE_POSITIVE_INFINITE);
        $this->assertSame("-INF", BigDecimal::VALUE_NEGATIVE_INFINITE);
        $this->assertSame(0, BigDecimal::SIGN_NaN);
        $this->assertSame(1, BigDecimal::SIGN_POSITIVE_ZERO);
        $this->assertSame(2, BigDecimal::SIGN_POSITIVE_FINITE);
        $this->assertSame(-2, BigDecimal::SIGN_NEGATIVE_FINITE);
        $this->assertSame(3, BigDecimal::SIGN_POSITIVE_INFINITE);
        $this->assertSame(-3, BigDecimal::SIGN_NEGATIVE_INFINITE);
    }

    /**
     * @dataProvider constructDataProvider
     * @covers \Kamiyonanayo\Math\BigDecimal::__construct
     * @covers \Kamiyonanayo\Math\Numeric::__construct
     */
    public function test__construct(string $expected_value, int $expected_scale, int $expected_sign, string $actual_value, int $actual_scale)
    {

        $constructor = (static function ($value, $scale): BigDecimal {
            return new BigDecimal($value, $scale);
        })->bindTo(null, BigDecimal::class);

        /** @var BigDecimal */
        $r = $constructor($actual_value, $actual_scale);
        $this->assertSameBigDecimalExact($expected_value, $expected_scale,  $expected_sign, $r);
    }

    public static function constructDataProvider()
    {
        return [

            ["NAN", 0, BigDecimal::SIGN_NaN, "NAN", 0],
            ["0", 0, BigDecimal::SIGN_POSITIVE_ZERO, "0", 0],
            ["0", 0, BigDecimal::SIGN_POSITIVE_ZERO, "-0", 0],
            ["1", 0, BigDecimal::SIGN_POSITIVE_FINITE, "1", 0],
            ["-1", 0, BigDecimal::SIGN_NEGATIVE_FINITE, "-1", 0],
            ["INF", 0, BigDecimal::SIGN_POSITIVE_INFINITE, "INF", 0],
            ["-INF", 0, BigDecimal::SIGN_NEGATIVE_INFINITE, "-INF", 0],

        ];
    }

    /**
     * @dataProvider serializeDataProvider
     * @covers \Kamiyonanayo\Math\BigDecimal::serialize
     * @covers \Kamiyonanayo\Math\BigDecimal::unserialize
     */
    public function testSerialize(string $expected_value, int $expected_scale, int $expected_sign, string $actual_value, int $actual_scale)
    {

        $constructor = (static function ($value, $scale): BigDecimal {
            return new BigDecimal($value, $scale);
        })->bindTo(null, BigDecimal::class);

        /** @var BigDecimal */
        $r1 = $constructor($actual_value, $actual_scale);
        $this->assertSame(serialize([$expected_value, $expected_scale,]), $r1->serialize());

        $r2 = BigDecimal::valueOf("999");
        $r2->unserialize(serialize([$expected_value, $expected_scale,]));
        $this->assertSameBigDecimalExact($expected_value, $expected_scale,  $expected_sign, $r2);

        $s = serialize($r1);
        $this->assertSameBigDecimalExact($expected_value, $expected_scale,  $expected_sign, unserialize($s));
    }

    /**
     * @dataProvider serializeDataProvider
     * @covers \Kamiyonanayo\Math\BigDecimal::__serialize
     * @covers \Kamiyonanayo\Math\BigDecimal::__unserialize
     */
    public function test__Serialize(string $expected_value, int $expected_scale, int $expected_sign, string $actual_value, int $actual_scale)
    {

        $constructor = (static function ($value, $scale): BigDecimal {
            return new BigDecimal($value, $scale);
        })->bindTo(null, BigDecimal::class);

        /** @var BigDecimal */
        $r1 = $constructor($actual_value, $actual_scale);
        $this->assertSame([$expected_value, $expected_scale], $r1->__serialize());

        $r2 = BigDecimal::valueOf("999");
        $r2->__unserialize([$expected_value, $expected_scale]);
        $this->assertSameBigDecimalExact($expected_value, $expected_scale,  $expected_sign, $r2);

        $s = serialize($r1);
        $this->assertSameBigDecimalExact($expected_value, $expected_scale,  $expected_sign, unserialize($s));
    }

    public static function serializeDataProvider()
    {
        return [
            ["NAN", 0, BigDecimal::SIGN_NaN, "NAN", 0],
            ["0", 0, BigDecimal::SIGN_POSITIVE_ZERO, "0", 0],
            ["0", 0, BigDecimal::SIGN_POSITIVE_ZERO, "-0", 0],
            ["1", 0, BigDecimal::SIGN_POSITIVE_FINITE, "1", 0],
            ["-1", 0, BigDecimal::SIGN_NEGATIVE_FINITE, "-1", 0],
            ["INF", 0, BigDecimal::SIGN_POSITIVE_INFINITE, "INF", 0],
            ["-INF", 0, BigDecimal::SIGN_NEGATIVE_INFINITE, "-INF", 0],
        ];
    }

    /**
     * @covers \Kamiyonanayo\Math\BigDecimal::NaN
     */
    public function testNaN()
    {
        $b = BigDecimal::NaN();
        $this->assertSameBigDecimalExact("NAN", 0, 0, $b);
        $this->assertSame("NAN", (string)$b);
        $this->assertSame(BigDecimal::NaN(), $b);
    }

    /**
     * @covers \Kamiyonanayo\Math\BigDecimal::PositiveInfinite
     */
    public function testPositiveInfinite()
    {
        $b = BigDecimal::PositiveInfinite();
        $this->assertSameBigDecimalExact("INF", 0, 3, $b);
        $this->assertSame("INF", (string)$b);
        $this->assertSame(BigDecimal::PositiveInfinite(), $b);
    }

    /**
     * @covers \Kamiyonanayo\Math\BigDecimal::NegativeInfinite
     */
    public function testNegativeInfinite()
    {
        $b = BigDecimal::NegativeInfinite();
        $this->assertSameBigDecimalExact("-INF", 0, -3, $b);
        $this->assertSame("-INF", (string)$b);
        $this->assertSame(BigDecimal::NegativeInfinite(), $b);
    }

    /**
     * @covers \Kamiyonanayo\Math\BigDecimal::Zero
     */
    public function testZero()
    {
        $b = BigDecimal::Zero();
        $this->assertSameBigDecimalExact("0", 0, 1, $b);
        $this->assertSame("0", (string)$b);
        $this->assertSame(BigDecimal::Zero(), $b);
    }

    /**
     * @covers \Kamiyonanayo\Math\BigDecimal::One
     */
    public function testOne()
    {
        $b = BigDecimal::One();
        $this->assertSameBigDecimalExact("1", 0, 2, $b);
        $this->assertSame("1", (string)$b);
        $this->assertSame(BigDecimal::One(), $b);
    }

    /**
     * @covers \Kamiyonanayo\Math\BigDecimal::Ten
     */
    public function testTen()
    {
        $b = BigDecimal::Ten();
        $this->assertSameBigDecimalExact("10", 0, 2, $b);
        $this->assertSame("10", (string)$b);
        $this->assertSame(BigDecimal::Ten(), $b);
    }

    /**
     * @covers \Kamiyonanayo\Math\BigDecimal::OneHundred
     */
    public function testOneHundred()
    {
        $b = BigDecimal::OneHundred();
        $this->assertSameBigDecimalExact("100", 0, 2, $b);
        $this->assertSame("100", (string)$b);
        $this->assertSame(BigDecimal::OneHundred(), $b);
    }


    /**
     * @dataProvider valueOfUnscaledValDataProvider
     * @covers \Kamiyonanayo\Math\BigDecimal::valueOfUnscaledVal
     */
    public function testValueOfUnscaledVal(string $expected_str, string $expected_value, int $expected_scale, int $expected_sign, int $actual_unscaled_value, int $actual_scale)
    {

        $r = BigDecimal::valueOfUnscaledVal($actual_unscaled_value, $actual_scale);
        $this->assertSameBigDecimalExact($expected_value, $expected_scale,  $expected_sign, $r);
        $this->assertSame($expected_str, $r->toPlainString());
    }

    public static function valueOfUnscaledValDataProvider()
    {
        return [
            ["0", "0", 0, BigDecimal::SIGN_POSITIVE_ZERO, 0, 0],
            ["0", "0", 0, BigDecimal::SIGN_POSITIVE_ZERO, -0, 0],
            ["0.0", "0", 1, BigDecimal::SIGN_POSITIVE_ZERO, 0, 1],
            ["0.00", "0", 2, BigDecimal::SIGN_POSITIVE_ZERO, 0, 2],
            ["0.000", "0", 3, BigDecimal::SIGN_POSITIVE_ZERO, 0, 3],
            ["0", "0", -1, BigDecimal::SIGN_POSITIVE_ZERO, 0, -1],
            ["0", "0", -2, BigDecimal::SIGN_POSITIVE_ZERO, 0, -2],
            ["0", "0", -3, BigDecimal::SIGN_POSITIVE_ZERO, 0, -3],

            ["1000", "1", -3, BigDecimal::SIGN_POSITIVE_FINITE, 1, -3],
            ["100", "1", -2, BigDecimal::SIGN_POSITIVE_FINITE, 1, -2],
            ["10", "1", -1, BigDecimal::SIGN_POSITIVE_FINITE, 1, -1],
            ["1", "1", 0, BigDecimal::SIGN_POSITIVE_FINITE, 1, 0],
            ["0.1", "1", 1, BigDecimal::SIGN_POSITIVE_FINITE, 1, 1],
            ["0.01", "1", 2, BigDecimal::SIGN_POSITIVE_FINITE, 1, 2],
            ["0.001", "1", 3, BigDecimal::SIGN_POSITIVE_FINITE, 1, 3],

            ["-1000", "-1", -3, BigDecimal::SIGN_NEGATIVE_FINITE, -1, -3],
            ["-100", "-1", -2, BigDecimal::SIGN_NEGATIVE_FINITE, -1, -2],
            ["-10", "-1", -1, BigDecimal::SIGN_NEGATIVE_FINITE, -1, -1],
            ["-1", "-1", 0, BigDecimal::SIGN_NEGATIVE_FINITE, -1, 0],
            ["-0.1", "-1", 1, BigDecimal::SIGN_NEGATIVE_FINITE, -1, 1],
            ["-0.01", "-1", 2, BigDecimal::SIGN_NEGATIVE_FINITE, -1, 2],
            ["-0.001", "-1", 3, BigDecimal::SIGN_NEGATIVE_FINITE, -1, 3],

            [(string)PHP_INT_MAX, (string)PHP_INT_MAX, 0, BigDecimal::SIGN_POSITIVE_FINITE, PHP_INT_MAX, 0],
            [(string)PHP_INT_MIN, (string)PHP_INT_MIN, 0, BigDecimal::SIGN_NEGATIVE_FINITE, PHP_INT_MIN, 0],

            ["123456789000", "123456789", -3, BigDecimal::SIGN_POSITIVE_FINITE, 123456789, -3],
            ["12345678900", "123456789", -2, BigDecimal::SIGN_POSITIVE_FINITE, 123456789, -2],
            ["1234567890", "123456789", -1, BigDecimal::SIGN_POSITIVE_FINITE, 123456789, -1],
            ["123456789", "123456789", 0, BigDecimal::SIGN_POSITIVE_FINITE, 123456789, 0],
            ["12345678.9", "123456789", 1, BigDecimal::SIGN_POSITIVE_FINITE, 123456789, 1],
            ["1234567.89", "123456789", 2, BigDecimal::SIGN_POSITIVE_FINITE, 123456789, 2],
            ["123456.789", "123456789", 3, BigDecimal::SIGN_POSITIVE_FINITE, 123456789, 3],
            ["12345.6789", "123456789", 4, BigDecimal::SIGN_POSITIVE_FINITE, 123456789, 4],
            ["1234.56789", "123456789", 5, BigDecimal::SIGN_POSITIVE_FINITE, 123456789, 5],
            ["123.456789", "123456789", 6, BigDecimal::SIGN_POSITIVE_FINITE, 123456789, 6],
            ["12.3456789", "123456789", 7, BigDecimal::SIGN_POSITIVE_FINITE, 123456789, 7],
            ["1.23456789", "123456789", 8, BigDecimal::SIGN_POSITIVE_FINITE, 123456789, 8],
            ["0.123456789", "123456789", 9, BigDecimal::SIGN_POSITIVE_FINITE, 123456789, 9],
            ["0.0123456789", "123456789", 10, BigDecimal::SIGN_POSITIVE_FINITE, 123456789, 10],

        ];
    }

    /**
     * @dataProvider valueOfDataProvider
     * @covers \Kamiyonanayo\Math\BigDecimal::valueOf
     * @covers \Kamiyonanayo\Math\BigDecimal::_valueOf
     */
    public function testValueOf(string $expected_value, int $expected_scale, int $expected_sign, $actual)
    {
        self::initContextError();
        $r1 = BigDecimal::valueOf($actual);
        $this->assertContextErrorNull();
        $this->assertSameBigDecimalExact($expected_value, $expected_scale, $expected_sign, $r1);

        self::initContextError();
        $r2 = UnitTestHelper::invokeMethod(BigDecimal::class, "_valueOf", [$actual]);
        $this->assertSameInitContextError();
        $this->assertSameBigDecimalExact($expected_value, $expected_scale, $expected_sign, $r2);
    }

    public static function valueOfDataProvider()
    {
        return [

            ["NAN", 0, BigDecimal::SIGN_NaN, "NAN"],
            ["NAN", 0, BigDecimal::SIGN_NaN, NAN],
            ["0", 0, BigDecimal::SIGN_POSITIVE_ZERO, "0"],
            ["0", 0, BigDecimal::SIGN_POSITIVE_ZERO, "-0"],
            ["1", 0, BigDecimal::SIGN_POSITIVE_FINITE, "1"],
            ["-1", 0, BigDecimal::SIGN_NEGATIVE_FINITE, "-1"],
            ["INF", 0, BigDecimal::SIGN_POSITIVE_INFINITE, "INF"],
            ["INF", 0, BigDecimal::SIGN_POSITIVE_INFINITE, INF],
            ["-INF", 0, BigDecimal::SIGN_NEGATIVE_INFINITE, "-INF"],
            ["-INF", 0, BigDecimal::SIGN_NEGATIVE_INFINITE, -INF],

            ["1", 1, BigDecimal::SIGN_POSITIVE_FINITE, "0.1"],
            ["1", 2, BigDecimal::SIGN_POSITIVE_FINITE, "0.01"],
            ["1", 3, BigDecimal::SIGN_POSITIVE_FINITE, "0.001"],
            ["110", 2, BigDecimal::SIGN_POSITIVE_FINITE, "1.10"],

            ["-1", 1, BigDecimal::SIGN_NEGATIVE_FINITE, "-0.1"],
            ["-1", 2, BigDecimal::SIGN_NEGATIVE_FINITE, "-0.01"],
            ["-1", 3, BigDecimal::SIGN_NEGATIVE_FINITE, "-0.001"],
            ["-110", 2, BigDecimal::SIGN_NEGATIVE_FINITE, "-1.10"],

            [(string)PHP_INT_MAX, 0, BigDecimal::SIGN_POSITIVE_FINITE, BigDecimal::valueOfUnscaledVal(PHP_INT_MAX, 0)],
            [(string)PHP_INT_MIN, 0, BigDecimal::SIGN_NEGATIVE_FINITE, BigDecimal::valueOfUnscaledVal(PHP_INT_MIN, 0)],

            ["21", 10, BigDecimal::SIGN_POSITIVE_FINITE, "21E-10"],
            ["21", -10, BigDecimal::SIGN_POSITIVE_FINITE, "21E+10"],
            ["-21", 10, BigDecimal::SIGN_NEGATIVE_FINITE, "-21E-10"],
            ["-21", -10, BigDecimal::SIGN_NEGATIVE_FINITE, "-21E+10"],

            ["123", 8, BigDecimal::SIGN_POSITIVE_FINITE, "0.123E-5"],
            ["123", -2, BigDecimal::SIGN_POSITIVE_FINITE, "0.123E+5"],

            ["1", 0, BigDecimal::SIGN_POSITIVE_FINITE, 1],
            ["1", 1, BigDecimal::SIGN_POSITIVE_FINITE, 0.1],
            ["-1", 0, BigDecimal::SIGN_NEGATIVE_FINITE, -1],
            ["-1", 1, BigDecimal::SIGN_NEGATIVE_FINITE, -0.1],

            ["1", 0, BigDecimal::SIGN_POSITIVE_FINITE, true],

        ];
    }


    /**
     * @dataProvider valueOfIllegalDataProvider
     * @covers \Kamiyonanayo\Math\BigDecimal::valueOf
     * @covers \Kamiyonanayo\Math\BigDecimal::_valueOf
     */
    public function testValueOfIllegal(int $error_mode, ?MathRuntimeException $exception, ?string $expected_value, ?int $expected_scale, ?int $expected_sign, $actual)
    {

        // self::initContextError();
        // $r2 = UnitTestHelper::invokeMethod(BigDecimal::class, "_valueOf", [$actual]);
        // $this->assertSameInitContextError();
        // $this->assertSameBigDecimalExact($expected_value, $expected_scale, $expected_sign, $r2);

        BigDecimal::getContext()->setErrorMode($error_mode);
        self::initContextError();
        if ($error_mode == MathContext::ERROR_MODE_THROW_EXCEPTION) {
            $this->expectExceptionObject($exception);
            $r1 = BigDecimal::valueOf($actual);
        } else {
            $r1 = BigDecimal::valueOf($actual);
            if (is_null($exception)) {
                $this->assertContextErrorNull();
            } else {
                $this->assertSame(get_class($exception), get_class(BigDecimal::getContext()->getLastError()));
                $this->assertSame($exception->getMessage(), BigDecimal::getContext()->getLastError()->getMessage());
                $this->assertSame($exception->getCode(), BigDecimal::getContext()->getLastError()->getCode());
            }
            $this->assertSameBigDecimalExact($expected_value, $expected_scale, $expected_sign, $r1);
        }
    }

    public static function valueOfIllegalDataProvider()
    {
        return [
            [MathContext::ERROR_MODE_RETURN_NAN, new NumberFormatException('does not support number "A"'), "NAN", 0, BigDecimal::SIGN_NaN, "A"],
            [MathContext::ERROR_MODE_RETURN_NULL, new NumberFormatException('does not support number "A"'), null, null, null, "A"],
            [MathContext::ERROR_MODE_THROW_EXCEPTION, new NumberFormatException('does not support number "A"'), null, null, null, "A"],

            [MathContext::ERROR_MODE_RETURN_NULL, new NumberFormatException('does not support number "0+E"'), null, null, null, "0+E"],
            [MathContext::ERROR_MODE_RETURN_NULL, new NumberFormatException('does not support number ""'), null, null, null, []],
            [MathContext::ERROR_MODE_RETURN_NULL, new NumberFormatException('does not support number ""'), null, null, null, (object)["a" => 1]],
            [MathContext::ERROR_MODE_RETURN_NULL, new NumberFormatException('does not support number ""'), null, null, null, false],

        ];
    }

    /**
     * @dataProvider addDataProvider
     * @covers \Kamiyonanayo\Math\BigDecimal::add
     */
    public function testAdd(?BigDecimal $expected, $left_operand, $right_operand)
    {
        $left = BigDecimal::valueOf($left_operand);
        self::initContextError();
        $r = $left->add($right_operand);
        $this->assertContextErrorNull();
        $this->assertSameBigDecimal($expected, $r);
    }

    public static function addDataProvider()
    {
        return [

            [BigDecimal::valueOf("0"), BigDecimal::valueOf("0"), BigDecimal::valueOf("0")],
            [BigDecimal::valueOf("1.10"), BigDecimal::valueOf("1.10"), BigDecimal::valueOf("0")],
            [BigDecimal::valueOf("-1.10"), BigDecimal::valueOf("-1.10"), BigDecimal::valueOf("0")],
            [BigDecimal::valueOf("NAN"), BigDecimal::valueOf("NAN"), BigDecimal::valueOf("0")],
            [BigDecimal::valueOf("INF"), BigDecimal::valueOf("INF"), BigDecimal::valueOf("0")],
            [BigDecimal::valueOf("-INF"), BigDecimal::valueOf("-INF"), BigDecimal::valueOf("0")],

            [BigDecimal::valueOf("1.10"), BigDecimal::valueOf("0"), BigDecimal::valueOf("1.10")],
            [BigDecimal::valueOf("2.20"), BigDecimal::valueOf("1.10"), BigDecimal::valueOf("1.10")],
            [BigDecimal::valueOf("0.00"), BigDecimal::valueOf("-1.10"), BigDecimal::valueOf("1.10")],
            [BigDecimal::valueOf("NAN"), BigDecimal::valueOf("NAN"), BigDecimal::valueOf("1.10")],
            [BigDecimal::valueOf("INF"), BigDecimal::valueOf("INF"), BigDecimal::valueOf("1.10")],
            [BigDecimal::valueOf("-INF"), BigDecimal::valueOf("-INF"), BigDecimal::valueOf("1.10")],

            [BigDecimal::valueOf("-1.10"), BigDecimal::valueOf("0"), BigDecimal::valueOf("-1.10")],
            [BigDecimal::valueOf("0.00"), BigDecimal::valueOf("1.10"), BigDecimal::valueOf("-1.10")],
            [BigDecimal::valueOf("-2.20"), BigDecimal::valueOf("-1.10"), BigDecimal::valueOf("-1.10")],
            [BigDecimal::valueOf("NAN"), BigDecimal::valueOf("NAN"), BigDecimal::valueOf("-1.10")],
            [BigDecimal::valueOf("INF"), BigDecimal::valueOf("INF"), BigDecimal::valueOf("-1.10")],
            [BigDecimal::valueOf("-INF"), BigDecimal::valueOf("-INF"), BigDecimal::valueOf("-1.10")],

            [BigDecimal::valueOf("NAN"), BigDecimal::valueOf("0"), BigDecimal::valueOf("NAN")],
            [BigDecimal::valueOf("NAN"), BigDecimal::valueOf("1.10"), BigDecimal::valueOf("NAN")],
            [BigDecimal::valueOf("NAN"), BigDecimal::valueOf("-1.10"), BigDecimal::valueOf("NAN")],
            [BigDecimal::valueOf("NAN"), BigDecimal::valueOf("NAN"), BigDecimal::valueOf("NAN")],
            [BigDecimal::valueOf("NAN"), BigDecimal::valueOf("INF"), BigDecimal::valueOf("NAN")],
            [BigDecimal::valueOf("NAN"), BigDecimal::valueOf("-INF"), BigDecimal::valueOf("NAN")],

            [BigDecimal::valueOf("INF"), BigDecimal::valueOf("0"), BigDecimal::valueOf("INF")],
            [BigDecimal::valueOf("INF"), BigDecimal::valueOf("1.10"), BigDecimal::valueOf("INF")],
            [BigDecimal::valueOf("INF"), BigDecimal::valueOf("-1.10"), BigDecimal::valueOf("INF")],
            [BigDecimal::valueOf("NAN"), BigDecimal::valueOf("NAN"), BigDecimal::valueOf("INF")],
            [BigDecimal::valueOf("INF"), BigDecimal::valueOf("INF"), BigDecimal::valueOf("INF")],
            [BigDecimal::valueOf("NAN"), BigDecimal::valueOf("-INF"), BigDecimal::valueOf("INF")],

            [BigDecimal::valueOf("-INF"), BigDecimal::valueOf("0"), BigDecimal::valueOf("-INF")],
            [BigDecimal::valueOf("-INF"), BigDecimal::valueOf("1.10"), BigDecimal::valueOf("-INF")],
            [BigDecimal::valueOf("-INF"), BigDecimal::valueOf("-1.10"), BigDecimal::valueOf("-INF")],
            [BigDecimal::valueOf("NAN"), BigDecimal::valueOf("NAN"), BigDecimal::valueOf("-INF")],
            [BigDecimal::valueOf("NAN"), BigDecimal::valueOf("INF"), BigDecimal::valueOf("-INF")],
            [BigDecimal::valueOf("-INF"), BigDecimal::valueOf("-INF"), BigDecimal::valueOf("-INF")],

            [BigDecimal::valueOf("0.3"), BigDecimal::valueOf("0.1"), BigDecimal::valueOf("0.2")],

            [BigDecimal::valueOf("21E-11"), BigDecimal::valueOf("1E-11"), BigDecimal::valueOf("2E-10")],
            [BigDecimal::valueOf("12E-11"), BigDecimal::valueOf("1E-10"), BigDecimal::valueOf("2E-11")],
            [BigDecimal::valueOf("12E+10"), BigDecimal::valueOf("1E+11"), BigDecimal::valueOf("2E+10")],
            [BigDecimal::valueOf("21E+10"), BigDecimal::valueOf("1E+10"), BigDecimal::valueOf("2E+11")],
            [BigDecimal::valueOf("200000000000000000001E-10"), BigDecimal::valueOf("1E-10"), BigDecimal::valueOf("2E+10")],
            [BigDecimal::valueOf("100000000000000000002E-10"), BigDecimal::valueOf("1E+10"), BigDecimal::valueOf("2E-10")],

            [BigDecimal::valueOf("3E+5"), BigDecimal::valueOf("1E+5"), BigDecimal::valueOf("2E+5")],
            [BigDecimal::valueOf("5E+5"), BigDecimal::valueOf("2E+5"), BigDecimal::valueOf("3E+5")],

            [BigDecimal::valueOf("+18446744073709551616.246913578000"), BigDecimal::valueOf("+9223372036854775808.123456789000"), BigDecimal::valueOf("+9223372036854775808.123456789000")],
            [BigDecimal::valueOf("-18446744073709551616.246913578000"), BigDecimal::valueOf("-9223372036854775808.123456789000"), BigDecimal::valueOf("-9223372036854775808.123456789000")],
            [BigDecimal::valueOf("0e-12"), BigDecimal::valueOf("+9223372036854775808.123456789000"), BigDecimal::valueOf("-9223372036854775808.123456789000")],
            [BigDecimal::valueOf("0e-12"), BigDecimal::valueOf("-9223372036854775808.123456789000"), BigDecimal::valueOf("+9223372036854775808.123456789000")],

        ];
    }

    /**
     * @dataProvider addIllegalDataProvider
     * @covers \Kamiyonanayo\Math\BigDecimal::add
     */
    public function testAddIllegal(int $error_mode, ?MathRuntimeException $exception, ?BigDecimal $expected, $left_operand, $right_operand)
    {

        BigDecimal::getContext()->setErrorMode($error_mode);
        $left = BigDecimal::valueOf($left_operand);
        self::initContextError();
        if ($error_mode == MathContext::ERROR_MODE_THROW_EXCEPTION) {
            $this->expectExceptionObject($exception);
            $r = $left->add($right_operand);
        } else {
            $r = $left->add($right_operand);
            if (is_null($exception)) {
                $this->assertContextErrorNull();
            } else {
                $this->assertSame(get_class($exception), get_class(BigDecimal::getContext()->getLastError()));
                $this->assertSame($exception->getMessage(), BigDecimal::getContext()->getLastError()->getMessage());
                $this->assertSame($exception->getCode(), BigDecimal::getContext()->getLastError()->getCode());
            }

            $this->assertSameBigDecimal($expected, $r);
        }
    }

    public static function addIllegalDataProvider()
    {
        return [
            [MathContext::ERROR_MODE_RETURN_NAN, new NumberFormatException('does not support number "A"'), BigDecimal::NaN(), BigDecimal::One(), "A"],
            [MathContext::ERROR_MODE_RETURN_NULL, new NumberFormatException('does not support number "A"'), null, BigDecimal::One(), "A"],
            [MathContext::ERROR_MODE_THROW_EXCEPTION, new NumberFormatException('does not support number "A"'), null, BigDecimal::One(), "A"],
        ];
    }

    /**
     * @dataProvider subtractDataProvider
     * @covers \Kamiyonanayo\Math\BigDecimal::subtract
     */
    public function testSubtract(?BigDecimal $expected, $left_operand, $right_operand)
    {
        $left = BigDecimal::valueOf($left_operand);
        self::initContextError();
        $r = $left->subtract($right_operand);
        $this->assertContextErrorNull();
        $this->assertSameBigDecimal($expected, $r);
    }

    public static function subtractDataProvider()
    {
        return [

            [BigDecimal::valueOf("0"), BigDecimal::valueOf("0"), BigDecimal::valueOf("0")],
            [BigDecimal::valueOf("1.10"), BigDecimal::valueOf("1.10"), BigDecimal::valueOf("0")],
            [BigDecimal::valueOf("-1.10"), BigDecimal::valueOf("-1.10"), BigDecimal::valueOf("0")],
            [BigDecimal::valueOf("NAN"), BigDecimal::valueOf("NAN"), BigDecimal::valueOf("0")],
            [BigDecimal::valueOf("INF"), BigDecimal::valueOf("INF"), BigDecimal::valueOf("0")],
            [BigDecimal::valueOf("-INF"), BigDecimal::valueOf("-INF"), BigDecimal::valueOf("0")],

            [BigDecimal::valueOf("-1.10"), BigDecimal::valueOf("0"), BigDecimal::valueOf("1.10")],
            [BigDecimal::valueOf("0.00"), BigDecimal::valueOf("1.10"), BigDecimal::valueOf("1.10")],
            [BigDecimal::valueOf("-2.20"), BigDecimal::valueOf("-1.10"), BigDecimal::valueOf("1.10")],
            [BigDecimal::valueOf("NAN"), BigDecimal::valueOf("NAN"), BigDecimal::valueOf("1.10")],
            [BigDecimal::valueOf("INF"), BigDecimal::valueOf("INF"), BigDecimal::valueOf("1.10")],
            [BigDecimal::valueOf("-INF"), BigDecimal::valueOf("-INF"), BigDecimal::valueOf("1.10")],

            [BigDecimal::valueOf("1.10"), BigDecimal::valueOf("0"), BigDecimal::valueOf("-1.10")],
            [BigDecimal::valueOf("2.20"), BigDecimal::valueOf("1.10"), BigDecimal::valueOf("-1.10")],
            [BigDecimal::valueOf("0.00"), BigDecimal::valueOf("-1.10"), BigDecimal::valueOf("-1.10")],
            [BigDecimal::valueOf("NAN"), BigDecimal::valueOf("NAN"), BigDecimal::valueOf("-1.10")],
            [BigDecimal::valueOf("INF"), BigDecimal::valueOf("INF"), BigDecimal::valueOf("-1.10")],
            [BigDecimal::valueOf("-INF"), BigDecimal::valueOf("-INF"), BigDecimal::valueOf("-1.10")],

            [BigDecimal::valueOf("NAN"), BigDecimal::valueOf("0"), BigDecimal::valueOf("NAN")],
            [BigDecimal::valueOf("NAN"), BigDecimal::valueOf("1.10"), BigDecimal::valueOf("NAN")],
            [BigDecimal::valueOf("NAN"), BigDecimal::valueOf("-1.10"), BigDecimal::valueOf("NAN")],
            [BigDecimal::valueOf("NAN"), BigDecimal::valueOf("NAN"), BigDecimal::valueOf("NAN")],
            [BigDecimal::valueOf("NAN"), BigDecimal::valueOf("INF"), BigDecimal::valueOf("NAN")],
            [BigDecimal::valueOf("NAN"), BigDecimal::valueOf("-INF"), BigDecimal::valueOf("NAN")],

            [BigDecimal::valueOf("-INF"), BigDecimal::valueOf("0"), BigDecimal::valueOf("INF")],
            [BigDecimal::valueOf("-INF"), BigDecimal::valueOf("1.10"), BigDecimal::valueOf("INF")],
            [BigDecimal::valueOf("-INF"), BigDecimal::valueOf("-1.10"), BigDecimal::valueOf("INF")],
            [BigDecimal::valueOf("NAN"), BigDecimal::valueOf("NAN"), BigDecimal::valueOf("INF")],
            [BigDecimal::valueOf("NAN"), BigDecimal::valueOf("INF"), BigDecimal::valueOf("INF")],
            [BigDecimal::valueOf("-INF"), BigDecimal::valueOf("-INF"), BigDecimal::valueOf("INF")],

            [BigDecimal::valueOf("INF"), BigDecimal::valueOf("0"), BigDecimal::valueOf("-INF")],
            [BigDecimal::valueOf("INF"), BigDecimal::valueOf("1.10"), BigDecimal::valueOf("-INF")],
            [BigDecimal::valueOf("INF"), BigDecimal::valueOf("-1.10"), BigDecimal::valueOf("-INF")],
            [BigDecimal::valueOf("NAN"), BigDecimal::valueOf("NAN"), BigDecimal::valueOf("-INF")],
            [BigDecimal::valueOf("INF"), BigDecimal::valueOf("INF"), BigDecimal::valueOf("-INF")],
            [BigDecimal::valueOf("NAN"), BigDecimal::valueOf("-INF"), BigDecimal::valueOf("-INF")],

            [BigDecimal::valueOf("-0.1"), BigDecimal::valueOf("0.1"), BigDecimal::valueOf("0.2")],

            [BigDecimal::valueOf("-1.9E-10"), BigDecimal::valueOf("1E-11"), BigDecimal::valueOf("2E-10")],
            [BigDecimal::valueOf("8E-11"), BigDecimal::valueOf("1E-10"), BigDecimal::valueOf("2E-11")],
            [BigDecimal::valueOf("8E+10"), BigDecimal::valueOf("1E+11"), BigDecimal::valueOf("2E+10")],
            [BigDecimal::valueOf("-19E+10"), BigDecimal::valueOf("1E+10"), BigDecimal::valueOf("2E+11")],
            [BigDecimal::valueOf("-19999999999.9999999999"), BigDecimal::valueOf("1E-10"), BigDecimal::valueOf("2E+10")],
            [BigDecimal::valueOf("9999999999.9999999998"), BigDecimal::valueOf("1E+10"), BigDecimal::valueOf("2E-10")],

            [BigDecimal::valueOf("-1E+5"), BigDecimal::valueOf("1E+5"), BigDecimal::valueOf("2E+5")],
            [BigDecimal::valueOf("-1E+5"), BigDecimal::valueOf("2E+5"), BigDecimal::valueOf("3E+5")],

            [BigDecimal::valueOf("0E-12"), BigDecimal::valueOf("+9223372036854775808.123456789000"), BigDecimal::valueOf("+9223372036854775808.123456789000")],
            [BigDecimal::valueOf("0E-12"), BigDecimal::valueOf("-9223372036854775808.123456789000"), BigDecimal::valueOf("-9223372036854775808.123456789000")],
            [BigDecimal::valueOf("+18446744073709551616.246913578000"), BigDecimal::valueOf("+9223372036854775808.123456789000"), BigDecimal::valueOf("-9223372036854775808.123456789000")],
            [BigDecimal::valueOf("-18446744073709551616.246913578000"), BigDecimal::valueOf("-9223372036854775808.123456789000"), BigDecimal::valueOf("+9223372036854775808.123456789000")],

        ];
    }

    /**
     * @dataProvider subtractIllegalDataProvider
     * @covers \Kamiyonanayo\Math\BigDecimal::subtract
     */
    public function testSubtractIllegal(int $error_mode, ?MathRuntimeException $exception, ?BigDecimal $expected, $left_operand, $right_operand)
    {

        BigDecimal::getContext()->setErrorMode($error_mode);
        $left = BigDecimal::valueOf($left_operand);
        self::initContextError();
        if ($error_mode == MathContext::ERROR_MODE_THROW_EXCEPTION) {
            $this->expectExceptionObject($exception);
            $r = $left->subtract($right_operand);
        } else {
            $r = $left->subtract($right_operand);
            if (is_null($exception)) {
                $this->assertContextErrorNull();
            } else {
                $this->assertSame(get_class($exception), get_class(BigDecimal::getContext()->getLastError()));
                $this->assertSame($exception->getMessage(), BigDecimal::getContext()->getLastError()->getMessage());
                $this->assertSame($exception->getCode(), BigDecimal::getContext()->getLastError()->getCode());
            }

            $this->assertSameBigDecimal($expected, $r);
        }
    }

    public static function subtractIllegalDataProvider()
    {
        return [
            [MathContext::ERROR_MODE_RETURN_NAN, new NumberFormatException('does not support number "A"'), BigDecimal::NaN(), BigDecimal::One(), "A"],
            [MathContext::ERROR_MODE_RETURN_NULL, new NumberFormatException('does not support number "A"'), null, BigDecimal::One(), "A"],
            [MathContext::ERROR_MODE_THROW_EXCEPTION, new NumberFormatException('does not support number "A"'), null, BigDecimal::One(), "A"],
        ];
    }


    /**
     * @dataProvider multiplyDataProvider
     * @covers \Kamiyonanayo\Math\BigDecimal::multiply
     */
    public function testMultiply(?BigDecimal $expected, $left_operand, $right_operand)
    {
        $left = BigDecimal::valueOf($left_operand);
        self::initContextError();
        $r = $left->multiply($right_operand);
        $this->assertContextErrorNull();
        $this->assertSameBigDecimal($expected, $r);
    }

    public static function multiplyDataProvider()
    {
        return [

            [BigDecimal::valueOf("0"), BigDecimal::valueOf("0"), BigDecimal::valueOf("0")],
            [BigDecimal::valueOf("0.00"), BigDecimal::valueOf("1.10"), BigDecimal::valueOf("0")],
            [BigDecimal::valueOf("0.00"), BigDecimal::valueOf("-1.10"), BigDecimal::valueOf("0")],
            [BigDecimal::valueOf("NAN"), BigDecimal::valueOf("NAN"), BigDecimal::valueOf("0")],
            [BigDecimal::valueOf("NAN"), BigDecimal::valueOf("INF"), BigDecimal::valueOf("0")],
            [BigDecimal::valueOf("NAN"), BigDecimal::valueOf("-INF"), BigDecimal::valueOf("0")],

            [BigDecimal::valueOf("0.00"), BigDecimal::valueOf("0"), BigDecimal::valueOf("1.10")],
            [BigDecimal::valueOf("1.2100"), BigDecimal::valueOf("1.10"), BigDecimal::valueOf("1.10")],
            [BigDecimal::valueOf("-1.2100"), BigDecimal::valueOf("-1.10"), BigDecimal::valueOf("1.10")],
            [BigDecimal::valueOf("NAN"), BigDecimal::valueOf("NAN"), BigDecimal::valueOf("1.10")],
            [BigDecimal::valueOf("INF"), BigDecimal::valueOf("INF"), BigDecimal::valueOf("1.10")],
            [BigDecimal::valueOf("-INF"), BigDecimal::valueOf("-INF"), BigDecimal::valueOf("1.10")],

            [BigDecimal::valueOf("0.00"), BigDecimal::valueOf("0"), BigDecimal::valueOf("-1.10")],
            [BigDecimal::valueOf("-1.2100"), BigDecimal::valueOf("1.10"), BigDecimal::valueOf("-1.10")],
            [BigDecimal::valueOf("1.2100"), BigDecimal::valueOf("-1.10"), BigDecimal::valueOf("-1.10")],
            [BigDecimal::valueOf("NAN"), BigDecimal::valueOf("NAN"), BigDecimal::valueOf("-1.10")],
            [BigDecimal::valueOf("-INF"), BigDecimal::valueOf("INF"), BigDecimal::valueOf("-1.10")],
            [BigDecimal::valueOf("INF"), BigDecimal::valueOf("-INF"), BigDecimal::valueOf("-1.10")],

            [BigDecimal::valueOf("NAN"), BigDecimal::valueOf("0"), BigDecimal::valueOf("NAN")],
            [BigDecimal::valueOf("NAN"), BigDecimal::valueOf("1.10"), BigDecimal::valueOf("NAN")],
            [BigDecimal::valueOf("NAN"), BigDecimal::valueOf("-1.10"), BigDecimal::valueOf("NAN")],
            [BigDecimal::valueOf("NAN"), BigDecimal::valueOf("NAN"), BigDecimal::valueOf("NAN")],
            [BigDecimal::valueOf("NAN"), BigDecimal::valueOf("INF"), BigDecimal::valueOf("NAN")],
            [BigDecimal::valueOf("NAN"), BigDecimal::valueOf("-INF"), BigDecimal::valueOf("NAN")],

            [BigDecimal::valueOf("NAN"), BigDecimal::valueOf("0"), BigDecimal::valueOf("INF")],
            [BigDecimal::valueOf("INF"), BigDecimal::valueOf("1.10"), BigDecimal::valueOf("INF")],
            [BigDecimal::valueOf("-INF"), BigDecimal::valueOf("-1.10"), BigDecimal::valueOf("INF")],
            [BigDecimal::valueOf("NAN"), BigDecimal::valueOf("NAN"), BigDecimal::valueOf("INF")],
            [BigDecimal::valueOf("INF"), BigDecimal::valueOf("INF"), BigDecimal::valueOf("INF")],
            [BigDecimal::valueOf("-INF"), BigDecimal::valueOf("-INF"), BigDecimal::valueOf("INF")],

            [BigDecimal::valueOf("NAN"), BigDecimal::valueOf("0"), BigDecimal::valueOf("-INF")],
            [BigDecimal::valueOf("-INF"), BigDecimal::valueOf("1.10"), BigDecimal::valueOf("-INF")],
            [BigDecimal::valueOf("INF"), BigDecimal::valueOf("-1.10"), BigDecimal::valueOf("-INF")],
            [BigDecimal::valueOf("NAN"), BigDecimal::valueOf("NAN"), BigDecimal::valueOf("-INF")],
            [BigDecimal::valueOf("-INF"), BigDecimal::valueOf("INF"), BigDecimal::valueOf("-INF")],
            [BigDecimal::valueOf("INF"), BigDecimal::valueOf("-INF"), BigDecimal::valueOf("-INF")],

            [BigDecimal::valueOf("0.02"), BigDecimal::valueOf("0.1"), BigDecimal::valueOf("0.2")],

            [BigDecimal::valueOf("0.000000000000000000002"), BigDecimal::valueOf("1E-11"), BigDecimal::valueOf("2E-10")],
            [BigDecimal::valueOf("0.000000000000000000002"), BigDecimal::valueOf("1E-10"), BigDecimal::valueOf("2E-11")],
            [BigDecimal::valueOf("2E+21"), BigDecimal::valueOf("1E+11"), BigDecimal::valueOf("2E+10")],
            [BigDecimal::valueOf("2E+21"), BigDecimal::valueOf("1E+10"), BigDecimal::valueOf("2E+11")],
            [BigDecimal::valueOf("2E+0"), BigDecimal::valueOf("1E-10"), BigDecimal::valueOf("2E+10")],
            [BigDecimal::valueOf("2E+0"), BigDecimal::valueOf("1E+10"), BigDecimal::valueOf("2E-10")],

            [BigDecimal::valueOf("2E+10"), BigDecimal::valueOf("1E+5"), BigDecimal::valueOf("2E+5")],
            [BigDecimal::valueOf("6E+10"), BigDecimal::valueOf("2E+5"), BigDecimal::valueOf("3E+5")],

            [BigDecimal::valueOf("85070591730234615868121027648787013425.156362602750190521000000"), BigDecimal::valueOf("+9223372036854775808.123456789000"), BigDecimal::valueOf("+9223372036854775808.123456789000")],
            [BigDecimal::valueOf("85070591730234615868121027648787013425.156362602750190521000000"), BigDecimal::valueOf("-9223372036854775808.123456789000"), BigDecimal::valueOf("-9223372036854775808.123456789000")],
            [BigDecimal::valueOf("-85070591730234615868121027648787013425.156362602750190521000000"), BigDecimal::valueOf("+9223372036854775808.123456789000"), BigDecimal::valueOf("-9223372036854775808.123456789000")],
            [BigDecimal::valueOf("-85070591730234615868121027648787013425.156362602750190521000000"), BigDecimal::valueOf("-9223372036854775808.123456789000"), BigDecimal::valueOf("+9223372036854775808.123456789000")],

            [BigDecimal::valueOf("123.456"), BigDecimal::valueOf("123.456"), BigDecimal::valueOf("1")],
            [BigDecimal::valueOf("-123.456"), BigDecimal::valueOf("-123.456"), BigDecimal::valueOf("1")],
            [BigDecimal::valueOf("123.4560"), BigDecimal::valueOf("123.456"), BigDecimal::valueOf("1.0")],
            [BigDecimal::valueOf("-123.4560"), BigDecimal::valueOf("123.456"), BigDecimal::valueOf("-1.0")],

            [BigDecimal::valueOf("123.456"), BigDecimal::valueOf("1"), BigDecimal::valueOf("123.456")],
            [BigDecimal::valueOf("-123.456"), BigDecimal::valueOf("1"), BigDecimal::valueOf("-123.456")],
            [BigDecimal::valueOf("123.4560"), BigDecimal::valueOf("1.0"), BigDecimal::valueOf("123.456")],
            [BigDecimal::valueOf("-123.4560"), BigDecimal::valueOf("-1.0"), BigDecimal::valueOf("123.456")],


        ];
    }

    /**
     * @dataProvider multiplyIllegalDataProvider
     * @covers \Kamiyonanayo\Math\BigDecimal::multiply
     */
    public function testMultiplyIllegal(int $error_mode, ?MathRuntimeException $exception, ?BigDecimal $expected, $left_operand, $right_operand)
    {

        BigDecimal::getContext()->setErrorMode($error_mode);
        $left = BigDecimal::valueOf($left_operand);
        self::initContextError();
        if ($error_mode == MathContext::ERROR_MODE_THROW_EXCEPTION) {
            $this->expectExceptionObject($exception);
            $r = $left->multiply($right_operand);
        } else {
            $r = $left->multiply($right_operand);
            if (is_null($exception)) {
                $this->assertContextErrorNull();
            } else {
                $this->assertSame(get_class($exception), get_class(BigDecimal::getContext()->getLastError()));
                $this->assertSame($exception->getMessage(), BigDecimal::getContext()->getLastError()->getMessage());
                $this->assertSame($exception->getCode(), BigDecimal::getContext()->getLastError()->getCode());
            }

            $this->assertSameBigDecimal($expected, $r);
        }
    }

    public static function multiplyIllegalDataProvider()
    {
        return [
            [MathContext::ERROR_MODE_RETURN_NAN, new NumberFormatException('does not support number "A"'), BigDecimal::NaN(), BigDecimal::One(), "A"],
            [MathContext::ERROR_MODE_RETURN_NULL, new NumberFormatException('does not support number "A"'), null, BigDecimal::One(), "A"],
            [MathContext::ERROR_MODE_THROW_EXCEPTION, new NumberFormatException('does not support number "A"'), null, BigDecimal::One(), "A"],
        ];
    }

    /**
     * @dataProvider divideDataProvider
     * @covers \Kamiyonanayo\Math\BigDecimal::divide
     */
    public function testDivide($expected, $left_operand, $right_operand, ?int $actual_scale, $rounding_mode)
    {
        $left = BigDecimal::valueOf($left_operand);
        self::initContextError();
        if (is_subclass_of($expected, \RuntimeException::class)) {
            BigDecimal::getContext()->setErrorMode(MathContext::ERROR_MODE_THROW_EXCEPTION);
            $this->expectException($expected);
        }
        $r = $left->divide($right_operand, $actual_scale, $rounding_mode);
        if (!is_subclass_of($expected, \RuntimeException::class)) {
            $this->assertContextErrorNull();
            $this->assertSameBigDecimal(BigDecimal::valueOf($expected), $r);
        }
    }

    public static function divideDataProvider()
    {
        return [

            [ArithmeticException::class, BigDecimal::valueOf("0"), BigDecimal::valueOf("0"), 10, RoundingMode::DOWN()],
            [ArithmeticException::class, BigDecimal::valueOf("1.10"), BigDecimal::valueOf("0"), 10, RoundingMode::DOWN()],
            [ArithmeticException::class, BigDecimal::valueOf("-1.10"), BigDecimal::valueOf("0"), 10, RoundingMode::DOWN()],
            [BigDecimal::valueOf("NAN"), BigDecimal::valueOf("NAN"), BigDecimal::valueOf("0"), 10, RoundingMode::DOWN()],
            [ArithmeticException::class, BigDecimal::valueOf("INF"), BigDecimal::valueOf("0"), 10, RoundingMode::DOWN()],
            [ArithmeticException::class, BigDecimal::valueOf("-INF"), BigDecimal::valueOf("0"), 10, RoundingMode::DOWN()],

            [BigDecimal::valueOf("0E-10"), BigDecimal::valueOf("0"), BigDecimal::valueOf("1.10"), 10, RoundingMode::DOWN()],
            [BigDecimal::valueOf("1.0000000000"), BigDecimal::valueOf("1.10"), BigDecimal::valueOf("1.10"), 10, RoundingMode::DOWN()],
            [BigDecimal::valueOf("-1.0000000000"), BigDecimal::valueOf("-1.10"), BigDecimal::valueOf("1.10"), 10, RoundingMode::DOWN()],
            [BigDecimal::valueOf("NAN"), BigDecimal::valueOf("NAN"), BigDecimal::valueOf("1.10"), 10, RoundingMode::DOWN()],
            [BigDecimal::valueOf("INF"), BigDecimal::valueOf("INF"), BigDecimal::valueOf("1.10"), 10, RoundingMode::DOWN()],
            [BigDecimal::valueOf("-INF"), BigDecimal::valueOf("-INF"), BigDecimal::valueOf("1.10"), 10, RoundingMode::DOWN()],

            [BigDecimal::valueOf("0E-10"), BigDecimal::valueOf("0"), BigDecimal::valueOf("-1.10"), 10, RoundingMode::DOWN()],
            [BigDecimal::valueOf("-1.0000000000"), BigDecimal::valueOf("1.10"), BigDecimal::valueOf("-1.10"), 10, RoundingMode::DOWN()],
            [BigDecimal::valueOf("1.0000000000"), BigDecimal::valueOf("-1.10"), BigDecimal::valueOf("-1.10"), 10, RoundingMode::DOWN()],
            [BigDecimal::valueOf("NAN"), BigDecimal::valueOf("NAN"), BigDecimal::valueOf("-1.10"), 10, RoundingMode::DOWN()],
            [BigDecimal::valueOf("-INF"), BigDecimal::valueOf("INF"), BigDecimal::valueOf("-1.10"), 10, RoundingMode::DOWN()],
            [BigDecimal::valueOf("INF"), BigDecimal::valueOf("-INF"), BigDecimal::valueOf("-1.10"), 10, RoundingMode::DOWN()],

            [BigDecimal::valueOf("NAN"), BigDecimal::valueOf("0"), BigDecimal::valueOf("NAN"), 10, RoundingMode::DOWN()],
            [BigDecimal::valueOf("NAN"), BigDecimal::valueOf("1.10"), BigDecimal::valueOf("NAN"), 10, RoundingMode::DOWN()],
            [BigDecimal::valueOf("NAN"), BigDecimal::valueOf("-1.10"), BigDecimal::valueOf("NAN"), 10, RoundingMode::DOWN()],
            [BigDecimal::valueOf("NAN"), BigDecimal::valueOf("NAN"), BigDecimal::valueOf("NAN"), 10, RoundingMode::DOWN()],
            [BigDecimal::valueOf("NAN"), BigDecimal::valueOf("INF"), BigDecimal::valueOf("NAN"), 10, RoundingMode::DOWN()],
            [BigDecimal::valueOf("NAN"), BigDecimal::valueOf("-INF"), BigDecimal::valueOf("NAN"), 10, RoundingMode::DOWN()],

            [BigDecimal::valueOf("0E-0"), BigDecimal::valueOf("0"), BigDecimal::valueOf("INF"), 10, RoundingMode::DOWN()],
            [BigDecimal::valueOf("0E-0"), BigDecimal::valueOf("1.10"), BigDecimal::valueOf("INF"), 10, RoundingMode::DOWN()],
            [BigDecimal::valueOf("0E-0"), BigDecimal::valueOf("-1.10"), BigDecimal::valueOf("INF"), 10, RoundingMode::DOWN()],
            [BigDecimal::valueOf("NAN"), BigDecimal::valueOf("NAN"), BigDecimal::valueOf("INF"), 10, RoundingMode::DOWN()],
            [BigDecimal::valueOf("NAN"), BigDecimal::valueOf("INF"), BigDecimal::valueOf("INF"), 10, RoundingMode::DOWN()],
            [BigDecimal::valueOf("NAN"), BigDecimal::valueOf("-INF"), BigDecimal::valueOf("INF"), 10, RoundingMode::DOWN()],

            [BigDecimal::valueOf("0E-0"), BigDecimal::valueOf("0"), BigDecimal::valueOf("-INF"), 10, RoundingMode::DOWN()],
            [BigDecimal::valueOf("0E-0"), BigDecimal::valueOf("1.10"), BigDecimal::valueOf("-INF"), 10, RoundingMode::DOWN()],
            [BigDecimal::valueOf("0E-0"), BigDecimal::valueOf("-1.10"), BigDecimal::valueOf("-INF"), 10, RoundingMode::DOWN()],
            [BigDecimal::valueOf("NAN"), BigDecimal::valueOf("NAN"), BigDecimal::valueOf("-INF"), 10, RoundingMode::DOWN()],
            [BigDecimal::valueOf("NAN"), BigDecimal::valueOf("INF"), BigDecimal::valueOf("-INF"), 10, RoundingMode::DOWN()],
            [BigDecimal::valueOf("NAN"), BigDecimal::valueOf("-INF"), BigDecimal::valueOf("-INF"), 10, RoundingMode::DOWN()],

            [BigDecimal::valueOf("0.5000000000"), BigDecimal::valueOf("0.1"), BigDecimal::valueOf("0.2"), 10, RoundingMode::DOWN()],

            [BigDecimal::valueOf("0.0500000000"), BigDecimal::valueOf("1E-11"), BigDecimal::valueOf("2E-10"), 10, RoundingMode::DOWN()],
            [BigDecimal::valueOf("5.0000000000"), BigDecimal::valueOf("1E-10"), BigDecimal::valueOf("2E-11"), 10, RoundingMode::DOWN()],
            [BigDecimal::valueOf("5.0000000000"), BigDecimal::valueOf("1E+11"), BigDecimal::valueOf("2E+10"), 10, RoundingMode::DOWN()],
            [BigDecimal::valueOf("0.0500000000"), BigDecimal::valueOf("1E+10"), BigDecimal::valueOf("2E+11"), 10, RoundingMode::DOWN()],
            [BigDecimal::valueOf("0.000000000000000000005000000000"), BigDecimal::valueOf("1E-10"), BigDecimal::valueOf("2E+10"), 30, RoundingMode::DOWN()],
            [BigDecimal::valueOf("50000000000000000000.000000000000000000000000000000"), BigDecimal::valueOf("1E+10"), BigDecimal::valueOf("2E-10"), 30, RoundingMode::DOWN()],

            [BigDecimal::valueOf("0.500000000000000000000000000000"), BigDecimal::valueOf("1E+5"), BigDecimal::valueOf("2E+5"), 30, RoundingMode::DOWN()],
            [BigDecimal::valueOf("0.666666666666666666666666666666"), BigDecimal::valueOf("2E+5"), BigDecimal::valueOf("3E+5"), 30, RoundingMode::DOWN()],

            [BigDecimal::valueOf("+3.000000000000000000000000000000"), BigDecimal::valueOf("+27670116110564327424.370370367003"), BigDecimal::valueOf("+9223372036854775808.123456789001"), 30, RoundingMode::DOWN()],
            [BigDecimal::valueOf("+3.000000000000000000000000000000"), BigDecimal::valueOf("-27670116110564327424.370370367003"), BigDecimal::valueOf("-9223372036854775808.123456789001"), 30, RoundingMode::DOWN()],
            [BigDecimal::valueOf("-3.000000000000000000000000000000"), BigDecimal::valueOf("+27670116110564327424.370370367003"), BigDecimal::valueOf("-9223372036854775808.123456789001"), 30, RoundingMode::DOWN()],
            [BigDecimal::valueOf("-3.000000000000000000000000000000"), BigDecimal::valueOf("-27670116110564327424.370370367003"), BigDecimal::valueOf("+9223372036854775808.123456789001"), 30, RoundingMode::DOWN()],

            [BigDecimal::valueOf("+13835058055282163712185185183501500000000000000000.0000000000"), BigDecimal::valueOf("+27670116110564327424.370370367003"), BigDecimal::valueOf("+0.000000000000000000000000000002"), 10, RoundingMode::DOWN()],
            [BigDecimal::valueOf("+13835058055282163712185185183501500000000000000000.0000000000"), BigDecimal::valueOf("-27670116110564327424.370370367003"), BigDecimal::valueOf("-0.000000000000000000000000000002"), 10, RoundingMode::DOWN()],
            [BigDecimal::valueOf("-13835058055282163712185185183501500000000000000000.0000000000"), BigDecimal::valueOf("+27670116110564327424.370370367003"), BigDecimal::valueOf("-0.000000000000000000000000000002"), 10, RoundingMode::DOWN()],
            [BigDecimal::valueOf("-13835058055282163712185185183501500000000000000000.0000000000"), BigDecimal::valueOf("-27670116110564327424.370370367003"), BigDecimal::valueOf("+0.000000000000000000000000000002"), 10, RoundingMode::DOWN()],

            [BigDecimal::valueOf("0.462857142857142857142857142857"), BigDecimal::valueOf("81"), BigDecimal::valueOf("175"), 30, RoundingMode::DOWN()],
            [BigDecimal::valueOf("4.62857142E-52"), BigDecimal::valueOf("0.000000000000000000000000081"), BigDecimal::valueOf("175000000000000000000000000"), 60, RoundingMode::DOWN()],
            [BigDecimal::valueOf("4.62857142E-52"), BigDecimal::valueOf("0.000000000000000000000000081"), BigDecimal::valueOf("175E+24"), 60, RoundingMode::DOWN()],
            [BigDecimal::valueOf("4.62857142E-52"), BigDecimal::valueOf("0.81E-25"), BigDecimal::valueOf("175000000000000000000000000"), 60, RoundingMode::DOWN()],
            [BigDecimal::valueOf("4.62857142E-52"), BigDecimal::valueOf("0.81E-25"), BigDecimal::valueOf("175E+24"), 60, RoundingMode::DOWN()],

            [BigDecimal::valueOf("1.185770750988142292490118577075"), BigDecimal::valueOf("300"), BigDecimal::valueOf("253"), 30, RoundingMode::DOWN()],
            [BigDecimal::valueOf("1.185770750E-51"), BigDecimal::valueOf("0.000000000000000000000000300"), BigDecimal::valueOf("253000000000000000000000000"), 60, RoundingMode::DOWN()],
            [BigDecimal::valueOf("1.185770750E-51"), BigDecimal::valueOf("0.000000000000000000000000300"), BigDecimal::valueOf("253E+24"), 60, RoundingMode::DOWN()],
            [BigDecimal::valueOf("1.185770750E-51"), BigDecimal::valueOf("0.300E-24"), BigDecimal::valueOf("253000000000000000000000000"), 60, RoundingMode::DOWN()],
            [BigDecimal::valueOf("1.185770750E-51"), BigDecimal::valueOf("0.300E-24"), BigDecimal::valueOf("253E+24"), 60, RoundingMode::DOWN()],

            [BigDecimal::valueOf("0.33"), BigDecimal::valueOf("1.00"), BigDecimal::valueOf("3"), null, RoundingMode::DOWN()],
            [BigDecimal::valueOf("0.0"), BigDecimal::valueOf("1.2"), BigDecimal::valueOf("17.1"), null, RoundingMode::DOWN()],


        ];
    }

    /**
     * @dataProvider divideIllegalDataProvider
     * @covers \Kamiyonanayo\Math\BigDecimal::divide
     */
    public function testDivideIllegal(int $error_mode, ?MathRuntimeException $exception, ?BigDecimal $expected, $left_operand, $right_operand, int $actual_scale, $rounding_mode)
    {

        BigDecimal::getContext()->setErrorMode($error_mode);
        $left = BigDecimal::valueOf($left_operand);
        self::initContextError();
        if ($error_mode == MathContext::ERROR_MODE_THROW_EXCEPTION) {
            $this->expectExceptionObject($exception);
            $r = $left->divide($right_operand, $actual_scale, $rounding_mode);
        } else {
            $r = $left->divide($right_operand, $actual_scale, $rounding_mode);
            if (is_null($exception)) {
                $this->assertContextErrorNull();
            } else {
                $this->assertSame(get_class($exception), get_class(BigDecimal::getContext()->getLastError()));
                $this->assertSame($exception->getMessage(), BigDecimal::getContext()->getLastError()->getMessage());
                $this->assertSame($exception->getCode(), BigDecimal::getContext()->getLastError()->getCode());
            }

            $this->assertSameBigDecimal($expected, $r);
        }
    }

    public static function divideIllegalDataProvider()
    {
        return [
            [MathContext::ERROR_MODE_RETURN_NAN, new NumberFormatException('does not support number "A"'), BigDecimal::NaN(), BigDecimal::One(), "A", 10, RoundingMode::UNNECESSARY],
            [MathContext::ERROR_MODE_RETURN_NULL, new NumberFormatException('does not support number "A"'), null, BigDecimal::One(), "A", 10, RoundingMode::UNNECESSARY],
            [MathContext::ERROR_MODE_THROW_EXCEPTION, new NumberFormatException('does not support number "A"'), null, BigDecimal::One(), "A", 10, RoundingMode::UNNECESSARY],

            [MathContext::ERROR_MODE_RETURN_NAN, new IllegalArgumentException('Invalid rounding mode "A"'), BigDecimal::NaN(), BigDecimal::One(), BigDecimal::One(), 10, "A"],
            [MathContext::ERROR_MODE_RETURN_NULL, new IllegalArgumentException('Invalid rounding mode "A"'), null, BigDecimal::One(), BigDecimal::One(), 10, "A"],
            [MathContext::ERROR_MODE_THROW_EXCEPTION, new IllegalArgumentException('Invalid rounding mode "A"'), null, BigDecimal::One(), BigDecimal::One(), 10, "A"],

            [MathContext::ERROR_MODE_RETURN_NAN, ArithmeticException::DivisionUndefined(), BigDecimal::NaN(), BigDecimal::valueOf("0"), BigDecimal::valueOf("0"), 10, RoundingMode::UNNECESSARY],
            [MathContext::ERROR_MODE_RETURN_NAN, ArithmeticException::DivisionByZero(), BigDecimal::NaN(), BigDecimal::valueOf("1.10"), BigDecimal::valueOf("0"), 10, RoundingMode::UNNECESSARY],
            [MathContext::ERROR_MODE_RETURN_NAN, ArithmeticException::DivisionByZero(), BigDecimal::NaN(), BigDecimal::valueOf("-1.10"), BigDecimal::valueOf("0"), 10, RoundingMode::UNNECESSARY],
            [MathContext::ERROR_MODE_RETURN_NAN, ArithmeticException::DivisionByZero(), BigDecimal::NaN(), BigDecimal::valueOf("INF"), BigDecimal::valueOf("0"), 10, RoundingMode::UNNECESSARY],
            [MathContext::ERROR_MODE_RETURN_NAN, ArithmeticException::DivisionByZero(), BigDecimal::NaN(), BigDecimal::valueOf("-INF"), BigDecimal::valueOf("0"), 10, RoundingMode::UNNECESSARY],

            [MathContext::ERROR_MODE_RETURN_NULL, ArithmeticException::DivisionUndefined(), null, BigDecimal::valueOf("0"), BigDecimal::valueOf("0"), 10, RoundingMode::UNNECESSARY],
            [MathContext::ERROR_MODE_RETURN_NULL, ArithmeticException::DivisionByZero(), null, BigDecimal::valueOf("1.10"), BigDecimal::valueOf("0"), 10, RoundingMode::UNNECESSARY],
            [MathContext::ERROR_MODE_RETURN_NULL, ArithmeticException::DivisionByZero(), null, BigDecimal::valueOf("-1.10"), BigDecimal::valueOf("0"), 10, RoundingMode::UNNECESSARY],
            [MathContext::ERROR_MODE_RETURN_NULL, ArithmeticException::DivisionByZero(), null, BigDecimal::valueOf("INF"), BigDecimal::valueOf("0"), 10, RoundingMode::UNNECESSARY],
            [MathContext::ERROR_MODE_RETURN_NULL, ArithmeticException::DivisionByZero(), null, BigDecimal::valueOf("-INF"), BigDecimal::valueOf("0"), 10, RoundingMode::UNNECESSARY],

            [MathContext::ERROR_MODE_THROW_EXCEPTION, ArithmeticException::DivisionUndefined(), null, BigDecimal::valueOf("0"), BigDecimal::valueOf("0"), 10, RoundingMode::UNNECESSARY],
            [MathContext::ERROR_MODE_THROW_EXCEPTION, ArithmeticException::DivisionByZero(), null, BigDecimal::valueOf("1.10"), BigDecimal::valueOf("0"), 10, RoundingMode::UNNECESSARY],
            [MathContext::ERROR_MODE_THROW_EXCEPTION, ArithmeticException::DivisionByZero(), null, BigDecimal::valueOf("-1.10"), BigDecimal::valueOf("0"), 10, RoundingMode::UNNECESSARY],
            [MathContext::ERROR_MODE_THROW_EXCEPTION, ArithmeticException::DivisionByZero(), null, BigDecimal::valueOf("INF"), BigDecimal::valueOf("0"), 10, RoundingMode::UNNECESSARY],
            [MathContext::ERROR_MODE_THROW_EXCEPTION, ArithmeticException::DivisionByZero(), null, BigDecimal::valueOf("-INF"), BigDecimal::valueOf("0"), 10, RoundingMode::UNNECESSARY],

            [MathContext::ERROR_MODE_RETURN_NAN, ArithmeticException::RoundingNecessary(), BigDecimal::NaN(), BigDecimal::One(), "3", 10, RoundingMode::UNNECESSARY],
            [MathContext::ERROR_MODE_RETURN_NULL, ArithmeticException::RoundingNecessary(), null, BigDecimal::One(), "3", 10, RoundingMode::UNNECESSARY],
            [MathContext::ERROR_MODE_THROW_EXCEPTION, ArithmeticException::RoundingNecessary(), null, BigDecimal::One(), "3", 10, RoundingMode::UNNECESSARY],

        ];
    }

    /**
     * @dataProvider exactlyDivideDataProvider
     * @covers \Kamiyonanayo\Math\BigDecimal::exactlyDivide
     */
    public function testExactlyDivide($expected, $left_operand, $right_operand)
    {
        $left = BigDecimal::valueOf($left_operand);
        self::initContextError();
        if (is_subclass_of($expected, \RuntimeException::class)) {
            BigDecimal::getContext()->setErrorMode(MathContext::ERROR_MODE_THROW_EXCEPTION);
            $this->expectException($expected);
        }
        $r = $left->exactlyDivide($right_operand);
        if (!is_subclass_of($expected, \RuntimeException::class)) {
            $this->assertContextErrorNull();
            $this->assertSameBigDecimal(BigDecimal::valueOf($expected), $r);
        }
    }

    public static function exactlyDivideDataProvider(): array
    {
        return [

            [ArithmeticException::class, BigDecimal::valueOf("0"), BigDecimal::valueOf("0")],
            [ArithmeticException::class, BigDecimal::valueOf("1.10"), BigDecimal::valueOf("0")],
            [ArithmeticException::class, BigDecimal::valueOf("-1.10"), BigDecimal::valueOf("0")],
            [BigDecimal::valueOf("NAN"), BigDecimal::valueOf("NAN"), BigDecimal::valueOf("0")],
            [ArithmeticException::class, BigDecimal::valueOf("INF"), BigDecimal::valueOf("0")],
            [ArithmeticException::class, BigDecimal::valueOf("-INF"), BigDecimal::valueOf("0")],

            [BigDecimal::valueOf("0E+2"), BigDecimal::valueOf("0"), BigDecimal::valueOf("1.10")],
            [BigDecimal::valueOf("1"), BigDecimal::valueOf("1.10"), BigDecimal::valueOf("1.10")],
            [BigDecimal::valueOf("-1"), BigDecimal::valueOf("-1.10"), BigDecimal::valueOf("1.10")],
            [BigDecimal::valueOf("NAN"), BigDecimal::valueOf("NAN"), BigDecimal::valueOf("1.10")],
            [BigDecimal::valueOf("INF"), BigDecimal::valueOf("INF"), BigDecimal::valueOf("1.10")],
            [BigDecimal::valueOf("-INF"), BigDecimal::valueOf("-INF"), BigDecimal::valueOf("1.10")],

            [BigDecimal::valueOf("0E+2"), BigDecimal::valueOf("0"), BigDecimal::valueOf("-1.10")],
            [BigDecimal::valueOf("-1"), BigDecimal::valueOf("1.10"), BigDecimal::valueOf("-1.10")],
            [BigDecimal::valueOf("1"), BigDecimal::valueOf("-1.10"), BigDecimal::valueOf("-1.10")],
            [BigDecimal::valueOf("NAN"), BigDecimal::valueOf("NAN"), BigDecimal::valueOf("-1.10")],
            [BigDecimal::valueOf("-INF"), BigDecimal::valueOf("INF"), BigDecimal::valueOf("-1.10")],
            [BigDecimal::valueOf("INF"), BigDecimal::valueOf("-INF"), BigDecimal::valueOf("-1.10")],

            [BigDecimal::valueOf("NAN"), BigDecimal::valueOf("0"), BigDecimal::valueOf("NAN")],
            [BigDecimal::valueOf("NAN"), BigDecimal::valueOf("1.10"), BigDecimal::valueOf("NAN")],
            [BigDecimal::valueOf("NAN"), BigDecimal::valueOf("-1.10"), BigDecimal::valueOf("NAN")],
            [BigDecimal::valueOf("NAN"), BigDecimal::valueOf("NAN"), BigDecimal::valueOf("NAN")],
            [BigDecimal::valueOf("NAN"), BigDecimal::valueOf("INF"), BigDecimal::valueOf("NAN")],
            [BigDecimal::valueOf("NAN"), BigDecimal::valueOf("-INF"), BigDecimal::valueOf("NAN")],

            [BigDecimal::valueOf("0E-0"), BigDecimal::valueOf("0"), BigDecimal::valueOf("INF")],
            [BigDecimal::valueOf("0E-0"), BigDecimal::valueOf("1.10"), BigDecimal::valueOf("INF")],
            [BigDecimal::valueOf("0E-0"), BigDecimal::valueOf("-1.10"), BigDecimal::valueOf("INF")],
            [BigDecimal::valueOf("NAN"), BigDecimal::valueOf("NAN"), BigDecimal::valueOf("INF")],
            [BigDecimal::valueOf("NAN"), BigDecimal::valueOf("INF"), BigDecimal::valueOf("INF")],
            [BigDecimal::valueOf("NAN"), BigDecimal::valueOf("-INF"), BigDecimal::valueOf("INF")],

            [BigDecimal::valueOf("0E-0"), BigDecimal::valueOf("0"), BigDecimal::valueOf("-INF")],
            [BigDecimal::valueOf("0E-0"), BigDecimal::valueOf("1.10"), BigDecimal::valueOf("-INF")],
            [BigDecimal::valueOf("0E-0"), BigDecimal::valueOf("-1.10"), BigDecimal::valueOf("-INF")],
            [BigDecimal::valueOf("NAN"), BigDecimal::valueOf("NAN"), BigDecimal::valueOf("-INF")],
            [BigDecimal::valueOf("NAN"), BigDecimal::valueOf("INF"), BigDecimal::valueOf("-INF")],
            [BigDecimal::valueOf("NAN"), BigDecimal::valueOf("-INF"), BigDecimal::valueOf("-INF")],

            ['1', 1, 1],
            ['1', '1.0', '1.00'],
            ['0.5', 1, 2],
            [ArithmeticException::class, 1, 3],
            ['0.25', 1, 4],
            ['0.2', 1, 5],
            [ArithmeticException::class, 1, 6],
            [ArithmeticException::class, 1, 7],
            ['0.125', 1, 8],
            [ArithmeticException::class, 1, 9],
            ['0.1', 1, 10],
            ['0.5', '1.0', 2],
            ['0.50', '1.00', 2],
            ['0.1250', '1.0000', 8],
            ['0.25', 1, '4.000'],
            ['8', '1', '0.125'],
            ['8', '1.0', '0.125'],
            ['617.2839', '1234.5678', '2'],
            ['308.64195', '1234.5678', '4'],
            ['154.320975', '1234.5678', '8'],
            ['192.90121875', '1234.5678', '6.4'],
            ['0.00224', '7', '3125'],
            ['13104400228.264126', '261411075767401404.6055825322879232', '19948343.3971727232'],
            ['268879387637.09088', '5363698357006206996.583638135484416', '19948343.3971727232'],
            ['29033.664563976628', '579173510800.5308386773419639133696', '19948343.3971727232'],
            ['39077744.97850056', '779536276018271.186198272867924992', '19948343.3971727232'],
            ['4.1922268260018992', '83627980.32392534864829247363590144', '19948343.3971727232'],

        ];
    }


    /**
     * @dataProvider exactlyDivideIllegalDataProvider
     * @covers \Kamiyonanayo\Math\BigDecimal::exactlyDivide
     */
    public function testExactlyDivideIllegal(int $error_mode, ?MathRuntimeException $exception, ?BigDecimal $expected, $left_operand, $right_operand)
    {

        BigDecimal::getContext()->setErrorMode($error_mode);
        $left = BigDecimal::valueOf($left_operand);
        self::initContextError();
        if ($error_mode == MathContext::ERROR_MODE_THROW_EXCEPTION) {
            $this->expectExceptionObject($exception);
            $r = $left->exactlyDivide($right_operand);
        } else {
            $r = $left->exactlyDivide($right_operand);
            if (is_null($exception)) {
                $this->assertContextErrorNull();
            } else {
                $this->assertSame(get_class($exception), get_class(BigDecimal::getContext()->getLastError()));
                $this->assertSame($exception->getMessage(), BigDecimal::getContext()->getLastError()->getMessage());
                $this->assertSame($exception->getCode(), BigDecimal::getContext()->getLastError()->getCode());
            }

            $this->assertSameBigDecimal($expected, $r);
        }
    }

    public static function exactlyDivideIllegalDataProvider()
    {
        return [
            [MathContext::ERROR_MODE_RETURN_NAN, new NumberFormatException('does not support number "A"'), BigDecimal::NaN(), BigDecimal::One(), "A"],
            [MathContext::ERROR_MODE_RETURN_NULL, new NumberFormatException('does not support number "A"'), null, BigDecimal::One(), "A"],
            [MathContext::ERROR_MODE_THROW_EXCEPTION, new NumberFormatException('does not support number "A"'), null, BigDecimal::One(), "A"],

            [MathContext::ERROR_MODE_RETURN_NAN, ArithmeticException::DivisionUndefined(), BigDecimal::NaN(), BigDecimal::valueOf("0"), BigDecimal::valueOf("0")],
            [MathContext::ERROR_MODE_RETURN_NAN, ArithmeticException::DivisionByZero(), BigDecimal::NaN(), BigDecimal::valueOf("1.10"), BigDecimal::valueOf("0")],
            [MathContext::ERROR_MODE_RETURN_NAN, ArithmeticException::DivisionByZero(), BigDecimal::NaN(), BigDecimal::valueOf("-1.10"), BigDecimal::valueOf("0")],
            [MathContext::ERROR_MODE_RETURN_NAN, ArithmeticException::DivisionByZero(), BigDecimal::NaN(), BigDecimal::valueOf("INF"), BigDecimal::valueOf("0")],
            [MathContext::ERROR_MODE_RETURN_NAN, ArithmeticException::DivisionByZero(), BigDecimal::NaN(), BigDecimal::valueOf("-INF"), BigDecimal::valueOf("0")],

            [MathContext::ERROR_MODE_RETURN_NULL, ArithmeticException::DivisionUndefined(), null, BigDecimal::valueOf("0"), BigDecimal::valueOf("0")],
            [MathContext::ERROR_MODE_RETURN_NULL, ArithmeticException::DivisionByZero(), null, BigDecimal::valueOf("1.10"), BigDecimal::valueOf("0")],
            [MathContext::ERROR_MODE_RETURN_NULL, ArithmeticException::DivisionByZero(), null, BigDecimal::valueOf("-1.10"), BigDecimal::valueOf("0")],
            [MathContext::ERROR_MODE_RETURN_NULL, ArithmeticException::DivisionByZero(), null, BigDecimal::valueOf("INF"), BigDecimal::valueOf("0")],
            [MathContext::ERROR_MODE_RETURN_NULL, ArithmeticException::DivisionByZero(), null, BigDecimal::valueOf("-INF"), BigDecimal::valueOf("0")],

            [MathContext::ERROR_MODE_THROW_EXCEPTION, ArithmeticException::DivisionUndefined(), null, BigDecimal::valueOf("0"), BigDecimal::valueOf("0")],
            [MathContext::ERROR_MODE_THROW_EXCEPTION, ArithmeticException::DivisionByZero(), null, BigDecimal::valueOf("1.10"), BigDecimal::valueOf("0")],
            [MathContext::ERROR_MODE_THROW_EXCEPTION, ArithmeticException::DivisionByZero(), null, BigDecimal::valueOf("-1.10"), BigDecimal::valueOf("0")],
            [MathContext::ERROR_MODE_THROW_EXCEPTION, ArithmeticException::DivisionByZero(), null, BigDecimal::valueOf("INF"), BigDecimal::valueOf("0")],
            [MathContext::ERROR_MODE_THROW_EXCEPTION, ArithmeticException::DivisionByZero(), null, BigDecimal::valueOf("-INF"), BigDecimal::valueOf("0")],

            [MathContext::ERROR_MODE_RETURN_NAN, new ArithmeticException("Non-terminating decimal expansion; no exact representable decimal result."), BigDecimal::NaN(), BigDecimal::One(), "3"],
            [MathContext::ERROR_MODE_RETURN_NULL, new ArithmeticException("Non-terminating decimal expansion; no exact representable decimal result."), null, BigDecimal::One(), "3"],
            [MathContext::ERROR_MODE_THROW_EXCEPTION, new ArithmeticException("Non-terminating decimal expansion; no exact representable decimal result."), null, BigDecimal::One(), "3"],

        ];
    }

    /**
     * @dataProvider divideAndRemainderDataProvider
     * @covers \Kamiyonanayo\Math\BigDecimal::divideAndRemainder
     */
    public function testDivideAndRemainder($expected_quotient_or_exception, $expected_remainder, $left_operand, $right_operand)
    {
        $left = BigDecimal::valueOf($left_operand);
        self::initContextError();
        if (is_subclass_of($expected_quotient_or_exception, \RuntimeException::class)) {
            BigDecimal::getContext()->setErrorMode(MathContext::ERROR_MODE_RETURN_NAN);
            $r = $left->divideAndRemainder($right_operand);

            $this->assertSameMathRuntimeException($expected_quotient_or_exception, BigDecimal::getContext()->getLastError());

            $this->assertSameBigDecimal(BigDecimal::NaN(), $r[0]);
            $this->assertSameBigDecimal(BigDecimal::NaN(), $r[1]);
        } else {
            $r = $left->divideAndRemainder($right_operand);
            $this->assertContextErrorNull();
            $this->assertSameBigDecimal(BigDecimal::valueOf($expected_quotient_or_exception), $r[0]);
            $this->assertSameBigDecimal(BigDecimal::valueOf($expected_remainder), $r[1]);
        }
    }

    /**
     * @dataProvider divideAndRemainderDataProvider
     * @covers \Kamiyonanayo\Math\BigDecimal::divideToIntegralValue
     */
    public function testDivideToIntegralValue($expected_quotient_or_exception, $expected_remainder, $left_operand, $right_operand)
    {
        $left = BigDecimal::valueOf($left_operand);
        self::initContextError();
        if (is_subclass_of($expected_quotient_or_exception, \RuntimeException::class)) {
            BigDecimal::getContext()->setErrorMode(MathContext::ERROR_MODE_RETURN_NAN);
            $r = $left->divideToIntegralValue($right_operand);

            $this->assertSameMathRuntimeException($expected_quotient_or_exception, BigDecimal::getContext()->getLastError());

            $this->assertSameBigDecimal(BigDecimal::NaN(), $r);
        } else {
            $r = $left->divideToIntegralValue($right_operand);
            $this->assertContextErrorNull();
            $this->assertSameBigDecimal(BigDecimal::valueOf($expected_quotient_or_exception), $r);
        }
    }

    /**
     * @dataProvider divideAndRemainderDataProvider
     * @covers \Kamiyonanayo\Math\BigDecimal::remainder
     */
    public function testRemainder($expected_quotient_or_exception, $expected_remainder, $left_operand, $right_operand)
    {
        $left = BigDecimal::valueOf($left_operand);
        self::initContextError();
        if (is_subclass_of($expected_quotient_or_exception, \RuntimeException::class)) {
            BigDecimal::getContext()->setErrorMode(MathContext::ERROR_MODE_RETURN_NAN);
            $r = $left->remainder($right_operand);

            $this->assertSameMathRuntimeException($expected_quotient_or_exception, BigDecimal::getContext()->getLastError());

            $this->assertSameBigDecimal(BigDecimal::NaN(), $r);
        } else {
            $r = $left->remainder($right_operand);
            $this->assertContextErrorNull();
            $this->assertSameBigDecimal(BigDecimal::valueOf($expected_remainder), $r);
        }
    }

    public static function divideAndRemainderDataProvider(): array
    {
        return [

            [ArithmeticException::DivisionUndefined(), null, BigDecimal::valueOf("0"), BigDecimal::valueOf("0")],
            [ArithmeticException::DivisionByZero(), null, BigDecimal::valueOf("1.10"), BigDecimal::valueOf("0")],
            [ArithmeticException::DivisionByZero(), null, BigDecimal::valueOf("-1.10"), BigDecimal::valueOf("0")],
            [BigDecimal::valueOf("NAN"), BigDecimal::valueOf("NAN"), BigDecimal::valueOf("NAN"), BigDecimal::valueOf("0")],
            [ArithmeticException::DivisionByZero(), null, BigDecimal::valueOf("INF"), BigDecimal::valueOf("0")],
            [ArithmeticException::DivisionByZero(), null, BigDecimal::valueOf("-INF"), BigDecimal::valueOf("0")],

            [BigDecimal::valueOf("0.0E+3"), BigDecimal::valueOf("0"), BigDecimal::valueOf("0"), BigDecimal::valueOf("1.10")],
            [BigDecimal::valueOf("1"), BigDecimal::valueOf("0.00"), BigDecimal::valueOf("1.10"), BigDecimal::valueOf("1.10")],
            [BigDecimal::valueOf("-1"), BigDecimal::valueOf("0.00"), BigDecimal::valueOf("-1.10"), BigDecimal::valueOf("1.10")],
            [BigDecimal::valueOf("NAN"), BigDecimal::valueOf("NAN"), BigDecimal::valueOf("NAN"), BigDecimal::valueOf("1.10")],
            [BigDecimal::valueOf("INF"), BigDecimal::valueOf("NAN"), BigDecimal::valueOf("INF"), BigDecimal::valueOf("1.10")],
            [BigDecimal::valueOf("-INF"), BigDecimal::valueOf("NAN"), BigDecimal::valueOf("-INF"), BigDecimal::valueOf("1.10")],

            [BigDecimal::valueOf("0.0E+3"), BigDecimal::valueOf("0"), BigDecimal::valueOf("0"), BigDecimal::valueOf("-1.10")],
            [BigDecimal::valueOf("-1"), BigDecimal::valueOf("0.00"), BigDecimal::valueOf("1.10"), BigDecimal::valueOf("-1.10")],
            [BigDecimal::valueOf("1"), BigDecimal::valueOf("0.00"), BigDecimal::valueOf("-1.10"), BigDecimal::valueOf("-1.10")],
            [BigDecimal::valueOf("NAN"), BigDecimal::valueOf("NAN"), BigDecimal::valueOf("NAN"), BigDecimal::valueOf("-1.10")],
            [BigDecimal::valueOf("-INF"), BigDecimal::valueOf("NAN"), BigDecimal::valueOf("INF"), BigDecimal::valueOf("-1.10")],
            [BigDecimal::valueOf("INF"), BigDecimal::valueOf("NAN"), BigDecimal::valueOf("-INF"), BigDecimal::valueOf("-1.10")],

            [BigDecimal::valueOf("NAN"), BigDecimal::valueOf("NAN"), BigDecimal::valueOf("0"), BigDecimal::valueOf("NAN")],
            [BigDecimal::valueOf("NAN"), BigDecimal::valueOf("NAN"), BigDecimal::valueOf("1.10"), BigDecimal::valueOf("NAN")],
            [BigDecimal::valueOf("NAN"), BigDecimal::valueOf("NAN"), BigDecimal::valueOf("-1.10"), BigDecimal::valueOf("NAN")],
            [BigDecimal::valueOf("NAN"), BigDecimal::valueOf("NAN"), BigDecimal::valueOf("NAN"), BigDecimal::valueOf("NAN")],
            [BigDecimal::valueOf("NAN"), BigDecimal::valueOf("NAN"), BigDecimal::valueOf("INF"), BigDecimal::valueOf("NAN")],
            [BigDecimal::valueOf("NAN"), BigDecimal::valueOf("NAN"), BigDecimal::valueOf("-INF"), BigDecimal::valueOf("NAN")],

            [BigDecimal::valueOf("0E-0"), BigDecimal::valueOf("0E-0"), BigDecimal::valueOf("0"), BigDecimal::valueOf("INF")],
            [BigDecimal::valueOf("0E-0"), BigDecimal::valueOf("1.10"), BigDecimal::valueOf("1.10"), BigDecimal::valueOf("INF")],
            [BigDecimal::valueOf("0E-0"), BigDecimal::valueOf("-1.10"), BigDecimal::valueOf("-1.10"), BigDecimal::valueOf("INF")],
            [BigDecimal::valueOf("NAN"), BigDecimal::valueOf("NAN"), BigDecimal::valueOf("NAN"), BigDecimal::valueOf("INF")],
            [BigDecimal::valueOf("NAN"), BigDecimal::valueOf("NAN"), BigDecimal::valueOf("INF"), BigDecimal::valueOf("INF")],
            [BigDecimal::valueOf("NAN"), BigDecimal::valueOf("NAN"), BigDecimal::valueOf("-INF"), BigDecimal::valueOf("INF")],

            [BigDecimal::valueOf("0E-0"), BigDecimal::valueOf("0E-0"), BigDecimal::valueOf("0"), BigDecimal::valueOf("-INF")],
            [BigDecimal::valueOf("0E-0"), BigDecimal::valueOf("1.10"), BigDecimal::valueOf("1.10"), BigDecimal::valueOf("-INF")],
            [BigDecimal::valueOf("0E-0"), BigDecimal::valueOf("-1.10"), BigDecimal::valueOf("-1.10"), BigDecimal::valueOf("-INF")],
            [BigDecimal::valueOf("NAN"), BigDecimal::valueOf("NAN"), BigDecimal::valueOf("NAN"), BigDecimal::valueOf("-INF")],
            [BigDecimal::valueOf("NAN"), BigDecimal::valueOf("NAN"), BigDecimal::valueOf("INF"), BigDecimal::valueOf("-INF")],
            [BigDecimal::valueOf("NAN"), BigDecimal::valueOf("NAN"), BigDecimal::valueOf("-INF"), BigDecimal::valueOf("-INF")],

            ['0', '1', '1', '3'],
            ['0', '-1', '-1', '3'],
            ['0', '1', '1', '-3'],
            ['0', '-1', '-1', '-3'],

            ['0', '1', '1', '123'],
            ['0', '1', '1', '-123'],
            ['0', '-1', '-1', '123'],
            ['0', '-1', '-1', '-123'],

            ['0', '1999999999999999999999999', '1999999999999999999999999', '2000000000000000000000000'],
            ['0', '1999999999999999999999999', '1999999999999999999999999', '-2000000000000000000000000'],
            ['0', '-1999999999999999999999999', '-1999999999999999999999999', '2000000000000000000000000'],
            ['0', '-1999999999999999999999999', '-1999999999999999999999999', '-2000000000000000000000000'],

            ['123', '0', '123', '1'],
            ['-123', '0', '123', '-1'],
            ['-123', '0', '-123', '1'],
            ['123', '0', '-123', '-1'],

            ['61', '1', '123', '2'],
            ['-61', '1', '123', '-2'],
            ['-61', '-1', '-123', '2'],
            ['61', '-1', '-123', '-2'],

            ['1', '0', '123', '123'],
            ['-1', '0', '123', '-123'],
            ['-1', '0', '-123', '123'],
            ['1', '0', '-123', '-123'],

            ['0', '123', '123', '124'],
            ['0', '123', '123', '-124'],
            ['0', '-123', '-123', '124'],
            ['0', '-123', '-123', '-124'],

            ['1', '1', '124', '123'],
            ['-1', '1', '124', '-123'],
            ['-1', '-1', '-124', '123'],
            ['1', '-1', '-124', '-123'],

            ['333333333333333333333333333333', '1', '1000000000000000000000000000000', '3'],
            ['111111111111111111111111111111', '1', '1000000000000000000000000000000', '9'],
            ['90909090909090909090909090909', '1', '1000000000000000000000000000000', '11'],
            ['76923076923076923076923076923', '1', '1000000000000000000000000000000', '13'],
            ['47619047619047619047619047619', '1', '1000000000000000000000000000000', '21'],

            ['124999998', '850308642973765431', '123456789123456789123456789', '987654321987654321'],
            ['-1408450676', '65623397056685793', '123456789123456789123456789', '-87654321987654321'],
            ['-16129030020', '-1834176331740369', '-123456789123456789123456789', '7654321987654321'],
            ['188678955396', '-205094497790673', '-123456789123456789123456789', '-654321987654321'],

            ['3.0', '0.21', '10.11', '3.3'],
            ['-769', '0.0003', '1', '-0.0013'],
            ['-1075449', '-0.0000002109080127582569', '-1.000000000000000000001', '0.0000009298439898981609'],
            ['23903344746475158719036', '-30.0786684482104867175202241524', '-1278438782896060000132323.32333', '-53.4836775545640521556878910541'],
            ['-4.7999186945745974996694207816418774859692752E+45', '0.0', '23999593472872987498347103908209387429846376', '-0.005'],

            ['333333333333333333333333333333.0', '1.0', '1000000000000000000000000000000.0', '3'],
            ['111111111111111111111111111111.0', '1.0', '1000000000000000000000000000000.0', '9'],
            ['90909090909090909090909090909.0', '1.0', '1000000000000000000000000000000.0', '11'],
            ['76923076923076923076923076923.0', '1.0', '1000000000000000000000000000000.0', '13'],
            ['4.00000000000000000000000000000', '0.1599999999999999999999999999999', '0.9999999999999999999999999999999', '0.21'],

            ['256410256410256410256410256410', '1.0', '1000000000000000000000000000000.0', '3.9'],
            ['-102040816326530612244897959183', '-6.6', '-1000000000000000000000000000000.0', '9.8'],
            ['-85470085470085470085470085470', '1.0', '1000000000000000000000000000000.0', '-11.7'],
            ['72992700729927007299270072992', '-9.6', '-1000000000000000000000000000000.0', '-13.7'],
            ['4.00000000000000000000000000000', '0.13999999999999999999999999999999', '0.99999999999999999999999999999999', '0.215'],

        ];
    }

    /**
     * @dataProvider divideAndRemainderIllegalDataProvider
     * @covers \Kamiyonanayo\Math\BigDecimal::divideAndRemainder
     */
    public function testDivideAndRemainderIllegal(int $error_mode, ?MathRuntimeException $exception, ?BigDecimal $expected_quotient, ?BigDecimal $expected_remainder, $left_operand, $right_operand)
    {

        BigDecimal::getContext()->setErrorMode($error_mode);
        $left = BigDecimal::valueOf($left_operand);
        self::initContextError();
        if ($error_mode == MathContext::ERROR_MODE_THROW_EXCEPTION) {
            $this->expectExceptionObject($exception);
            $r = $left->divideAndRemainder($right_operand);
        } else {
            $r = $left->divideAndRemainder($right_operand);
            if (is_null($exception)) {
                $this->assertContextErrorNull();
            } else {
                $this->assertSame(get_class($exception), get_class(BigDecimal::getContext()->getLastError()));
                $this->assertSame($exception->getMessage(), BigDecimal::getContext()->getLastError()->getMessage());
                $this->assertSame($exception->getCode(), BigDecimal::getContext()->getLastError()->getCode());
            }
            $this->assertSameBigDecimal($expected_quotient, $r[0]);
            $this->assertSameBigDecimal($expected_remainder, $r[1]);
        }
    }

    /**
     * @dataProvider divideAndRemainderIllegalDataProvider
     * @covers \Kamiyonanayo\Math\BigDecimal::divideToIntegralValue
     */
    public function testDivideToIntegralValueIllegal(int $error_mode, ?MathRuntimeException $exception, ?BigDecimal $expected_quotient, ?BigDecimal $expected_remainder, $left_operand, $right_operand)
    {

        BigDecimal::getContext()->setErrorMode($error_mode);
        $left = BigDecimal::valueOf($left_operand);
        self::initContextError();
        if ($error_mode == MathContext::ERROR_MODE_THROW_EXCEPTION) {
            $this->expectExceptionObject($exception);
            $r = $left->divideToIntegralValue($right_operand);
        } else {
            $r = $left->divideToIntegralValue($right_operand);
            if (is_null($exception)) {
                $this->assertContextErrorNull();
            } else {
                $this->assertSame(get_class($exception), get_class(BigDecimal::getContext()->getLastError()));
                $this->assertSame($exception->getMessage(), BigDecimal::getContext()->getLastError()->getMessage());
                $this->assertSame($exception->getCode(), BigDecimal::getContext()->getLastError()->getCode());
            }
            $this->assertSameBigDecimal($expected_quotient, $r);
        }
    }

    /**
     * @dataProvider divideAndRemainderIllegalDataProvider
     * @covers \Kamiyonanayo\Math\BigDecimal::remainder
     */
    public function testRemainderIllegal(int $error_mode, ?MathRuntimeException $exception, ?BigDecimal $expected_quotient, ?BigDecimal $expected_remainder, $left_operand, $right_operand)
    {

        BigDecimal::getContext()->setErrorMode($error_mode);
        $left = BigDecimal::valueOf($left_operand);
        self::initContextError();
        if ($error_mode == MathContext::ERROR_MODE_THROW_EXCEPTION) {
            $this->expectExceptionObject($exception);
            $r = $left->remainder($right_operand);
        } else {
            $r = $left->remainder($right_operand);
            if (is_null($exception)) {
                $this->assertContextErrorNull();
            } else {
                $this->assertSame(get_class($exception), get_class(BigDecimal::getContext()->getLastError()));
                $this->assertSame($exception->getMessage(), BigDecimal::getContext()->getLastError()->getMessage());
                $this->assertSame($exception->getCode(), BigDecimal::getContext()->getLastError()->getCode());
            }
            $this->assertSameBigDecimal($expected_remainder, $r);
        }
    }

    public static function divideAndRemainderIllegalDataProvider()
    {
        return [
            [MathContext::ERROR_MODE_RETURN_NAN, new NumberFormatException('does not support number "A"'), BigDecimal::NaN(), BigDecimal::NaN(), BigDecimal::One(), "A"],
            [MathContext::ERROR_MODE_RETURN_NULL, new NumberFormatException('does not support number "A"'), null, null, BigDecimal::One(), "A"],
            [MathContext::ERROR_MODE_THROW_EXCEPTION, new NumberFormatException('does not support number "A"'), null, null, BigDecimal::One(), "A"],

            [MathContext::ERROR_MODE_RETURN_NAN, ArithmeticException::DivisionUndefined(), BigDecimal::NaN(), BigDecimal::NaN(), BigDecimal::valueOf("0"), BigDecimal::valueOf("0")],
            [MathContext::ERROR_MODE_RETURN_NAN, ArithmeticException::DivisionByZero(), BigDecimal::NaN(), BigDecimal::NaN(), BigDecimal::valueOf("1.10"), BigDecimal::valueOf("0")],
            [MathContext::ERROR_MODE_RETURN_NAN, ArithmeticException::DivisionByZero(), BigDecimal::NaN(), BigDecimal::NaN(), BigDecimal::valueOf("-1.10"), BigDecimal::valueOf("0")],
            [MathContext::ERROR_MODE_RETURN_NAN, ArithmeticException::DivisionByZero(), BigDecimal::NaN(), BigDecimal::NaN(), BigDecimal::valueOf("INF"), BigDecimal::valueOf("0")],
            [MathContext::ERROR_MODE_RETURN_NAN, ArithmeticException::DivisionByZero(), BigDecimal::NaN(), BigDecimal::NaN(), BigDecimal::valueOf("-INF"), BigDecimal::valueOf("0")],

            [MathContext::ERROR_MODE_RETURN_NULL, ArithmeticException::DivisionUndefined(), null, null, BigDecimal::valueOf("0"), BigDecimal::valueOf("0")],
            [MathContext::ERROR_MODE_RETURN_NULL, ArithmeticException::DivisionByZero(), null, null, BigDecimal::valueOf("1.10"), BigDecimal::valueOf("0")],
            [MathContext::ERROR_MODE_RETURN_NULL, ArithmeticException::DivisionByZero(), null, null, BigDecimal::valueOf("-1.10"), BigDecimal::valueOf("0")],
            [MathContext::ERROR_MODE_RETURN_NULL, ArithmeticException::DivisionByZero(), null, null, BigDecimal::valueOf("INF"), BigDecimal::valueOf("0")],
            [MathContext::ERROR_MODE_RETURN_NULL, ArithmeticException::DivisionByZero(), null, null, BigDecimal::valueOf("-INF"), BigDecimal::valueOf("0")],

            [MathContext::ERROR_MODE_THROW_EXCEPTION, ArithmeticException::DivisionUndefined(), null, null, BigDecimal::valueOf("0"), BigDecimal::valueOf("0")],
            [MathContext::ERROR_MODE_THROW_EXCEPTION, ArithmeticException::DivisionByZero(), null, null, BigDecimal::valueOf("1.10"), BigDecimal::valueOf("0")],
            [MathContext::ERROR_MODE_THROW_EXCEPTION, ArithmeticException::DivisionByZero(), null, null, BigDecimal::valueOf("-1.10"), BigDecimal::valueOf("0")],
            [MathContext::ERROR_MODE_THROW_EXCEPTION, ArithmeticException::DivisionByZero(), null, null, BigDecimal::valueOf("INF"), BigDecimal::valueOf("0")],
            [MathContext::ERROR_MODE_THROW_EXCEPTION, ArithmeticException::DivisionByZero(), null, null, BigDecimal::valueOf("-INF"), BigDecimal::valueOf("0")],

        ];
    }



    /**
     * @dataProvider stripTrailingZerosDataProvider
     * @covers \Kamiyonanayo\Math\BigDecimal::stripTrailingZeros
     */
    public function testStripTrailingZeros(?string $expected_value, ?int $expected_scale, $testdata)
    {
        $actual_decimal = BigDecimal::valueOf($testdata);
        self::initContextError();
        $actual = $actual_decimal->stripTrailingZeros();
        $this->assertSameInitContextError();
        $this->assertSameBigDecimalValue($expected_value, $expected_scale, $actual);
    }

    public static function stripTrailingZerosDataProvider()
    {
        return [
            ["0", 0, "0"],
            ["0", 0, "+0"],
            ["0", 0, "-0"],
            ["0", 0, "0.0"],
            ["0", 0, "+0.0"],
            ["0", 0, "-0.0"],
            ["NAN", 0, "NAN"],
            ["INF", 0, "INF"],
            ["INF", 0, "+INF"],
            ["-INF", 0, "-INF"],
            ["1", 0, "1"],
            ["1", 0, "+1"],
            ["-1", 0, "-1"],
            ["1", 1, "0.1"],
            ["1", 1, "+0.1"],
            ["-1", 1, "-0.1"],
            ["1", 2, "0.010"],
            ["1", 2, "+0.010"],
            ["-1", 2, "-0.010"],
            ["-1", 1, "-0.10000000"],
            ["9223372036854775808123456789", 9, "9223372036854775808.123456789000"],
            ["9223372036854775808123456789", 9, "+9223372036854775808.123456789000"],
            ["-9223372036854775808123456789", 9, "-9223372036854775808.123456789000"],
            ["1", -1, "10.000"],
            ["10001", 3, "10.001"],
            ["10001", 3, "10.0010"],
            ["10001", 3, "10.00100"],
            ["1", -5, "100000.000000000"],
            ["1", -6, "1000000.000000000"],
            ["1", -7, "10000000.000000000"],
            ["1", -8, "100000000.000000000"],
            ["1", -9, "1000000000.000000000"],
            ["1", -10, "10000000000.000000000"],
            ["1", -11, "100000000000.000000000"],
        ];
    }

    /**
     * @dataProvider setScaleDataProvider
     * @covers \Kamiyonanayo\Math\BigDecimal::setScale
     */
    public function testSetScale($expected, $actual, int $actual_scale, $rounding_mode)
    {
        BigDecimal::getContext()->setErrorMode(MathContext::ERROR_MODE_RETURN_NULL);
        $actual_decimal = BigDecimal::valueOf($actual);
        self::initContextError();
        $r = $actual_decimal->setScale($actual_scale, $rounding_mode);
        if ($expected === "ArithmeticException") {
            $act = BigDecimal::getContext()->getLastError();
            $this->assertNull($r);
            $exp = ArithmeticException::RoundingNecessary();
            $this->assertInstanceOf(ArithmeticException::class, $act);
            $this->assertSame($exp->getMessage(), $act->getMessage());
            $this->assertSame($exp->getCode(), $act->getCode());
        } else {
            $this->assertContextErrorNull();
            $this->assertSameBigDecimal(BigDecimal::valueOf($expected), $r);
        }
    }

    public static function setScaleDataProvider()
    {
        $ret = [];
        $csv_data = UnitTestHelper::getCsvData(__DIR__ . "/BigDecimalTestSetScaleDataProvider.csv");
        $header2index = array_flip($csv_data[0]);
        $rounds = explode(",", "UP,DOWN,CEILING,FLOOR,HALF_UP,HALF_DOWN,HALF_EVEN,HALF_ODD,HALF_CEILING,HALF_FLOOR,UNNECESSARY");
        foreach (array_slice($csv_data, 1) as $row) {
            $actual = $row[$header2index["INPUT"]];
            $actual_scale = (int)$row[$header2index["SCALE"]];
            foreach ($rounds as $round) {
                $expected = $row[$header2index[$round]];
                $ret[] = [$expected, $actual, $actual_scale, strtolower($round)];
            }
        };

        $ret[] = [BigDecimal::valueOf("0"), BigDecimal::valueOf("0"), 0, RoundingMode::DOWN()];
        $ret[] = [BigDecimal::valueOf("1"), BigDecimal::valueOf("1.10"), 0, RoundingMode::DOWN()];
        $ret[] = [BigDecimal::valueOf("-1"), BigDecimal::valueOf("-1.10"), 0, RoundingMode::DOWN()];
        $ret[] = [BigDecimal::valueOf("NAN"), BigDecimal::valueOf("NAN"), 0, RoundingMode::DOWN()];
        $ret[] = [BigDecimal::valueOf("INF"), BigDecimal::valueOf("INF"), 0, RoundingMode::DOWN()];
        $ret[] = [BigDecimal::valueOf("-INF"), BigDecimal::valueOf("-INF"), 0, RoundingMode::DOWN()];

        return $ret;
    }

    /**
     * @dataProvider setScaleIllegalDataProvider
     * @covers \Kamiyonanayo\Math\BigDecimal::setScale
     */
    public function testSetScaleIllegal(int $error_mode, ?MathRuntimeException $exception, $expected, $actual, int $actual_scale, $rounding_mode)
    {

        BigDecimal::getContext()->setErrorMode($error_mode);
        $actual_decimal = BigDecimal::valueOf($actual);
        self::initContextError();
        if ($error_mode == MathContext::ERROR_MODE_THROW_EXCEPTION) {
            $this->expectExceptionObject($exception);
            $r = $actual_decimal->setScale($actual_scale, $rounding_mode);
        } else {
            $r = $actual_decimal->setScale($actual_scale, $rounding_mode);
            if (is_null($exception)) {
                $this->assertContextErrorNull();
            } else {
                $this->assertSame(get_class($exception), get_class(BigDecimal::getContext()->getLastError()));
                $this->assertSame($exception->getMessage(), BigDecimal::getContext()->getLastError()->getMessage());
                $this->assertSame($exception->getCode(), BigDecimal::getContext()->getLastError()->getCode());
            }

            $this->assertSameBigDecimal($expected, $r);
        }
    }

    public static function setScaleIllegalDataProvider()
    {
        return [
            [MathContext::ERROR_MODE_RETURN_NAN, new IllegalArgumentException('Invalid rounding mode "A"'), BigDecimal::NaN(), BigDecimal::One(), 10, "A"],
            [MathContext::ERROR_MODE_RETURN_NULL, new IllegalArgumentException('Invalid rounding mode "A"'), null, BigDecimal::One(), 10, "A"],
            [MathContext::ERROR_MODE_THROW_EXCEPTION, new IllegalArgumentException('Invalid rounding mode "A"'), null, BigDecimal::One(), 10, "A"],
        ];
    }

    /**
     * @dataProvider negateDataProvider
     * @covers \Kamiyonanayo\Math\BigDecimal::negate
     */
    public function testNegate(?string $expected_value, ?int $expected_scale, $actual)
    {
        $actual_decimal = BigDecimal::valueOf($actual);
        self::initContextError();
        $r = $actual_decimal->negate();
        $this->assertSameInitContextError();
        $this->assertSameBigDecimalValue($expected_value, $expected_scale, $r);
    }

    public static function negateDataProvider()
    {
        return [
            ["0", 0, "0"],
            ["0", 0, "+0"],
            ["0", 0, "-0"],
            ["0", 1, "0.0"],
            ["0", 1, "+0.0"],
            ["0", 1, "-0.0"],
            ["NAN", 0, "NAN"],
            ["-INF", 0, "INF"],
            ["-INF", 0, "+INF"],
            ["INF", 0, "-INF"],
            ["-1", 0, "1"],
            ["-1", 0, "+1"],
            ["1", 0, "-1"],
            ["-1", 1, "0.1"],
            ["-1", 1, "+0.1"],
            ["1", 1, "-0.1"],
            ["10000000", 8, "-0.10000000"],
            ["-9223372036854775808123456789000", 12, "9223372036854775808.123456789000"],
            ["-9223372036854775808123456789000", 12, "+9223372036854775808.123456789000"],
            ["9223372036854775808123456789000", 12, "-9223372036854775808.123456789000"],
        ];
    }

    /**
     * @covers \Kamiyonanayo\Math\BigDecimal::negate
     */
    public function testNegateIllegal()
    {
        $actual_decimal = BigDecimal::valueOf("1.23");
        UnitTestHelper::setPropValue($actual_decimal, "sign", 999);
        self::initContextError();
        $r = $actual_decimal->negate();
        $this->assertSameInitContextError();
        $this->assertSame($actual_decimal, $r);
    }

    /**
     * @dataProvider absDataProvider
     * @covers \Kamiyonanayo\Math\BigDecimal::abs
     */
    public function testAbs(?string $expected_value, ?int $expected_scale, $actual)
    {
        $actual_decimal = BigDecimal::valueOf($actual);
        self::initContextError();
        $r = $actual_decimal->abs();
        $this->assertSameInitContextError();
        $this->assertSameBigDecimalValue($expected_value, $expected_scale, $r);
    }

    public static function absDataProvider()
    {
        return [
            ["0", 0, "0"],
            ["0", 0, "+0"],
            ["0", 0, "-0"],
            ["0", 1, "0.0"],
            ["0", 1, "+0.0"],
            ["0", 1, "-0.0"],
            ["NAN", 0, "NAN"],
            ["INF", 0, "INF"],
            ["INF", 0, "+INF"],
            ["INF", 0, "-INF"],
            ["1", 0, "1"],
            ["1", 0, "+1"],
            ["1", 0, "-1"],
            ["1", 1, "0.1"],
            ["1", 1, "+0.1"],
            ["1", 1, "-0.1"],
            ["10000000", 8, "-0.10000000"],
            ["9223372036854775808123456789000", 12, "9223372036854775808.123456789000"],
            ["9223372036854775808123456789000", 12, "+9223372036854775808.123456789000"],
            ["9223372036854775808123456789000", 12, "-9223372036854775808.123456789000"],
        ];
    }

    /**
     * @dataProvider scaleprecisionDataProvider
     * @covers \Kamiyonanayo\Math\BigDecimal::precision
     */
    public function testPrecision($precision_expected, $scale_expected, $testdata)
    {
        $actual_decimal = BigDecimal::valueOf($testdata);
        self::initContextError();
        $actual = $actual_decimal->precision();
        $this->assertSameInitContextError();
        $this->assertSame($precision_expected, $actual);
    }

    /**
     * @dataProvider scaleprecisionDataProvider
     * @covers \Kamiyonanayo\Math\BigDecimal::scale
     */
    public function testScale($precision_expected, $scale_expected, $testdata)
    {
        $actual_decimal = BigDecimal::valueOf($testdata);
        self::initContextError();
        $actual = $actual_decimal->scale();
        $this->assertSameInitContextError();
        $this->assertSame($scale_expected, $actual);
    }

    public static function scaleprecisionDataProvider()
    {
        return [
            [1, 0, "0"],
            [1, 0, "+0"],
            [1, 0, "-0"],
            [1, 1, "0.0"],
            [1, 1, "+0.0"],
            [1, 1, "-0.0"],
            [0, 0, "NAN"],
            [0, 0, "INF"],
            [0, 0, "+INF"],
            [0, 0, "-INF"],
            [1, 0, "1"],
            [1, 0, "+1"],
            [1, 0, "-1"],
            [1, 1, "0.1"],
            [1, 1, "+0.1"],
            [1, 1, "-0.1"],
            [8, 8, "0.10000000"],
            [8, 8, "-0.10000000"],
            [31, 12, "9223372036854775808.123456789000"],
            [31, 12, "+9223372036854775808.123456789000"],
            [31, 12, "-9223372036854775808.123456789000"],
        ];
    }

    /**
     * @dataProvider signDataProvider
     * @covers \Kamiyonanayo\Math\BigDecimal::sign
     */
    public function testSign($expected, $actual)
    {
        $actual_decimal = BigDecimal::valueOf($actual);
        self::initContextError();
        $r = $actual_decimal->sign();
        $this->assertSameInitContextError();
        $this->assertSame($expected, $r);
    }

    public static function signDataProvider()
    {
        return [
            [BigDecimal::SIGN_POSITIVE_ZERO, "0"],
            [BigDecimal::SIGN_POSITIVE_ZERO, "+0"],
            [BigDecimal::SIGN_POSITIVE_ZERO, "-0"],
            [BigDecimal::SIGN_POSITIVE_ZERO, "0.0"],
            [BigDecimal::SIGN_POSITIVE_ZERO, "+0.0"],
            [BigDecimal::SIGN_POSITIVE_ZERO, "-0.0"],
            [BigDecimal::SIGN_NaN, "NAN"],
            [BigDecimal::SIGN_POSITIVE_INFINITE, "INF"],
            [BigDecimal::SIGN_POSITIVE_INFINITE, "+INF"],
            [BigDecimal::SIGN_NEGATIVE_INFINITE, "-INF"],
            [BigDecimal::SIGN_POSITIVE_FINITE, "1"],
            [BigDecimal::SIGN_POSITIVE_FINITE, "+1"],
            [BigDecimal::SIGN_NEGATIVE_FINITE, "-1"],
            [BigDecimal::SIGN_POSITIVE_FINITE, "0.1"],
            [BigDecimal::SIGN_POSITIVE_FINITE, "+0.1"],
            [BigDecimal::SIGN_NEGATIVE_FINITE, "-0.1"],
            [BigDecimal::SIGN_NEGATIVE_FINITE, "-0.10000000"],
            [BigDecimal::SIGN_POSITIVE_FINITE, "9223372036854775808.123456789000"],
            [BigDecimal::SIGN_POSITIVE_FINITE, "+9223372036854775808.123456789000"],
            [BigDecimal::SIGN_NEGATIVE_FINITE, "-9223372036854775808.123456789000"],
        ];
    }

    /**
     * @dataProvider isPositiveDataProvider
     * @covers \Kamiyonanayo\Math\BigDecimal::isPositive
     */
    public function testIsPositive($expected, $actual)
    {
        $actual_decimal = BigDecimal::valueOf($actual);
        self::initContextError();
        $r = $actual_decimal->isPositive();
        $this->assertSameInitContextError();
        $this->assertSame($expected, $r);
    }

    public static function isPositiveDataProvider()
    {
        return [
            [false, "0"],
            [false, "+0"],
            [false, "-0"],
            [false, "0.0"],
            [false, "+0.0"],
            [false, "-0.0"],
            [false, "NAN"],
            [true, "INF"],
            [true, "+INF"],
            [false, "-INF"],
            [true, "1"],
            [true, "+1"],
            [false, "-1"],
            [true, "0.1"],
            [true, "+0.1"],
            [false, "-0.1"],
            [false, "-0.10000000"],
            [true, "9223372036854775808.123456789000"],
            [true, "+9223372036854775808.123456789000"],
            [false, "-9223372036854775808.123456789000"],
        ];
    }

    /**
     * @dataProvider isNegativeDataProvider
     * @covers \Kamiyonanayo\Math\BigDecimal::isNegative
     */
    public function testIsNegative($expected, $actual)
    {
        $actual_decimal = BigDecimal::valueOf($actual);
        self::initContextError();
        $r = $actual_decimal->isNegative();
        $this->assertSameInitContextError();
        $this->assertSame($expected, $r);
    }

    public static function isNegativeDataProvider()
    {
        return [
            [false, "0"],
            [false, "+0"],
            [false, "-0"],
            [false, "0.0"],
            [false, "+0.0"],
            [false, "-0.0"],
            [false, "NAN"],
            [false, "INF"],
            [false, "+INF"],
            [true, "-INF"],
            [false, "1"],
            [false, "+1"],
            [true, "-1"],
            [false, "0.1"],
            [false, "+0.1"],
            [true, "-0.1"],
            [true, "-0.10000000"],
            [false, "9223372036854775808.123456789000"],
            [false, "+9223372036854775808.123456789000"],
            [true, "-9223372036854775808.123456789000"],
        ];
    }

    /**
     * @dataProvider isFiniteDataProvider
     * @covers \Kamiyonanayo\Math\BigDecimal::isFinite
     */
    public function testIsFinite($expected, $actual)
    {
        $actual_decimal = BigDecimal::valueOf($actual);
        self::initContextError();
        $r = $actual_decimal->isFinite();
        $this->assertSameInitContextError();
        $this->assertSame($expected, $r);
    }

    public static function isFiniteDataProvider()
    {
        return [
            [true, "0"],
            [true, "+0"],
            [true, "-0"],
            [true, "0.0"],
            [true, "+0.0"],
            [true, "-0.0"],
            [false, "NAN"],
            [false, "INF"],
            [false, "+INF"],
            [false, "-INF"],
            [true, "1"],
            [true, "+1"],
            [true, "-1"],
            [true, "0.1"],
            [true, "+0.1"],
            [true, "-0.1"],
            [true, "-0.10000000"],
            [true, "9223372036854775808.123456789000"],
            [true, "+9223372036854775808.123456789000"],
            [true, "-9223372036854775808.123456789000"],
        ];
    }

    /**
     * @dataProvider infiniteDataProvider
     * @covers \Kamiyonanayo\Math\BigDecimal::infinite
     */
    public function testinfinite($expected, $actual)
    {

        $actual_decimal = BigDecimal::valueOf($actual);
        self::initContextError();
        $r = $actual_decimal->infinite();
        $this->assertSameInitContextError();
        $this->assertSame($expected, $r);
    }

    public static function infiniteDataProvider()
    {
        return [
            [0, "0"],
            [0, "+0"],
            [0, "-0"],
            [0, "0.0"],
            [0, "+0.0"],
            [0, "-0.0"],
            [0, "NAN"],
            [1, "INF"],
            [1, "+INF"],
            [-1, "-INF"],
            [0, "1"],
            [0, "+1"],
            [0, "-1"],
            [0, "0.1"],
            [0, "+0.1"],
            [0, "-0.1"],
            [0, "-0.10000000"],
            [0, "9223372036854775808.123456789000"],
            [0, "+9223372036854775808.123456789000"],
            [0, "-9223372036854775808.123456789000"],
        ];
    }

    /**
     * @dataProvider isNanDataProvider
     * @covers \Kamiyonanayo\Math\BigDecimal::isNan
     */
    public function testIsNan($expected, $actual)
    {
        $actual_decimal = BigDecimal::valueOf($actual);
        self::initContextError();
        $r = $actual_decimal->isNan();
        $this->assertSameInitContextError();
        $this->assertSame($expected, $r);
    }

    public static function isNanDataProvider()
    {
        return [
            [false, "0"],
            [false, "+0"],
            [false, "-0"],
            [false, "0.0"],
            [false, "+0.0"],
            [false, "-0.0"],
            [true, "NAN"],
            [false, "INF"],
            [false, "+INF"],
            [false, "-INF"],
            [false, "1"],
            [false, "+1"],
            [false, "-1"],
            [false, "0.1"],
            [false, "+0.1"],
            [false, "-0.1"],
            [false, "-0.10000000"],
            [false, "9223372036854775808.123456789000"],
            [false, "+9223372036854775808.123456789000"],
            [false, "-9223372036854775808.123456789000"],
        ];
    }

    /**
     * @dataProvider isZeroDataProvider
     * @covers \Kamiyonanayo\Math\BigDecimal::isZero
     */
    public function testIsZero($expected, $actual)
    {
        $actual_decimal = BigDecimal::valueOf($actual);
        self::initContextError();
        $r = $actual_decimal->isZero();
        $this->assertSameInitContextError();
        $this->assertSame($expected, $r);
    }

    public static function isZeroDataProvider()
    {
        return [
            [true, "0"],
            [true, "+0"],
            [true, "-0"],
            [true, "0.0"],
            [true, "+0.0"],
            [true, "-0.0"],
            [false, "NAN"],
            [false, "INF"],
            [false, "+INF"],
            [false, "-INF"],
            [false, "1"],
            [false, "+1"],
            [false, "-1"],
            [false, "0.1"],
            [false, "+0.1"],
            [false, "-0.1"],
            [false, "-0.10000000"],
            [false, "9223372036854775808.123456789000"],
            [false, "+9223372036854775808.123456789000"],
            [false, "-9223372036854775808.123456789000"],
        ];
    }

    /**
     * @dataProvider nonZeroDataProvider
     * @covers \Kamiyonanayo\Math\BigDecimal::nonZero
     */
    public function testNonZero($expected_value, $expected_scale, $actual)
    {
        $actual_decimal = BigDecimal::valueOf($actual);
        self::initContextError();
        $r = $actual_decimal->nonZero();
        $this->assertSameInitContextError();
        if (is_null($expected_value)) {
            $this->assertNull($r);
        } else {
            $this->assertSameBigDecimalValue($expected_value, $expected_scale, $r);
        }
    }

    public static function nonZeroDataProvider()
    {
        return [
            [null, 0, "0"],
            [null, 0, "+0"],
            [null, 0, "-0"],
            [null, 0, "0.0"],
            [null, 0, "+0.0"],
            [null, 0, "-0.0"],
            ["NAN", 0, "NAN"],
            ["INF", 0, "INF"],
            ["INF", 0, "+INF"],
            ["-INF", 0, "-INF"],
            ["1", 0, "1"],
            ["1", 0, "+1"],
            ["-1", 0, "-1"],
            ["1", 1, "0.1"],
            ["1", 1, "+0.1"],
            ["-1", 1, "-0.1"],
            ["-10000000", 8, "-0.10000000"],
            ["9223372036854775808123456789000", 12, "9223372036854775808.123456789000"],
            ["9223372036854775808123456789000", 12, "+9223372036854775808.123456789000"],
            ["-9223372036854775808123456789000", 12, "-9223372036854775808.123456789000"],
        ];
    }

    /**
     * @dataProvider equalsExactDataProvider
     * @covers \Kamiyonanayo\Math\BigDecimal::equalsExact
     */
    public function testEqualsExact(int $error_mode, ?\Exception $exception, bool $expected, BigDecimal $number, $actual)
    {

        BigDecimal::getContext()->setErrorMode($error_mode);

        if ($exception) {
            $this->expectExceptionObject($exception);
            $number->equalsExact($actual);
        } else {
            self::initContextError();
            $c = $number->equalsExact($actual);
            $this->assertContextErrorNull();
            $this->assertSame($expected, $c);
        }
    }

    public static function equalsExactDataProvider()
    {
        return [

            [MathContext::ERROR_MODE_RETURN_NAN, null, true, BigDecimal::valueOf("0"), BigDecimal::valueOf("0")],
            [MathContext::ERROR_MODE_RETURN_NAN, null, true, BigDecimal::valueOf("0.00"), BigDecimal::valueOf("0.00")],

            [MathContext::ERROR_MODE_RETURN_NAN, null, false, BigDecimal::valueOf("0"), BigDecimal::valueOf("0.00")],
            [MathContext::ERROR_MODE_RETURN_NAN, null, false, BigDecimal::valueOf("0.00"), BigDecimal::valueOf("0")],
            [MathContext::ERROR_MODE_RETURN_NAN, null, false, BigDecimal::valueOf("1.00"), BigDecimal::valueOf("1")],
            [MathContext::ERROR_MODE_RETURN_NAN, null, true, BigDecimal::valueOf("9223372036854775808.123456789000"), BigDecimal::valueOf("9223372036854775808.123456789000")],

            [MathContext::ERROR_MODE_RETURN_NAN, null, true, BigDecimal::valueOf("0"), BigDecimal::valueOf("0")],
            [MathContext::ERROR_MODE_RETURN_NAN, null, false, BigDecimal::valueOf("1.10"), BigDecimal::valueOf("0")],
            [MathContext::ERROR_MODE_RETURN_NAN, null, false, BigDecimal::valueOf("-1.10"), BigDecimal::valueOf("0")],
            [MathContext::ERROR_MODE_RETURN_NAN, null, false, BigDecimal::valueOf("NAN"), BigDecimal::valueOf("0")],
            [MathContext::ERROR_MODE_RETURN_NAN, null, false, BigDecimal::valueOf("INF"), BigDecimal::valueOf("0")],
            [MathContext::ERROR_MODE_RETURN_NAN, null, false, BigDecimal::valueOf("-INF"), BigDecimal::valueOf("0")],

            [MathContext::ERROR_MODE_RETURN_NAN, null, false, BigDecimal::valueOf("0"), BigDecimal::valueOf("1.10")],
            [MathContext::ERROR_MODE_RETURN_NAN, null, true, BigDecimal::valueOf("1.10"), BigDecimal::valueOf("1.10")],
            [MathContext::ERROR_MODE_RETURN_NAN, null, false, BigDecimal::valueOf("-1.10"), BigDecimal::valueOf("1.10")],
            [MathContext::ERROR_MODE_RETURN_NAN, null, false, BigDecimal::valueOf("NAN"), BigDecimal::valueOf("1.10")],
            [MathContext::ERROR_MODE_RETURN_NAN, null, false, BigDecimal::valueOf("INF"), BigDecimal::valueOf("1.10")],
            [MathContext::ERROR_MODE_RETURN_NAN, null, false, BigDecimal::valueOf("-INF"), BigDecimal::valueOf("1.10")],

            [MathContext::ERROR_MODE_RETURN_NAN, null, false, BigDecimal::valueOf("0"), BigDecimal::valueOf("-1.10")],
            [MathContext::ERROR_MODE_RETURN_NAN, null, false, BigDecimal::valueOf("1.10"), BigDecimal::valueOf("-1.10")],
            [MathContext::ERROR_MODE_RETURN_NAN, null, true, BigDecimal::valueOf("-1.10"), BigDecimal::valueOf("-1.10")],
            [MathContext::ERROR_MODE_RETURN_NAN, null, false, BigDecimal::valueOf("NAN"), BigDecimal::valueOf("-1.10")],
            [MathContext::ERROR_MODE_RETURN_NAN, null, false, BigDecimal::valueOf("INF"), BigDecimal::valueOf("-1.10")],
            [MathContext::ERROR_MODE_RETURN_NAN, null, false, BigDecimal::valueOf("-INF"), BigDecimal::valueOf("-1.10")],

            [MathContext::ERROR_MODE_RETURN_NAN, null, false, BigDecimal::valueOf("0"), BigDecimal::valueOf("NAN")],
            [MathContext::ERROR_MODE_RETURN_NAN, null, false, BigDecimal::valueOf("1.10"), BigDecimal::valueOf("NAN")],
            [MathContext::ERROR_MODE_RETURN_NAN, null, false, BigDecimal::valueOf("-1.10"), BigDecimal::valueOf("NAN")],
            [MathContext::ERROR_MODE_RETURN_NAN, null, false, BigDecimal::valueOf("NAN"), BigDecimal::valueOf("NAN")],
            [MathContext::ERROR_MODE_RETURN_NAN, null, false, BigDecimal::valueOf("INF"), BigDecimal::valueOf("NAN")],
            [MathContext::ERROR_MODE_RETURN_NAN, null, false, BigDecimal::valueOf("-INF"), BigDecimal::valueOf("NAN")],

            [MathContext::ERROR_MODE_RETURN_NAN, null, false, BigDecimal::valueOf("0"), BigDecimal::valueOf("INF")],
            [MathContext::ERROR_MODE_RETURN_NAN, null, false, BigDecimal::valueOf("1.10"), BigDecimal::valueOf("INF")],
            [MathContext::ERROR_MODE_RETURN_NAN, null, false, BigDecimal::valueOf("-1.10"), BigDecimal::valueOf("INF")],
            [MathContext::ERROR_MODE_RETURN_NAN, null, false, BigDecimal::valueOf("NAN"), BigDecimal::valueOf("INF")],
            [MathContext::ERROR_MODE_RETURN_NAN, null, true, BigDecimal::valueOf("INF"), BigDecimal::valueOf("INF")],
            [MathContext::ERROR_MODE_RETURN_NAN, null, false, BigDecimal::valueOf("-INF"), BigDecimal::valueOf("INF")],

            [MathContext::ERROR_MODE_RETURN_NAN, null, false, BigDecimal::valueOf("0"), BigDecimal::valueOf("-INF")],
            [MathContext::ERROR_MODE_RETURN_NAN, null, false, BigDecimal::valueOf("1.10"), BigDecimal::valueOf("-INF")],
            [MathContext::ERROR_MODE_RETURN_NAN, null, false, BigDecimal::valueOf("-1.10"), BigDecimal::valueOf("-INF")],
            [MathContext::ERROR_MODE_RETURN_NAN, null, false, BigDecimal::valueOf("NAN"), BigDecimal::valueOf("-INF")],
            [MathContext::ERROR_MODE_RETURN_NAN, null, false, BigDecimal::valueOf("INF"), BigDecimal::valueOf("-INF")],

            [MathContext::ERROR_MODE_RETURN_NAN, null, true, BigDecimal::valueOf("-INF"), BigDecimal::valueOf("-INF")],

            [MathContext::ERROR_MODE_RETURN_NAN, null, false, BigDecimal::valueOf("0"), "0"],

        ];
    }

    /**
     * @dataProvider toIntDataProvider
     * @covers \Kamiyonanayo\Math\BigDecimal::toInt
     */
    public function testToInt($expected, $actual)
    {
        $actual_decimal = BigDecimal::valueOf($actual);
        self::initContextError();
        $this->assertSame($expected, $actual_decimal->toInt());
        $this->assertSameInitContextError();
    }

    public static function toIntDataProvider()
    {
        return [
            [(int)0, "0"],
            [(int)0, "+0"],
            [(int)0, "-0"],
            [(int)NAN, "NAN"],
            [(int)INF, "INF"],
            [(int)INF, "+INF"],
            [(int)-INF, "-INF"],
            [(int)1, "1"],
            [(int)1, "+1"],
            [(int)-1, "-1"],
            [(int)0.1, "0.1"],
            [(int)0.1, "+0.1"],
            [(int)-0.1, "-0.1"],
            [(int)-0.10000000, "-0.10000000"],
            [PHP_INT_MAX, "9223372036854775807"],
            [PHP_INT_MIN, "-9223372036854775808"],
            [PHP_INT_MAX, "9223372036854775808.123456789000"],
            [PHP_INT_MAX, "+9223372036854775808.123456789000"],
            [PHP_INT_MIN, "-9223372036854775808.123456789000"],
        ];
    }

    /**
     * @dataProvider toFloatDataProvider
     * @covers \Kamiyonanayo\Math\BigDecimal::toFloat
     */
    public function testToFloat($expected, $actual)
    {
        $actual_decimal = BigDecimal::valueOf($actual);
        self::initContextError();
        if (is_nan($expected)) {
            $this->assertNan($actual_decimal->toFloat());
        } else {
            $this->assertSame($expected, $actual_decimal->toFloat());
        }
        $this->assertSameInitContextError();
    }

    public static function toFloatDataProvider()
    {
        return [
            [(float)0, "0"],
            [(float)0, "+0"],
            [(float)NAN, "NAN"],
            [(float)INF, "INF"],
            [(float)INF, "+INF"],
            [(float)-INF, "-INF"],
            [(float)1, "1"],
            [(float)1, "+1"],
            [(float)-1, "-1"],
            [(float)0.1, "0.1"],
            [(float)0.1, "+0.1"],
            [(float)-0.1, "-0.1"],
            [(float)-0.10000000, "-0.10000000"],
            [1.8e308, "1.8e308"],
            [(float)9223372036854775808.123456789000, "9223372036854775808.123456789000"],
            [(float)9223372036854775808.123456789000, "+9223372036854775808.123456789000"],
            [(float)-9223372036854775808.123456789000, "-9223372036854775808.123456789000"],
        ];
    }

    /**
     * @dataProvider toStringDataProvider
     * @covers \Kamiyonanayo\Math\BigDecimal::__toString
     */
    public function test__toString($expected, $actual)
    {
        $actual_decimal = BigDecimal::valueOf($actual);
        self::initContextError();
        $this->assertSame($expected, (string)$actual_decimal);
        $this->assertSameInitContextError();

        self::initContextError();
        $this->assertSame($expected, $actual_decimal->__toString());
        $this->assertSameInitContextError();
    }

    public static function toStringDataProvider()
    {
        return [
            ["0", "0"],
            ["NAN", "NAN"],
            ["INF", "INF"],
            ["INF", "+INF"],
            ["-INF", "-INF"],
            ["1", "1"],
            ["1", "+1"],
            ["-1", "-1"],
            ["0.1", "0.1"],
            ["0.1", "+0.1"],
            ["-0.1", "-0.1"],
            ["-0.10000000", "-0.10000000"],
            ["9223372036854775808.123456789000", "9223372036854775808.123456789000"],
            ["9223372036854775808.123456789000", "+9223372036854775808.123456789000"],
            ["-9223372036854775808.123456789000", "-9223372036854775808.123456789000"],
        ];
    }

    /**
     * @dataProvider jsonSerializeDataProvider
     * @covers \Kamiyonanayo\Math\BigDecimal::jsonSerialize
     */
    public function testJsonSerialize($expected, $actual)
    {
        $actual_decimal = BigDecimal::valueOf($actual);
        self::initContextError();
        $this->assertSame($expected, $actual_decimal->jsonSerialize());
        $this->assertSameInitContextError();

        self::initContextError();
        $this->assertSame(json_encode($actual_decimal->jsonSerialize()), json_encode($actual_decimal));
        $this->assertSameInitContextError();
    }

    public static function jsonSerializeDataProvider()
    {
        return [
            ["0", "0"],
            ["NAN", "NAN"],
            ["INF", "INF"],
            ["INF", "+INF"],
            ["-INF", "-INF"],
            ["1", "1"],
            ["1", "+1"],
            ["-1", "-1"],
            ["0.1", "0.1"],
            ["0.1", "+0.1"],
            ["-0.1", "-0.1"],
            ["-0.10000000", "-0.10000000"],
            ["9223372036854775808.123456789000", "9223372036854775808.123456789000"],
            ["9223372036854775808.123456789000", "+9223372036854775808.123456789000"],
            ["-9223372036854775808.123456789000", "-9223372036854775808.123456789000"],
        ];
    }

    /**
     * @dataProvider toPlainStringDataProvider
     * @covers \Kamiyonanayo\Math\BigDecimal::toPlainString
     */
    public function testToPlainString($expected, $actual)
    {
        $actual_decimal = BigDecimal::valueOf($actual);
        self::initContextError();
        $this->assertSame($expected, $actual_decimal->toPlainString());
        $this->assertSameInitContextError();
    }

    public static function toPlainStringDataProvider()
    {
        return [
            ["0", "0"],
            ["NAN", "NAN"],
            ["INF", "INF"],
            ["INF", "+INF"],
            ["-INF", "-INF"],
            ["1", "1"],
            ["1", "+1"],
            ["-1", "-1"],
            ["0.1", "0.1"],
            ["0.1", "+0.1"],
            ["-0.1", "-0.1"],
            ["-0.10000000", "-0.10000000"],
            ["9223372036854775808.123456789000", "9223372036854775808.123456789000"],
            ["9223372036854775808.123456789000", "+9223372036854775808.123456789000"],
            ["-9223372036854775808.123456789000", "-9223372036854775808.123456789000"],
        ];
    }

    /**
     * @dataProvider compareToDataProvider
     * @covers \Kamiyonanayo\Math\BigDecimal::compareTo
     */
    public function testCompareTo(?int $expected, BigDecimal $number, $actual)
    {
        self::initContextError();
        $c = $number->compareTo($actual);
        $this->assertContextErrorNull();
        $this->assertSame($expected, $c);
    }

    public static function compareToDataProvider()
    {
        return [
            [0, BigDecimal::valueOf("0"), "0"],
            [0, BigDecimal::valueOf("0"), "0.00"],
            [0, BigDecimal::valueOf("0.999"), "0.999"],
            [0, BigDecimal::valueOf("10.1230"), "10.1230000"],
            [0, BigDecimal::valueOf("0.001"), "0.001"],
            [1, BigDecimal::valueOf("1"), "0"],
            [-1, BigDecimal::valueOf("0"), "1"],
            [1, BigDecimal::valueOf("0.00100001"), "0.001"],
            [-1, BigDecimal::valueOf("0.002"), "0.01"],
            [-1, BigDecimal::valueOf("0.0021"), "0.0022"],

            [1, BigDecimal::valueOf("1"), "-1"],
            [-1, BigDecimal::valueOf("-1"), "1"],
            [0, BigDecimal::valueOf("-1"), "-1"],

            [-1, BigDecimal::valueOf("-10.2"), "-10.1"],
            [0, BigDecimal::valueOf("-10.2"), "-10.2"],
            [1, BigDecimal::valueOf("-10.2"), "-10.3"],
            [0, BigDecimal::valueOf("-10.200"), "-10.20"],
            [-1, BigDecimal::valueOf("-0.011"), "-0.01"],

            [0, BigDecimal::valueOf("0"), "0"],
            [1, BigDecimal::valueOf("1.10"), "0"],
            [-1, BigDecimal::valueOf("-1.10"), "0"],
            [null, BigDecimal::valueOf("NAN"), "0"],
            [1, BigDecimal::valueOf("INF"), "0"],
            [-1, BigDecimal::valueOf("-INF"), "0"],

            [-1, BigDecimal::valueOf("0"), "1.10"],
            [0, BigDecimal::valueOf("1.10"), "1.10"],
            [-1, BigDecimal::valueOf("-1.10"), "1.10"],
            [null, BigDecimal::valueOf("NAN"), "1.10"],
            [1, BigDecimal::valueOf("INF"), "1.10"],
            [-1, BigDecimal::valueOf("-INF"), "1.10"],

            [1, BigDecimal::valueOf("0"), "-1.10"],
            [1, BigDecimal::valueOf("1.10"), "-1.10"],
            [0, BigDecimal::valueOf("-1.10"), "-1.10"],
            [null, BigDecimal::valueOf("NAN"), "-1.10"],
            [1, BigDecimal::valueOf("INF"), "-1.10"],
            [-1, BigDecimal::valueOf("-INF"), "-1.10"],

            [null, BigDecimal::valueOf("0"), "NAN"],
            [null, BigDecimal::valueOf("1.10"), "NAN"],
            [null, BigDecimal::valueOf("-1.10"), "NAN"],
            [null, BigDecimal::valueOf("NAN"), "NAN"],
            [null, BigDecimal::valueOf("INF"), "NAN"],
            [null, BigDecimal::valueOf("-INF"), "NAN"],

            [-1, BigDecimal::valueOf("0"), "INF"],
            [-1, BigDecimal::valueOf("1.10"), "INF"],
            [-1, BigDecimal::valueOf("-1.10"), "INF"],
            [null, BigDecimal::valueOf("NAN"), "INF"],
            [0, BigDecimal::valueOf("INF"), "INF"],
            [-1, BigDecimal::valueOf("-INF"), "INF"],

            [1, BigDecimal::valueOf("0"), "-INF"],
            [1, BigDecimal::valueOf("1.10"), "-INF"],
            [1, BigDecimal::valueOf("-1.10"), "-INF"],
            [null, BigDecimal::valueOf("NAN"), "-INF"],
            [1, BigDecimal::valueOf("INF"), "-INF"],
            [0, BigDecimal::valueOf("-INF"), "-INF"],

            [0, BigDecimal::valueOf("9223372036854775808.123456789000"), "9223372036854775808.123456789000"],

            [0, BigDecimal::valueOf("10E+1"), "100"],
            [0, BigDecimal::valueOf("1E+2"), "100"],
            [-1, BigDecimal::valueOf("0E+3"), "100"],
            [1, BigDecimal::valueOf("1E+2"), BigDecimal::valueOf("0.001")],
            [-1, BigDecimal::valueOf("-1E+2"), BigDecimal::valueOf("-0.00100")],

        ];
    }

    /**
     * @dataProvider compareToIllegalDataProvider
     * @covers \Kamiyonanayo\Math\BigDecimal::compareTo
     */
    public function testCompareToIllegal(int $error_mode, ?\Exception $exception, ?int $expected, ?MathRuntimeException $err_msg, BigDecimal $number, $actual)
    {

        BigDecimal::getContext()->setErrorMode($error_mode);

        if ($exception) {
            $this->expectExceptionObject($exception);
            $number->compareTo($actual);
        } else {
            self::initContextError();
            $c = $number->compareTo($actual);
            if (is_null($err_msg)) {
                $this->assertContextErrorNull();
            } else {
                $this->assertSame(get_class($err_msg), get_class(BigDecimal::getContext()->getLastError()));
                $this->assertSame($err_msg->getMessage(), BigDecimal::getContext()->getLastError()->getMessage());
                $this->assertSame($err_msg->getCode(), BigDecimal::getContext()->getLastError()->getCode());
            }
            $this->assertSame($expected, $c);
        }
    }

    public static function compareToIllegalDataProvider()
    {
        return [
            [MathContext::ERROR_MODE_RETURN_NAN, null, null, new NumberFormatException('does not support number "A"'), BigDecimal::valueOf("0.002"), "A"],
            [MathContext::ERROR_MODE_RETURN_NULL, null, null, new NumberFormatException('does not support number "A"'), BigDecimal::valueOf("0.002"), "A"],
            [MathContext::ERROR_MODE_THROW_EXCEPTION, new NumberFormatException('does not support number "A"'), null, null, BigDecimal::valueOf("0.002"), "A"],
        ];
    }

    /**
     * @dataProvider makeDataProvider
     * @covers \Kamiyonanayo\Math\BigDecimal::make
     */
    public function testMake(bool $eq, ?BigDecimal $expected, ?string $expected_value, ?int $expected_scale, string $actual_value, int $actual_scale)
    {

        $d1 = UnitTestHelper::invokeMethod(BigDecimal::class, "make", [$actual_value, $actual_scale]);
        if ($eq) {
            $this->assertSame($expected, $d1);
        } else {
            $this->assertSameBigDecimalValue($expected_value, $expected_scale, $d1);
        }
    }

    public static function makeDataProvider()
    {
        return [
            [true, BigDecimal::NaN(), null, null, "NAN", 0],
            [true, BigDecimal::PositiveInfinite(), null, null, "INF", 0],
            [true, BigDecimal::NegativeInfinite(), null, null, "-INF", 0],
            [true, BigDecimal::Zero(), null, null, "0", 0],
            [true, BigDecimal::One(), null, null, "1", 0],
            [true, BigDecimal::Ten(), null, null, "10", 0],
            [true, BigDecimal::OneHundred(), null, null, "100", 0],
            [false, null, "2", 0, "2", 0],
            [false, null, "0", 2, "0", 2],
            [false, null, "0", 2, "-0", 2],
            [false, null, "99", 0, "99", 0],
            [false, null, "2", 1, "2", 1],
            [false, null, "5", -10, "5", -10],
            [false, null, "9223372036854775808123456789000", 12, "9223372036854775808123456789000", 12],
        ];
    }


    /**
     * @covers \Kamiyonanayo\Math\BigDecimal::cached
     */
    public function testCached()
    {

        $tmp_cached_instances = UnitTestHelper::getPropValue(BigDecimal::class, "cached_instances");

        UnitTestHelper::setPropValue(BigDecimal::class, "cached_instances", []);

        $d1 = UnitTestHelper::invokeMethod(BigDecimal::class, "cached", ["10000", 0]);
        $this->assertSameBigDecimalValue("10000", 0, $d1);

        $d2 = UnitTestHelper::invokeMethod(BigDecimal::class, "cached", ["123", 3]);
        $this->assertSameBigDecimalValue("123", 3, $d2);

        $d3 = UnitTestHelper::invokeMethod(BigDecimal::class, "cached", ["987", -5]);
        $this->assertSameBigDecimalValue("987", -5, $d3);

        UnitTestHelper::setPropValue(BigDecimal::class, "cached_instances", $tmp_cached_instances);
    }

    /**
     * @dataProvider signValueDataProvider
     * @covers \Kamiyonanayo\Math\BigDecimal::signValue
     */
    public function testSignValue($expected, $actual)
    {
        $this->assertSame($expected, UnitTestHelper::invokeMethod(BigDecimal::class, "signValue", [$actual]));
    }

    public static function signValueDataProvider()
    {
        return [
            [[BigDecimal::VALUE_NaN, BigDecimal::SIGN_NaN], BigDecimal::VALUE_NaN],
            [["NAN", BigDecimal::SIGN_NaN], "NAN"],
            [["0", BigDecimal::SIGN_POSITIVE_ZERO], "0"],
            [["0", BigDecimal::SIGN_POSITIVE_ZERO], "-0"],
            [["999", BigDecimal::SIGN_POSITIVE_FINITE], "999"],
            [["-99", BigDecimal::SIGN_NEGATIVE_FINITE], "-99"],
            [["9223372036854775808123456789000", BigDecimal::SIGN_POSITIVE_FINITE], "9223372036854775808123456789000"],
            [["-9223372036854775808123456789000", BigDecimal::SIGN_NEGATIVE_FINITE], "-9223372036854775808123456789000"],
            [[BigDecimal::VALUE_POSITIVE_INFINITE, BigDecimal::SIGN_POSITIVE_INFINITE], BigDecimal::VALUE_POSITIVE_INFINITE],
            [["INF", BigDecimal::SIGN_POSITIVE_INFINITE], "INF"],
            [[BigDecimal::VALUE_NEGATIVE_INFINITE, BigDecimal::SIGN_NEGATIVE_INFINITE], BigDecimal::VALUE_NEGATIVE_INFINITE],
            [["-INF", BigDecimal::SIGN_NEGATIVE_INFINITE], "-INF"],
        ];
    }

    /**
     * @dataProvider splitifDataProvider
     * @dataProvider splitifPlusScaleDataProvider
     * @dataProvider splitifMinusScaleDataProvider
     * @covers \Kamiyonanayo\Math\BigDecimal::toS
     */
    public function testToS(array $expected, array $actual)
    {
        $bd = UnitTestHelper::newInstance(BigDecimal::class, $actual);
        $this->assertSame(implode(($expected[1] === "" ? "" : "."), $expected), UnitTestHelper::invokeMethod($bd, "toS", []));
    }

    /**
     * @dataProvider splitifDataProvider
     * @dataProvider splitifPlusScaleDataProvider
     * @dataProvider splitifMinusScaleDataProvider
     * @covers \Kamiyonanayo\Math\BigDecimal::splitif
     */
    public function testSplitif(array $expected, array $actual)
    {
        $this->assertSame($expected, UnitTestHelper::invokeMethod(BigDecimal::class, "splitif", $actual));
    }

    public static function splitifDataProvider()
    {
        return [
            [["0", ""], ["0", 0]],
            [["1", ""], ["1", 0]],
            [["NAN", ""], ["NAN", 0]],
            [["INF", ""], ["INF", 0]],
            [["-INF", ""], ["-INF", 0]],
            [["XXXXXXX", ""], ["XXXXXXX", 0]],
        ];
    }

    /**
     * @dataProvider splitifPlusScaleDataProvider
     * @covers \Kamiyonanayo\Math\BigDecimal::splitifPlusScale
     */
    public function testSplitifPlusScale(array $expected, array $actual)
    {
        $this->assertSame($expected, UnitTestHelper::invokeMethod(BigDecimal::class, "splitifPlusScale", $actual));
    }

    public static function splitifPlusScaleDataProvider()
    {
        return [
            [["0", "0"], ["0", 1]],
            [["0", "00"], ["0", 2]],
            [["0", "000"], ["0", 3]],
            [["0", "1"], ["1", 1]],
            [["0", "01"], ["1", 2]],
            [["0", "001"], ["1", 3]],

            [["123", "4"], ["1234", 1]],
            [["12", "34"], ["1234", 2]],
            [["1", "234"], ["1234", 3]],
            [["0", "1234"], ["1234", 4]],
            [["0", "01234"], ["1234", 5]],
        ];
    }

    /**
     * @dataProvider splitifMinusScaleDataProvider
     * @covers \Kamiyonanayo\Math\BigDecimal::splitifMinusScale
     */
    public function testSplitifMinusScale(array $expected, array $actual)
    {
        $this->assertSame($expected, UnitTestHelper::invokeMethod(BigDecimal::class, "splitifMinusScale", $actual));
    }

    public static function splitifMinusScaleDataProvider()
    {
        return [
            [["0", ""], ["0", -1]],
            [["0", ""], ["0", -2]],
            [["0", ""], ["0", -3]],

            [["10", ""], ["1", -1]],
            [["100", ""], ["1", -2]],
            [["1000", ""], ["1", -3]],

            [["1230", ""], ["123", -1]],
            [["12300", ""], ["123", -2]],
            [["123000", ""], ["123", -3]],
        ];
    }

    /**
     * @dataProvider createLeadingZeroValueDataProvider
     * @covers \Kamiyonanayo\Math\BigDecimal::createLeadingZeroValue
     */
    public function testCreateLeadingZeroValue(string $expected, array $actual)
    {
        $this->assertSame($expected, UnitTestHelper::invokeMethod(BigDecimal::class, "createLeadingZeroValue", $actual));
    }

    public static function createLeadingZeroValueDataProvider()
    {
        return [
            ["0", ["0", 0]],
            ["00", ["0", 1]],

            ["1", ["1", 0]],
            ["01", ["1", 1]],
            ["-1", ["-1", 0]],
            ["-01", ["-1", 1]],

            ["123", ["123", 0]],
            ["123", ["123", 1]],
            ["123", ["123", 2]],
            ["0123", ["123", 3]],
            ["00123", ["123", 4]],
            ["000123", ["123", 5]],
            ["-123", ["-123", 0]],
            ["-123", ["-123", 1]],
            ["-123", ["-123", 2]],
            ["-0123", ["-123", 3]],
            ["-00123", ["-123", 4]],
            ["-000123", ["-123", 5]],

            ["0", ["0", -1]],
            ["0", ["0", -2]],
            ["0", ["0", -3]],
            ["123", ["123", -1]],
            ["-123", ["-123", -2]],

        ];
    }


    /**
     * @dataProvider minScaleValueDataProvider
     * @covers \Kamiyonanayo\Math\BigDecimal::minScaleValue
     */
    public function testMinScaleValue(string $expected, array $actual)
    {
        $this->assertSame($expected, UnitTestHelper::invokeMethod(BigDecimal::class, "minScaleValue", $actual));
    }

    public static function minScaleValueDataProvider()
    {
        return [
            ["0", [BigDecimal::valueOf("0E-0"), 0]],
            ["0", [BigDecimal::valueOf("0E-10"), 100]],
            ["0", [BigDecimal::valueOf("0E+10"), -100]],
            ["1", [BigDecimal::valueOf("1E-0"), 0]],
            ["10", [BigDecimal::valueOf("1E-0"), 1]],
            ["1", [BigDecimal::valueOf("1E-0"), -1]],

            ["123", [BigDecimal::valueOf("123E-10"), 10]],
            ["1230", [BigDecimal::valueOf("123E-10"), 11]],
            ["12300", [BigDecimal::valueOf("123E-10"), 12]],
            ["123", [BigDecimal::valueOf("123E-11"), 10]],
            ["123", [BigDecimal::valueOf("123E-12"), 10]],

            ["123", [BigDecimal::valueOf("123E+10"), -10]],
            ["123", [BigDecimal::valueOf("123E+10"), -11]],
            ["123", [BigDecimal::valueOf("123E+10"), -12]],
            ["1230", [BigDecimal::valueOf("123E+11"), -10]],
            ["12300", [BigDecimal::valueOf("123E+12"), -10]],

            ["-123", [BigDecimal::valueOf("-123E-10"), 10]],
            ["-1230", [BigDecimal::valueOf("-123E-10"), 11]],
            ["-12300", [BigDecimal::valueOf("-123E-10"), 12]],
            ["-123", [BigDecimal::valueOf("-123E-11"), 10]],
            ["-123", [BigDecimal::valueOf("-123E-12"), 10]],

            ["-123", [BigDecimal::valueOf("-123E+10"), -10]],
            ["-123", [BigDecimal::valueOf("-123E+10"), -11]],
            ["-123", [BigDecimal::valueOf("-123E+10"), -12]],
            ["-1230", [BigDecimal::valueOf("-123E+11"), -10]],
            ["-12300", [BigDecimal::valueOf("-123E+12"), -10]],


            ["-123", [BigDecimal::valueOf("-123E-5"), -5]],
            ["-1230000000000", [BigDecimal::valueOf("-123E+5"), 5]],

        ];
    }

    /**
     * @dataProvider sameScaleValuesDataProvider
     * @covers \Kamiyonanayo\Math\BigDecimal::sameScaleValues
     */
    public function testSameScaleValues(array $expected, array $actual)
    {
        $this->assertSame($expected, UnitTestHelper::invokeMethod(BigDecimal::class, "sameScaleValues", $actual));
    }

    public static function sameScaleValuesDataProvider()
    {
        return [
            [["0", "0"], [BigDecimal::valueOf("0E-0"), BigDecimal::valueOf("0E-0")]],
            [["0", "0"], [BigDecimal::valueOf("0E-10"), BigDecimal::valueOf("0E-0")]],
            [["0", "0"], [BigDecimal::valueOf("0E+10"), BigDecimal::valueOf("0E+0")]],
            [["0", "0"], [BigDecimal::valueOf("0E-0"), BigDecimal::valueOf("0E-10")]],
            [["0", "0"], [BigDecimal::valueOf("0E+0"), BigDecimal::valueOf("0E+10")]],
            [["1", "1"], [BigDecimal::valueOf("1E-0"), BigDecimal::valueOf("1E-0")]],
            [["1", "10"], [BigDecimal::valueOf("1E-1"), BigDecimal::valueOf("1E-0")]],
            [["10", "1"], [BigDecimal::valueOf("1E-0"), BigDecimal::valueOf("1E-1")]],
            [["10", "1"], [BigDecimal::valueOf("1E+1"), BigDecimal::valueOf("1E+0")]],
            [["1", "10"], [BigDecimal::valueOf("1E+0"), BigDecimal::valueOf("1E+1")]],

            [["123", "-123"], [BigDecimal::valueOf("123E-10"), BigDecimal::valueOf("-123E-10")]],
            [["123", "-1230"], [BigDecimal::valueOf("123E-11"), BigDecimal::valueOf("-123E-10")]],
            [["123", "-12300"], [BigDecimal::valueOf("123E-12"), BigDecimal::valueOf("-123E-10")]],
            [["123", "-123"], [BigDecimal::valueOf("123E-10"), BigDecimal::valueOf("-123E-10")]],
            [["1230", "-123"], [BigDecimal::valueOf("123E-10"), BigDecimal::valueOf("-123E-11")]],
            [["12300", "-123"], [BigDecimal::valueOf("123E-10"), BigDecimal::valueOf("-123E-12")]],
            [["-123", "123"], [BigDecimal::valueOf("-123E-10"), BigDecimal::valueOf("123E-10")]],
            [["-123", "1230"], [BigDecimal::valueOf("-123E-11"), BigDecimal::valueOf("123E-10")]],
            [["-123", "12300"], [BigDecimal::valueOf("-123E-12"), BigDecimal::valueOf("123E-10")]],
            [["-123", "123"], [BigDecimal::valueOf("-123E-10"), BigDecimal::valueOf("123E-10")]],
            [["-1230", "123"], [BigDecimal::valueOf("-123E-10"), BigDecimal::valueOf("123E-11")]],
            [["-12300", "123"], [BigDecimal::valueOf("-123E-10"), BigDecimal::valueOf("123E-12")]],

            [["123", "1230000000000"], [BigDecimal::valueOf("123E-5"), BigDecimal::valueOf("123E+5")]],
            [["1230000000000", "123"], [BigDecimal::valueOf("123E+5"), BigDecimal::valueOf("123E-5")]],
            [["123", "-1230000000000"], [BigDecimal::valueOf("123E-5"), BigDecimal::valueOf("-123E+5")]],
            [["1230000000000", "-123"], [BigDecimal::valueOf("123E+5"), BigDecimal::valueOf("-123E-5")]],
            [["-123", "1230000000000"], [BigDecimal::valueOf("-123E-5"), BigDecimal::valueOf("123E+5")]],
            [["-1230000000000", "123"], [BigDecimal::valueOf("-123E+5"), BigDecimal::valueOf("123E-5")]],
            [["-123", "-1230000000000"], [BigDecimal::valueOf("-123E-5"), BigDecimal::valueOf("-123E+5")]],
            [["-1230000000000", "-123"], [BigDecimal::valueOf("-123E+5"), BigDecimal::valueOf("-123E-5")]],

        ];
    }

    /**
     * @dataProvider calcSpecialValueDataProvider
     * @covers \Kamiyonanayo\Math\BigDecimal::calcSpecialValue
     */
    public function testCalcSpecialValue($expected, $op1, $op2, $op)
    {

        UnitTestHelper::setPropValue(BigDecimal::class, "special_value_defs", []);

        if ($expected === "null") {
            $expected = null;
        }

        $actual = UnitTestHelper::invokeMethod(BigDecimal::valueOf($op1), "calcSpecialValue", [BigDecimal::valueOf($op2), $op]);
        if ($actual instanceof BigDecimal) {
            $actual = (string)$actual;
        }
        $this->assertSame($expected, $actual);
    }

    public static function calcSpecialValueDataProvider()
    {
        $ret = [];
        $csv_data = UnitTestHelper::getCsvData(__DIR__ . "/BigDecimalTestCalcSpecialValue.csv");
        $header2index = array_flip($csv_data[0]);
        $ops = explode(",", "+,-,*,/,%");
        foreach (array_slice($csv_data, 1) as $row) {
            $op1 = $row[$header2index["op1"]];
            $op2 = $row[$header2index["op2"]];
            foreach ($ops as $op) {
                $expected = $row[$header2index[$op]];
                $ret[] = [$expected, $op1, $op2, $op];
            }
        };
        return $ret;
    }

    /**
     * @dataProvider errorDataProvider
     * @covers \Kamiyonanayo\Math\BigDecimal::error
     */
    public function testError($error_mode, $exception, $expected, $decimal)
    {

        $context = BigDecimal::getContext();

        UnitTestHelper::setPropValue($context, "error_mode", $error_mode);

        $e = new MathRuntimeException("abc", 99);

        if ($exception) {
            $this->expectExceptionObject($e);
            UnitTestHelper::invokeMethod($decimal, "error", [$e]);
        } else {
            $this->assertSame($expected, UnitTestHelper::invokeMethod($decimal, "error", [$e]));
        }
    }

    public static function errorDataProvider()
    {
        return [
            [MathContext::ERROR_MODE_RETURN_NAN, false, BigDecimal::NaN(), BigDecimal::One()],
            [MathContext::ERROR_MODE_RETURN_NULL, false, null, BigDecimal::One()],
            [MathContext::ERROR_MODE_THROW_EXCEPTION, true, null, BigDecimal::One()],
            [99, true, null, BigDecimal::One()],
        ];
    }
}
