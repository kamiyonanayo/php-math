<?php

use LaravelFw\Support\FwNumberCalcHelper;

require_once __DIR__ . "/vendor/autoload.php";

function aaa()
{
    $file = new SplFileObject(__DIR__ . "/test.tsv");
    $file->setFlags(SplFileObject::READ_CSV);
    $file->setCsvControl("\t");
    $records = [];
    foreach ($file as $line) {
        if (!is_null($line[0])) {
            $records[] = $line;
        }
    }

    $keys = ["UP", "DOWN", "CEILING", "FLOOR", "HALF_UP", "HALF_DOWN", "HALF_CEILING", "HALF_FLOOR", "HALF_EVEN", "HALF_ODD"];
    $header2index = array_flip((array)$records[0]);

    $result = [];
    foreach (array_slice($records, 1) as $row) {
        $input = $row[$header2index["INPUT"]];
        $scale = $row[$header2index["SCALE"]];
        foreach ($keys as $mode) {
            $ret = FwNumberCalcHelper::scale($input, $scale, strtolower($mode));
            $ex = $row[$header2index[$mode]];
            if ($ret === $ex) {
                $result[] = [$input, $scale, $mode, $ex, $ret];
            }
        }
    }

    print_r($result);
}

aaa();
//var_dump(StringValidator::validateFloatNumber("-0000.0",false,true));

//var_dump(StringValidator::validateFloatNumber("0.001"));
