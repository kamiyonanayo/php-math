<?php

declare(strict_types=1);

namespace Kamiyonanayo\Math\Exception;

class NumberFormatException extends MathRuntimeException
{
}
