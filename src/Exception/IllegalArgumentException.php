<?php

declare(strict_types=1);

namespace Kamiyonanayo\Math\Exception;

use Kamiyonanayo\Math\Internals\Helper;

class IllegalArgumentException extends MathRuntimeException
{

    public static function InvalidContextErrorMode(int $error_mode): self
    {
        return new IllegalArgumentException(sprintf('Invalid error mode "%s"', (string)$error_mode));
    }

    /**
     * @param \Kamiyonanayo\Math\RoundingMode|string $rounding_mode
     */
    public static function InvalidRoundingMode($rounding_mode): self
    {
        return new IllegalArgumentException(sprintf('Invalid rounding mode "%s"', Helper::convertString($rounding_mode)));
    }

}
