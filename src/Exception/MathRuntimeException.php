<?php

declare(strict_types=1);

namespace Kamiyonanayo\Math\Exception;

class MathRuntimeException extends \RuntimeException
{
}
