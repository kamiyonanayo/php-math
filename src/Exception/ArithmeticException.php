<?php

declare(strict_types=1);

namespace Kamiyonanayo\Math\Exception;

class ArithmeticException extends MathRuntimeException
{
    public static function RoundingNecessary(): self
    {
        return new ArithmeticException("Rounding necessary");
    }

    public static function DivisionUndefined(): self
    {
        return new ArithmeticException("Division undefined");
    }

    public static function DivisionByZero(): self
    {
        return new ArithmeticException("Division by zero");
    }

}
