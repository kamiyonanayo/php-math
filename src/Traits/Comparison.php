<?php

declare(strict_types=1);

namespace Kamiyonanayo\Math\Traits;

/**
 * The Comparison trait is used by classes whose objects may be ordered.
 *
 * The class must define the `compareTo($that): ?int` method
 *
 */
trait Comparison
{

    /**
     * Compares must define mtehod
     *
     * @param mixed $that Numeric to which this Numeric is to be compared.
     *
     * @return integer|null if a == b, 1 if a > b, -1 if a < b, null if can't compare
     */
    abstract public function compareTo($that): ?int;

    /**
     * Tests for value equality
     *
     * @param mixed $that
     * @return boolean true if the `$this == $that`.
     * @see Comparison::equalTo()
     */
    public function eq($that): bool
    {
        return $this->equalTo($that);
    }

    /**
     * Tests for value equality
     *
     * @param mixed $that
     * @return boolean true if the `$this == $that`.
     */
    public function equalTo($that): bool
    {
        $c = $this->compareTo($that);
        return (!\is_null($c)) &&  $c == 0;
    }

    /**
     * Tests for value not equality
     *
     * @param mixed $that
     * @return boolean true if the `$this != $that`.
     * @see Comparison::notEqualTo()
     */
    public function ne($that): bool
    {
        return $this->notEqualTo($that);
    }

    /**
     * Tests for value not equality
     *
     * @param mixed $that
     * @return boolean true if the `$this != $that`.
     */
    public function notEqualTo($that): bool
    {
        return !$this->equalTo($that);
    }

    /**
     * Returns true if a is greater than b.
     *
     * @param mixed $that
     * @return boolean true if $this &gt; $that
     * @see Comparison::greaterThan()
     */
    public function gt($that): bool
    {
        return $this->greaterThan($that);
    }

    /**
     * Returns true if a is greater than b.
     *
     * @param mixed $that
     * @return boolean true if $this &gt; $that
     */
    public function greaterThan($that): bool
    {
        $c = $this->compareTo($that);
        return (!\is_null($c)) &&  $c > 0;
    }

    /**
     * Returns true if a is greater than or equal to b.
     *
     * @param mixed $that
     * @return boolean true if $this &gt;= $that
     * @see Comparison::greaterThanOrEqualTo()
     */
    public function gte($that): bool
    {
        return $this->greaterThanOrEqualTo($that);
    }

    /**
     * Returns true if a is greater than or equal to b.
     *
     * @param mixed $that
     * @return boolean true if $this &gt;= $that
     */
    public function greaterThanOrEqualTo($that): bool
    {
        $c = $this->compareTo($that);
        return (!\is_null($c)) &&  $c >= 0;
    }

    /**
     * Returns true if a is less than b.
     *
     * @param mixed $that
     * @return boolean true if $this &lt; $that
     * @see Comparison::lessThan()
     */
    public function lt($that): bool
    {
        return $this->lessThan($that);
    }

    /**
     * Returns true if a is less than b.
     *
     * @param mixed $that
     * @return boolean true if $this &lt; $that
     */
    public function lessThan($that): bool
    {
        $c = $this->compareTo($that);
        return (!\is_null($c)) &&  $c < 0;
    }

    /**
     * Returns true if a is less than or equal to b.
     *
     * @param mixed $that
     * @return boolean true if $this &lt;= $that
     * @see Comparison::lessThanOrEqualTo()
     */
    public function lte($that): bool
    {
        return $this->lessThanOrEqualTo($that);
    }

    /**
     * Returns true if a is less than or equal to b.
     *
     * @param mixed $that
     * @return boolean true if $this &lt;= $that
     */
    public function lessThanOrEqualTo($that): bool
    {
        $c = $this->compareTo($that);
        return (!\is_null($c)) &&  $c <= 0;
    }

    /**
     * Determines if the instance is between two others.
     *
     * - `($min <= $this AND $this <= $max)` if `$equal` is `true`
     * - `($min < $this AND $this < $max)` if `$equal` is `false`
     *
     * @param mixed $min begin value
     * @param mixed $max end value
     * @param boolean $equal bounds are included or not
     * @return boolean true if between two others.
     *
     * @SuppressWarnings("PMD.BooleanArgumentFlag")
     */
    public function between($min, $max, $equal = true): bool
    {
        if ($equal) {
            return $this->greaterThanOrEqualTo($min) && $this->lessThanOrEqualTo($max);
        }
        return $this->greaterThan($min) && $this->lessThan($max);
    }

    /**
     * Determines if the instance is between two others, bounds included.
     *
     * - `($min <= $this AND $this <= $max)`
     *
     * @param mixed $min begin value
     * @param mixed $max end value
     * @return boolean true if between two others.
     */
    public function betweenIncluded($min, $max): bool
    {
        return $this->between($min, $max, true);
    }

    /**
     * Determines if the instance is between two others, bounds excluded.
     *
     * - `($min < $this AND $this < $max)`
     *
     * @param mixed $min begin value
     * @param mixed $max end value
     * @return boolean true if between two others.
     */
    public function betweenExcluded($min, $max): bool
    {
        return $this->between($min, $max, false);
    }

}
