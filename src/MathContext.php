<?php

declare(strict_types=1);

namespace Kamiyonanayo\Math;

use Kamiyonanayo\Math\Exception\IllegalArgumentException;
use Kamiyonanayo\Math\Exception\MathRuntimeException;

/**
 * Mutable, Math Context
 * @psalm-consistent-constructor
 */
class MathContext
{

    /**
     * error mode : Returns `NAN` in case of error
     */
    public const ERROR_MODE_RETURN_NAN = 1;

    /**
     * error mode : Returns `null` in case of error
     */
    public const ERROR_MODE_RETURN_NULL = 2;

    /**
     * error mode : Throws exception in case of error
     */
    public const ERROR_MODE_THROW_EXCEPTION = 3;

    /**
     * default error mode
     */
    public const DEFAULT_ERROR_MODE = self::ERROR_MODE_RETURN_NAN;

    /**
     * error mode
     *
     * @var int
     */
    private $error_mode = self::DEFAULT_ERROR_MODE;

    /**
     * Last exception
     * @var MathRuntimeException|null
     */
    private $last_exception = null;

    protected function __construct()
    {
    }

    /**
     * Create MathContext
     *
     * @param integer|null $error_mode error mode
     * @return MathContext
     */
    public static function make(?int $error_mode = null): MathContext
    {
        $context = new static();
        if (!\is_null($error_mode)) {
            $context->setErrorMode($error_mode);
        }
        return $context;
    }

    /**
     * Sets error mode
     *
     * @param integer $error_mode error mode
     * @return void
     */
    public function setErrorMode(int $error_mode): void
    {

        if (!\in_array($error_mode, [
            self::ERROR_MODE_RETURN_NAN,
            self::ERROR_MODE_RETURN_NULL,
            self::ERROR_MODE_THROW_EXCEPTION,
        ], true)) {
            throw IllegalArgumentException::InvalidContextErrorMode($error_mode);
        }

        $this->error_mode = $error_mode;
    }

    /**
     * Returns error mode
     *
     * @return integer
     */
    public function getErrorMode(): int
    {
        return $this->error_mode;
    }

    /**
     * Returns last exception
     *
     * @return MathRuntimeException|null
     */
    public function getLastError(): ?MathRuntimeException
    {
        return $this->last_exception;
    }

    /**
     * Returns last exception message
     *
     * @return string exception message, empty string if no exception
     */
    public function getLastErrorMessage(): string
    {
        $e = $this->getLastError();
        if (\is_null($e)) {
            return "";
        } else {
            return $e->getMessage();
        }
    }

    /**
     * Sets exception
     *
     * @internal used in BigDecimal.
     *
     * @param MathRuntimeException|null $exception error reason
     * @return void
     */
    public function setError(?MathRuntimeException $exception): void
    {
        $this->last_exception = $exception;
    }

    /**
     * clear last exception
     *
     * @return void
     */
    public function clearError(): void
    {
        $this->setError(null);
    }
}
