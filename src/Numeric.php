<?php

declare(strict_types=1);

namespace Kamiyonanayo\Math;

use JsonSerializable;
use Kamiyonanayo\Math\Exception\MathRuntimeException;
use Kamiyonanayo\Math\Traits\Comparison;
use Serializable;

/**
 * provides arbitrary-precision
 */
abstract class Numeric implements Serializable, JsonSerializable
{

    use Comparison;

    /**
     * Context
     * @var MathContext|null
     */
    private static $context = null;

    protected function __construct()
    {
    }

    /**
     * Set current Context
     *
     * @param MathContext $context new context
     * @return MathContext Previously set context
     */
    public static function setContext(MathContext $context): MathContext
    {
        $context->clearError();

        $old = self::getContext();

        self::$context = $context;

        return $old;
    }

    /**
     * Returns current Context
     *
     * @return MathContext current Context
     */
    public static function getContext(): MathContext
    {
        if (\is_null(self::$context)) {
            self::$context = MathContext::make();
        }
        return self::$context;
    }

    /**
     * Returns the last error occurred
     *
     * @return MathRuntimeException|null
     */
    public static function getLastError(): ?MathRuntimeException
    {
        return self::getContext()->getLastError();
    }

    /**
     * Returns the error string of the last method call
     *
     * @return string error message , or empty string if no error has occurred.
     */
    public static function getLastErrorMessage(): string
    {
        return self::getContext()->getLastErrorMessage();
    }

    /**
     * clear error
     *
     * @return void
     */
    protected static function clearError(): void
    {
        self::getContext()->clearError();
    }

    /**
     * Execute the provided callable, but preserve the MathContext
     *
     * @param callable $callback
     * @return mixed
     */
    public static function save_context(callable $callback)
    {
        self::clearError();
        $context = clone self::getContext();
        try {
            /** @var mixed $ret */
            $ret = $callback();
        } finally {
            self::$context = $context;
        }
        return $ret;
    }

    // -------------------

    /**
     * serialize method
     *
     * @internal
     * @return string
     */
    public function serialize(): string
    {
        return \serialize($this->__serialize());
    }

    /**
     * unserialize method
     *
     * @internal
     * @param string $serialized serialized string
     * @return void
     */
    public function unserialize($serialized): void
    {
        /** @var array<mixed,mixed> $data */
        $data = \unserialize($serialized);
        $this->__unserialize($data);
    }

    /**
     * serialize method
     *
     * @internal
     * @return array<mixed,mixed>
     */
    abstract public function __serialize(): array;

    /**
     * unserialize method
     *
     * @internal
     * @param array<mixed,mixed> $data serialized array
     * @return void
     */
    abstract public function __unserialize(array $data): void;

    // ---------------------------------------------------------

    /**
     * Compares this Numeric with the specified Numeric.
     *
     * @param mixed $that Numeric to which this Numeric is to be compared.
     *
     * @return integer|null if a == b, 1 if a > b, -1 if a < b, null if can't compare
     */
    abstract public function compareTo($that): ?int;

    /**
     * Converts this Numeric to an int.
     *
     * @return integer this Numeric converted to an int.
     */
    abstract public function toInt(): int;

    /**
     * Converts this Numeric to an float.
     *
     * @return float this Numeric converted to an float.
     */
    abstract public function toFloat(): float;

    /**
     * Returns a string representation of this Numeric without an exponent field.
     *
     * @return string a string representation of this Numeric without an exponent field.
     */
    abstract public function __toString(): string;

    /**
     * Returns a Numeric whose value is the absolute value of this Numeric.
     *
     * @return Numeric `abs(this)`.
     */
    abstract public function abs(): Numeric;

    /**
     * Returns True if the value is Zero.
     *
     * @return boolean true if Numeric is 0
     */
    abstract public function isZero(): bool;

    /**
     * Returns `$this` if the value is non-zero.
     *
     * @return Numeric|null `$this` if Numeric is non-zero, null if Numeric is 0
     */
    abstract public function nonZero(): ?Numeric;
}
