<?php

declare(strict_types=1);

namespace Kamiyonanayo\Math;

use Kamiyonanayo\Math\Exception\ArithmeticException;
use Kamiyonanayo\Math\Exception\IllegalArgumentException;
use Kamiyonanayo\Math\Exception\MathRuntimeException;
use Kamiyonanayo\Math\Exception\NumberFormatException;
use Kamiyonanayo\Math\Internals\Helper;
use Kamiyonanayo\Math\Internals\IntegerCalculator;

/**
 * Immutable, provides arbitrary-precision floating point decimal arithmetic.
 * @SuppressWarnings("PMD.ExcessiveClassLength")
 * @SuppressWarnings("PMD.ConstantNamingConventions")
 * @SuppressWarnings("PMD.ExcessiveClassComplexity")
 * @phan-file-suppress PhanAccessMethodInternal
 */
final class BigDecimal extends Numeric
{

    /** NAN(Not a Number) string representation. */
    public const VALUE_NaN = "NAN";
    /** Positive Infinite string representation. */
    public const VALUE_POSITIVE_INFINITE = "INF";
    /** Negative Infinite string representation. */
    public const VALUE_NEGATIVE_INFINITE = "-INF";

    /** Indicates that a value is not a number. */
    public const SIGN_NaN = 0;
    /** Indicates that a value is 0. */
    public const SIGN_POSITIVE_ZERO = 1;
    // public const SIGN_NEGATIVE_ZERO = -1;
    /** Indicates that a value is positive and finite. */
    public const SIGN_POSITIVE_FINITE = 2;
    /** Indicates that a value is negative and finite. */
    public const SIGN_NEGATIVE_FINITE = -2;
    /** Indicates that a value is negative and infinite. */
    public const SIGN_POSITIVE_INFINITE = 3;
    /** Indicates that a value is negative and infinite. */
    public const SIGN_NEGATIVE_INFINITE = -3;

    // ---------------------------------------------------------

    /**
     * unscaled string
     *
     * @var string
     */
    private $value;

    /**
     * scale
     *
     * @var int
     */
    private $scale;

    /**
     * sign value
     *
     * @var int
     */
    private $sign;

    // ---------------------------------------------------------

    /**
     * private constructor.
     *
     * @param string    $value unscaled string
     * @param integer   $scale scale
     */
    private function __construct(string $value, int $scale)
    {
        parent::__construct();
        list($this->value, $this->sign) = self::signValue($value);
        $this->scale = $scale;
    }

    // ---------------------------------------------------------

    /**
     * Returns The value NaN
     *
     * @return BigDecimal NaN
     */
    public static function NaN(): BigDecimal
    {
        return self::cached(self::VALUE_NaN);
    }

    /**
     * Returns The value INF
     *
     * @return BigDecimal INF
     */
    public static function PositiveInfinite(): BigDecimal
    {
        return self::cached(self::VALUE_POSITIVE_INFINITE);
    }

    /**
     * Returns The value -INF
     *
     * @return BigDecimal -INF
     */
    public static function NegativeInfinite(): BigDecimal
    {
        return self::cached(self::VALUE_NEGATIVE_INFINITE);
    }

    /**
     * Returns The value 0, with a scale of 0.
     *
     * @return BigDecimal BigDecimal(0, 0)
     */
    public static function Zero(): BigDecimal
    {
        return self::cached('0');
    }

    /**
     * Returns The value 1, with a scale of 0.
     *
     * @return BigDecimal BigDecimal(1, 0)
     */
    public static function One(): BigDecimal
    {
        return self::cached('1');
    }

    /**
     * Returns The value 10, with a scale of 0.
     *
     * @return BigDecimal BigDecimal(10, 0)
     */
    public static function Ten(): BigDecimal
    {
        return self::cached('10');
    }

    /**
     * Returns The value 100, with a scale of 0.
     *
     * @return BigDecimal BigDecimal(100, 0)
     */
    public static function OneHundred(): BigDecimal
    {
        return self::cached('100');
    }

    // ---------------------------------------------------------

    /**
     * Translates unscaled value and an int scale into a BigDecimal.
     *
     * - __clear the MathContext error.__
     *
     * @param integer $unscaled_value   unscaled value of the BigDecimal.
     * @param integer $scale            scale of the BigDecimal.
     *
     * @return BigDecimal
     */
    public static function valueOfUnscaledVal(int $unscaled_value, int $scale): BigDecimal
    {
        self::clearError();
        return self::make((string)$unscaled_value, $scale);
    }

    /**
     * Translates the BigDecimal representation into a BigDecimal.
     *
     * - __clear the Context error.__
     *
     * @param BigDecimal|string|int|float $value BigDecimal representation of a BigDecimal
     *
     * @return BigDecimal|null
     *
     * @throws NumberFormatException if value is not a valid representation of a BigDecimal.
     */
    public static function valueOf($value): ?BigDecimal
    {
        self::clearError();
        return self::_valueOf($value);
    }

    // ---------------------------------------------------------

    /**
     * Returns a BigDecimal whose value is (this + augend).
     *
     * - scale is `max(this.scale(), augend.scale())`.
     * - __clear the Context error.__
     *
     * @param BigDecimal|string|int|float $augend value to be added to this BigDecimal.
     *
     * @return BigDecimal|null `this + augend`
     *
     * @throws NumberFormatException if value is not a valid representation of a BigDecimal.
     */
    public function add($augend): ?BigDecimal
    {
        self::clearError();
        $augend = self::_valueOf($augend);
        if (\is_null($augend)) {
            return null;
        }

        $sv = $this->calcSpecialValue($augend, "+");
        if (!\is_null($sv)) {
            return $sv;
        }

        if (($augend->sign === self::SIGN_POSITIVE_ZERO) && ($augend->scale <= $this->scale)) {
            // X + 0
            return $this;
        }

        if (($this->sign === self::SIGN_POSITIVE_ZERO) && ($this->scale <= $augend->scale)) {
            // 0 + X
            return $augend;
        }

        list($a, $b) = self::sameScaleValues($this, $augend);
        $result = IntegerCalculator::get()->add($a, $b);
        $new_scale = $this->scale > $augend->scale ? $this->scale : $augend->scale;

        return self::make($result, $new_scale);
    }

    /**
     * Returns a BigDecimal whose value is (this - subtrahend).
     *
     * - scale is `max(this.scale(), subtrahend.scale())`.
     * - __clear the Context error.__
     *
     * @param BigDecimal|string|int|float $subtrahend value to be subtracted from this BigDecimal.
     *
     * @return BigDecimal|null `this - subtrahend`
     *
     * @throws NumberFormatException if value is not a valid representation of a BigDecimal.
     */
    public function subtract($subtrahend): ?BigDecimal
    {
        self::clearError();
        $subtrahend = self::_valueOf($subtrahend);
        if (\is_null($subtrahend)) {
            return null;
        }

        $sv = $this->calcSpecialValue($subtrahend, "-");
        if (!\is_null($sv)) {
            return $sv;
        }

        if (($subtrahend->sign === self::SIGN_POSITIVE_ZERO) && ($subtrahend->scale <= $this->scale)) {
            // X - 0
            return $this;
        }

        list($a, $b) = self::sameScaleValues($this, $subtrahend);
        $result = IntegerCalculator::get()->sub($a, $b);
        $new_scale = $this->scale > $subtrahend->scale ? $this->scale : $subtrahend->scale;

        return self::make($result, $new_scale);
    }

    /**
     * Returns a BigDecimal whose value is (this × multiplicand).
     *
     * - scale is `(this.scale() + multiplicand.scale())`.
     * - __clear the Context error.__
     *
     * @param BigDecimal|string|int|float $multiplicand value to be multiplied by this BigDecimal.
     *
     * @return BigDecimal|null `this * multiplicand`
     *
     * @throws NumberFormatException if value is not a valid representation of a BigDecimal.
     * @throws ArithmeticException (overflow or underflow) if the new scale is out of range.
     */
    public function multiply($multiplicand): ?BigDecimal
    {
        self::clearError();
        $multiplicand = self::_valueOf($multiplicand);
        if (\is_null($multiplicand)) {
            return null;
        }

        $sv = $this->calcSpecialValue($multiplicand, "*");
        if (!\is_null($sv)) {
            return $sv;
        }

        if ($multiplicand->value === '1' && $multiplicand->scale === 0) {
            return $this;
        }

        if ($this->value === '1' && $this->scale === 0) {
            return $multiplicand;
        }

        // TODO: check max scale

        $result = IntegerCalculator::get()->mul($this->value, $multiplicand->value);
        $new_scale = $this->scale + $multiplicand->scale;

        return self::make($result, $new_scale);
    }

    /**
     * Returns a BigDecimal whose value is (this / divisor)
     *
     * - scale is as specified
     * - The specified rounding mode is applied.
     * - __clear the Context error.__
     *
     * @param BigDecimal|string|int|float $divisor value by which this BigDecimal is to be divided.
     * @param integer|null $scale scale of the BigDecimal quotient to be returned.
     *                            null to use the scale of this number.
     * @param RoundingMode|string $rounding_mode rounding mode to apply.
     *
     * @return BigDecimal|null `this / divisor`
     *
     * @throws NumberFormatException if divisor is not a valid representation of a BigDecimal.
     * @throws IllegalArgumentException if rounding mode is not a valid representation of a RoundingMode.
     * @throws ArithmeticException If the divisor is zero, or rounding was necessary, or scale is out of range.
     */
    public function divide($divisor, ?int $scale, $rounding_mode = RoundingMode::UNNECESSARY): ?BigDecimal
    {
        self::clearError();
        $divisor = self::_valueOf($divisor);
        if (\is_null($divisor)) {
            return null;
        }

        $rounding = RoundingMode::valueOf($rounding_mode);
        if (\is_null($rounding)) {
            return self::error(IllegalArgumentException::InvalidRoundingMode($rounding_mode));
        }

        $sv = $this->calcSpecialValue($divisor, "/");
        if (!\is_null($sv)) {
            return $sv;
        }

        if ($divisor->sign === self::SIGN_POSITIVE_ZERO) {
            if ($this->sign === self::SIGN_POSITIVE_ZERO) {
                return self::error(ArithmeticException::DivisionUndefined());
            } else {
                return self::error(ArithmeticException::DivisionByZero());
            }
        }

        if (\is_null($scale)) {
            $scale = $this->scale;
        }

        // todo: check max scale

        $p = self::minScaleValue($this, $divisor->scale + $scale);
        $q = self::minScaleValue($divisor, $this->scale - $scale);

        $e = null;
        $result = IntegerCalculator::get()->divRound($p, $q, $rounding, $e);
        if (!\is_null($e)) {
            return self::error($e);
        }

        return self::make($result, $scale);
    }

    /**
     * Returns a BigDecimal whose value is (this / divisor), and whose preferred scale is (this.scale() - divisor.scale());
     * if the exact quotient cannot be represented (because it has a non-terminating decimal expansion) an ArithmeticException is thrown.
     *
     * - __clear the Context error.__
     *
     * @param BigDecimal|string|int|float $divisor value by which this BigDecimal is to be divided.
     * @return BigDecimal|null `this / divisor`
     *
     * @throws NumberFormatException if divisor is not a valid representation of a BigDecimal.
     * @throws ArithmeticException If the divisor is zero, or rounding was necessary, or scale is out of range.
     */
    public function exactlyDivide($divisor): ?BigDecimal
    {
        self::clearError();
        $divisor = self::_valueOf($divisor);
        if (\is_null($divisor)) {
            return null;
        }

        $sv = $this->calcSpecialValue($divisor, "/");
        if (!\is_null($sv)) {
            return $sv;
        }

        if ($divisor->sign === self::SIGN_POSITIVE_ZERO) {
            if ($this->sign === self::SIGN_POSITIVE_ZERO) {
                return self::error(ArithmeticException::DivisionUndefined());
            } else {
                return self::error(ArithmeticException::DivisionByZero());
            }
        }

        $preferred_scale = $this->scale - $divisor->scale;

        if ($this->sign === self::SIGN_POSITIVE_ZERO) {
            // 0 / $divisor
            return self::make("0", $preferred_scale);
        }

        /** @psalm-suppress RedundantCast */
        $scale = (int)min(
            ($this->precision() + (int)ceil(10.0 * $divisor->precision() / 3.0)),
            PHP_INT_MAX
        );

        // todo: check max scale

        $p = self::minScaleValue($this, $divisor->scale + $scale);
        $q = self::minScaleValue($divisor, $this->scale - $scale);

        $e = null;
        $result = IntegerCalculator::get()->divRound($p, $q, RoundingMode::UNNECESSARY(), $e);
        if (!\is_null($e)) {
            return self::error(new ArithmeticException("Non-terminating decimal expansion; no exact representable decimal result."));
        }

        $quotient = self::make($result, $scale)->stripTrailingZeros();
        if ($quotient->scale < $preferred_scale) {
            $quotient =  $quotient->setScale($preferred_scale);
        }

        return $quotient;
    }

    /**
     * Returns a two-element BigDecimal array containing the result of divideToIntegralValue followed by the result of remainder on the two operands.
     * if the exact quotient cannot be represented (because it has a non-terminating decimal expansion) an ArithmeticException is thrown.
     *
     * - scale is `max(this.scale() + divisor.scale())`.
     * - __clear the Context error.__
     *
     * @param BigDecimal|string|int|float $divisor value by which this BigDecimal is to be divided.
     * @return null[]|BigDecimal[] a two element BigDecimal array: the quotient (the result of divideToIntegralValue) is the initial element and the remainder is the final element.
     *
     * @throws NumberFormatException if divisor is not a valid representation of a BigDecimal.
     * @throws ArithmeticException If the divisor is zero, or rounding was necessary, or scale is out of range.
     */
    public function divideAndRemainder($divisor): array
    {
        self::clearError();
        $divisor = self::_valueOf($divisor);
        if (\is_null($divisor)) {
            return [null, null];
        }

        $sv1 = $this->calcSpecialValue($divisor, "/");
        $sv2 = $this->calcSpecialValue($divisor, "%");
        if (!\is_null($sv1)) {
            return [$sv1, $sv2];
        }

        if ($divisor->sign === self::SIGN_POSITIVE_ZERO) {
            if ($this->sign === self::SIGN_POSITIVE_ZERO) {
                $error =  self::error(ArithmeticException::DivisionUndefined());
            } else {
                $error = self::error(ArithmeticException::DivisionByZero());
            }
            return [$error, $error];
        }

        /** @var BigDecimal $divide */
        $divide = $this->divideToIntegralValue($divisor);
        /** @var BigDecimal $divide_multiply_divisor */
        $divide_multiply_divisor = $divide->multiply($divisor);
        $remainder = $this->subtract($divide_multiply_divisor);

        return [$divide, $remainder];
    }

    /**
     * Returns a BigDecimal whose value is the integer part of the quotient (this / divisor) rounded down.
     * The preferred scale of the result is (this.scale() - divisor.scale()).
     *
     * - __clear the Context error.__
     *
     * @param BigDecimal|string|int|float $divisor value by which this BigDecimal is to be divided.
     * @return BigDecimal|null `this / divisor`
     *
     * @throws NumberFormatException if divisor is not a valid representation of a BigDecimal.
     * @throws ArithmeticException If the divisor is zero, or rounding was necessary, or scale is out of range.
     */
    public function divideToIntegralValue($divisor): ?BigDecimal
    {
        self::clearError();
        $divisor = self::_valueOf($divisor);
        if (\is_null($divisor)) {
            return null;
        }

        $sv = $this->calcSpecialValue($divisor, "/");
        if (!\is_null($sv)) {
            return $sv;
        }

        if ($divisor->sign === self::SIGN_POSITIVE_ZERO) {
            if ($this->sign === self::SIGN_POSITIVE_ZERO) {
                return self::error(ArithmeticException::DivisionUndefined());
            } else {
                return self::error(ArithmeticException::DivisionByZero());
            }
        }

        $preferred_scale = $this->scale - $divisor->scale;

        if ($this->sign === self::SIGN_POSITIVE_ZERO) {
            // 0 / $divisor
            return self::make("0", $preferred_scale);
        }

        /** @psalm-suppress RedundantCast */
        $max_digits = (int)min(
            ($this->precision() +
                ((int)ceil(10.0 * $divisor->precision() / 3.0)) +
                abs($this->scale() - $divisor->scale()) + 2
            ),
            PHP_INT_MAX
        );

        // todo: check max scale

        $p = self::minScaleValue($this, ($divisor->scale + $max_digits));
        $q = self::minScaleValue($divisor, ($this->scale - $max_digits));

        $e = null;
        $result = IntegerCalculator::get()->divRound($p, $q, RoundingMode::DOWN(), $e);

        $quotient = self::make($result, $max_digits);

        if ($quotient->scale > 0) {
            /** @var BigDecimal $quotient */
            $quotient = $quotient->setScale(0, RoundingMode::DOWN());
            $quotient = $quotient->stripTrailingZeros();
        }

        if ($quotient->scale < $preferred_scale) {
            $quotient =  $quotient->setScale($preferred_scale);
        }

        return $quotient;
    }

    /**
     * Returns a BigDecimal whose value is (this % divisor).
     *
     * - scale is `max(this.scale() + divisor.scale())`.
     * - __clear the Context error.__
     *
     * @param BigDecimal|string|int|float $divisor this % divisor.
     *
     * @return BigDecimal|null `this % divisor`.
     *
     * @throws NumberFormatException if divisor is not a valid representation of a BigDecimal.
     * @throws ArithmeticException If the divisor is zero.
     */
    public function remainder($divisor): ?BigDecimal
    {
        return $this->divideAndRemainder($divisor)[1];
    }

    /**
     * Returns a BigDecimal whose scale is the specified value
     *
     * - __clear the Context error.__
     *
     * @param integer $scale scale of the BigDecimal value to be returned.
     * @param RoundingMode|string $rounding_mode The rounding mode to apply.
     *
     * @return BigDecimal|null a BigDecimal whose scale is the specified value,
     *
     * @throws IllegalArgumentException if rounding mode is not a valid representation of a RoundingMode.
     * @throws ArithmeticException If rounding was necessary.
     */
    public function setScale(int $scale, $rounding_mode = RoundingMode::UNNECESSARY): ?BigDecimal
    {

        self::clearError();
        $rounding = RoundingMode::valueOf($rounding_mode);
        if (\is_null($rounding)) {
            return self::error(IllegalArgumentException::InvalidRoundingMode($rounding_mode));
        }

        if (!$this->isFinite()) {
            return $this;
        }

        if ($scale === $this->scale) {
            return $this;
        }

        return $this->divide(BigDecimal::one(), $scale, $rounding_mode);
    }

    /**
     * Returns a BigDecimal which is numerically equal to this one but with any trailing zeros removed from the representation.
     *
     * @return BigDecimal a numerically equal BigDecimal with any trailing zeros removed.
     */
    public function stripTrailingZeros(): BigDecimal
    {

        if (!$this->isFinite()) {
            return $this;
        }

        if ($this->sign === self::SIGN_POSITIVE_ZERO) {
            return self::Zero();
        }

        $trimmable_zeros = \strlen($this->value) - \strlen(\rtrim($this->value, '0'));

        if ($trimmable_zeros === 0) {
            return $this;
        }

        $value = \substr($this->value, 0, -$trimmable_zeros);
        $new_scale = $this->scale - $trimmable_zeros;

        return self::make($value, $new_scale);
    }

    /**
     * Returns a BigDecimal whose value is (-this), and whose scale is this.scale().
     *
     * @return BigDecimal `-this`.
     */
    public function negate(): BigDecimal
    {
        switch ($this->sign) {
            case self::SIGN_NaN:
                return $this;
            case self::SIGN_POSITIVE_ZERO:
                return $this;
            case self::SIGN_POSITIVE_FINITE:
                return self::make('-' . $this->value, $this->scale);
            case self::SIGN_NEGATIVE_FINITE:
                return self::make(\substr($this->value, 1), $this->scale);
            case self::SIGN_POSITIVE_INFINITE:
                return static::NegativeInfinite();
            case self::SIGN_NEGATIVE_INFINITE:
                return static::PositiveInfinite();
            default:
                return $this;
        }
    }

    /**
     * Returns a BigDecimal whose value is the absolute value of this BigDecimal.
     *
     * @return BigDecimal `abs(this)`.
     */
    public function abs(): Numeric
    {
        switch ($this->sign) {
            case self::SIGN_NEGATIVE_FINITE:
            case self::SIGN_NEGATIVE_INFINITE:
                return $this->negate();
            default:
                return $this;
        }
    }

    /**
     * Returns the scale of this BigDecimal.
     *
     * - If zero or positive, the scale is the number of digits to the right of the decimal point.
     * - If negative, the unscaled value of the number is multiplied by ten to the power of the negation of the scale.
     *
     * @return integer the scale of this BigDecimal.
     */
    public function scale(): int
    {
        return $this->scale;
    }

    /**
     * Returns the precision of this BigDecimal.
     *
     * - The precision of a zero value is 1.
     * - 0 if BigDecimal is NaN or -Infinity or Infinity.
     *
     * @return integer the precision of this BigDecimal.
     */
    public function precision(): int
    {
        switch ($this->sign) {
            case self::SIGN_NaN:
            case self::SIGN_POSITIVE_INFINITE:
            case self::SIGN_NEGATIVE_INFINITE:
                return 0;
            case self::SIGN_NEGATIVE_FINITE:
                return \strlen($this->value) - 1;
            default:
                return \strlen($this->value);
        }
    }

    // ---------------------------------------------------------

    /**
     * Returns the sign of the value.
     *
     * The specific value returned indicates the type and sign of the BigDecimal, as follows:
     * - `BigDecimal::SIGN_NaN` value is Not a Number
     * - `BigDecimal::SIGN_POSITIVE_ZERO` value is 0
     * - `BigDecimal::SIGN_POSITIVE_INFINITE` value is +Infinity
     * - `BigDecimal::SIGN_NEGATIVE_INFINITE` value is -Infinity
     * - `BigDecimal::SIGN_POSITIVE_FINITE` value is positive
     * - `BigDecimal::SIGN_NEGATIVE_FINITE` value is negative
     *
     * @return integer
     */
    public function sign(): int
    {
        return $this->sign;
    }

    /**
     * Returns True if the value is positive.
     *
     * @return boolean true if BigDecimal is positive
     */
    public function isPositive(): bool
    {
        return self::SIGN_POSITIVE_FINITE <= $this->sign;
    }

    /**
     * Returns True if the value is negative.
     *
     * @return boolean true if BigDecimal is negative
     */
    public function isNegative(): bool
    {
        return $this->sign <= self::SIGN_NEGATIVE_FINITE;
    }

    /**
     * Returns True if the value is finite (not (NaN or infinite)).
     *
     * @return boolean true if BigDecimal is finite
     */
    public function isFinite(): bool
    {
        return $this->infinite() === 0 && (!$this->isNan());
    }

    /**
     * Returns 0, -1, or +1 depending on whether the value is finite, -Infinity, or +Infinity.
     *
     * @return integer 0 if BigDecimal is finite or NaN, -1 if BigDecimal is -Infinity, 1 if BigDecimal is Infinity.
     */
    public function infinite(): int
    {
        switch ($this->sign) {
            case self::SIGN_POSITIVE_INFINITE:
                return 1;
            case self::SIGN_NEGATIVE_INFINITE:
                return -1;
            default:
                return 0;
        }
    }

    /**
     * Returns True if the value is NaN.
     *
     * @return boolean true if BigDecimal is NaN
     */
    public function isNan(): bool
    {
        return $this->sign === self::SIGN_NaN;
    }

    /**
     * Returns True if the value is Zero.
     *
     * - scale is ignored.
     *
     * @return boolean true if BigDecimal is 0
     */
    public function isZero(): bool
    {
        return $this->sign === self::SIGN_POSITIVE_ZERO;
    }

    /**
     * Returns self if the value is non-zero, null otherwise.
     *
     * - scale is ignored.
     *
     * @return BigDecimal|null $this if BigDecimal is non-zero, null if BigDecimal is zero
     */
    public function nonZero(): ?Numeric
    {
        if ($this->sign === self::SIGN_POSITIVE_ZERO) {
            return null;
        } else {
            return $this;
        }
    }

    /**
     * Compares this BigDecimal with the specified BigDecimal.
     *
     * - __clear the Context error.__
     *
     * @param mixed $that BigDecimal to which this BigDecimal is to be compared.
     *
     * @return integer|null if a == b, 1 if a > b, -1 if a < b, null if a or b is NaN
     */
    public function compareTo($that): ?int
    {
        self::clearError();
        $that = self::_valueOf($that);
        if (\is_null($that)) {
            return null;
        }

        $left_sign = $this->sign;
        $right_sign = $that->sign;

        if (($left_sign == self::SIGN_NaN) || ($right_sign == self::SIGN_NaN)) {
            // NAN && X or NAN && X
            return null;
        }

        if ($left_sign == self::SIGN_POSITIVE_INFINITE) {
            if ($right_sign == self::SIGN_POSITIVE_INFINITE) {
                // INF && INF
                return 0;
            } else {
                // INF && (-INF || number)
                return 1;
            }
        } else if ($left_sign == self::SIGN_NEGATIVE_INFINITE) {
            if ($right_sign == self::SIGN_NEGATIVE_INFINITE) {
                // -INF && -INF
                return 0;
            } else {
                // -INF && (INF || number)
                return -1;
            }
        } else {
            if ($right_sign == self::SIGN_POSITIVE_INFINITE) {
                // number && INF
                return -1;
            } else if ($right_sign == self::SIGN_NEGATIVE_INFINITE) {
                // number && -INF
                return 1;
            }
        }

        list($a, $b) = self::sameScaleValues($this, $that);

        return IntegerCalculator::get()->compare($a, $b);
    }

    /**
     * Compares this BigDecimal with the specified Object for equality.
     *
     * - __clear the Context error.__
     *
     * @param BigDecimal|string|int|float $that Object to which this BigDecimal is to be compared.
     *
     * @return boolean true if value and scale equal.
     */
    public function equalsExact($that): bool
    {
        self::clearError();
        if (!($that instanceof self)) {
            return false;
        }
        /** @var BigDecimal $that */
        $that = self::_valueOf($that);
        if ($this->isNan() || $that->isNan()) {
            return false;
        }
        return $this->value === $that->value && $this->scale === $that->scale;
    }

    // ---------------------------------------------------------

    /**
     * Converts this BigDecimal to an int.
     *
     * @return integer this BigDecimal converted to an int.
     */
    public function toInt(): int
    {
        switch ($this->sign) {
            case self::SIGN_NaN:
                return (int)NAN;
            case self::SIGN_POSITIVE_INFINITE:
                return (int)INF;
            case self::SIGN_NEGATIVE_INFINITE:
                return (int)-INF;
            default:
                break;
        }
        return \intval($this->toS());
    }

    /**
     * Converts this BigDecimal to an float.
     *
     * @return float this BigDecimal converted to an float.
     */
    public function toFloat(): float
    {
        switch ($this->sign) {
            case self::SIGN_NaN:
                return NAN;
            case self::SIGN_POSITIVE_INFINITE:
                return INF;
            case self::SIGN_NEGATIVE_INFINITE:
                return -INF;
            default:
                break;
        }
        return \floatval($this->toS());
    }

    /**
     * Returns a string representation of this BigDecimal without an exponent field.
     *
     * @return string a string representation of this BigDecimal without an exponent field.
     */
    public function toPlainString(): string
    {
        return $this->toS();
    }

    /**
     * Returns a string representation of this BigDecimal.
     *
     * @see BigDecimal::toPlainString()
     *
     * @return string
     */
    public function __toString(): string
    {
        return $this->toS();
    }

    /**
     * {@inheritdoc}
     */
    #[\ReturnTypeWillChange]
    public function jsonSerialize()
    {
        return $this->toS();
    }

    // ---------------------------------------------------------

    /**
     * {@inheritdoc}
     */
    public function __serialize(): array
    {
        return [
            $this->value,
            $this->scale,
        ];
    }

    /**
     * {@inheritdoc}
     * @param array{string,int} $data
     */
    public function __unserialize(array $data): void
    {
        list($value, $scale) = $data;
        /** @var string $value */
        /** @var int $scale */
        /** @psalm-suppress DirectConstructorCall */
        $this->__construct($value, $scale);
    }

    /**
     * Translates the BigDecimal representation into a BigDecimal.
     *
     * @param mixed $number
     * @return BigDecimal|null
     */
    private static function _valueOf($number): ?BigDecimal
    {

        if ($number instanceof self) {
            return $number;
        }

        $number_str = Helper::convertString($number);

        $tmp = Helper::parse($number_str);

        if (empty($tmp)) {
            return self::error(new NumberFormatException(sprintf('does not support number "%s"', $number_str)));
        }

        // TODO check max scale

        return self::make($tmp["value"], $tmp["scale"]);
    }

    /**
     * make BigDecimal instance
     *
     * @param string $v
     * @param integer $scale
     * @return BigDecimal
     */
    private static function make(string $v, int $scale): BigDecimal
    {

        list($value, $sign) = self::signValue($v);

        switch ($sign) {
            case self::SIGN_NaN:
                return self::NaN();
            case self::SIGN_POSITIVE_INFINITE:
                return self::PositiveInfinite();
            case self::SIGN_NEGATIVE_INFINITE:
                return self::NegativeInfinite();
            case self::SIGN_POSITIVE_ZERO:
                if ($scale === 0) {
                    return self::Zero();
                }
                break;
            default:
                if ($scale === 0) {
                    if ($value === "1") {
                        return self::One();
                    } else if ($value === "10") {
                        return self::Ten();
                    } else if ($value === "100") {
                        return self::OneHundred();
                    }
                }
                break;
        }
        return new static($value, $scale);
    }

    /**
     * instance cache
     *
     * @var array<string,array<int,BigDecimal>>
     */
    private static $cached_instances = [];

    /**
     * cache BigDecimal
     *
     * @param string $value
     * @param integer $scale
     * @return BigDecimal
     */
    private static function cached(string $value, int $scale = 0): BigDecimal
    {
        if (!isset(self::$cached_instances[$value][$scale])) {
            self::$cached_instances[$value][$scale] = new static($value, $scale);
        }
        return self::$cached_instances[$value][$scale];
    }

    /**
     * to string
     *
     * @return string
     */
    private function toS(): string
    {
        list($i, $d) = self::splitif($this->value, $this->scale);
        if ($d === "") {
            return $i;
        } else {
            return $i . '.' . $d;
        }
    }

    /**
     * convert integer_part and fractional_part pair array
     *
     * @param string $value
     * @param integer $scale
     * @return string[]
     */
    private static function splitif(string $value, int $scale): array
    {
        if (0 < $scale) {
            return self::splitifPlusScale($value, $scale);
        } else if ($scale < 0) {
            return self::splitifMinusScale($value, $scale);
        }
        // int, NAN, INF, -INF
        return [$value, ""];
    }

    /**
     * convert integer_part and fractional_part pair array plus scale
     *
     * @param string $value
     * @param integer $scale
     * @return string[]
     */
    private static function splitifPlusScale(string $value, int $scale): array
    {
        // decimal

        $padvalue = self::createLeadingZeroValue($value, $scale);

        $integer_part = \substr($padvalue, 0, -$scale);

        $fractional_part = \substr($padvalue, -$scale);

        return [$integer_part, $fractional_part];
    }

    /**
     * convert integer_part and fractional_part pair array minus scale
     *
     * @param string $value
     * @param integer $scale
     * @return string[]
     */
    private static function splitifMinusScale(string $value, int $scale): array
    {
        // integer
        if ($value === "0") {
            $integer_part = $value;
        } else {
            $integer_part = $value . \str_repeat('0', -$scale);
        }
        return [$integer_part, ''];
    }

    /**
     * add leading zero
     *
     * @param string $value
     * @param integer $scale
     * @return string
     */
    private static function createLeadingZeroValue(string $value, int $scale): string
    {

        $target_length = $scale + 1;
        $length = \strlen($value);

        if ($value[0] === '-') {

            if ($target_length <= ($length - 1)) {
                return $value;
            }

            // remove '-'
            $value = \substr($value, 1);
            $value = \str_pad($value, $target_length, '0', STR_PAD_LEFT);
            $value = '-' . $value;
        } else {

            if ($target_length <= $length) {
                return $value;
            }

            $value = \str_pad($value, $target_length, '0', STR_PAD_LEFT);
        }

        return $value;
    }

    /**
     * Align to the specified minimum scale
     *
     * @param BigDecimal $x
     * @param integer $minimum_scale
     * @return string
     */
    private static function minScaleValue(BigDecimal $x, int $minimum_scale): string
    {

        $x_value = $x->value;

        if ($x->scale < $minimum_scale) {
            if ($x->sign !== self::SIGN_POSITIVE_ZERO) {
                $x_value = $x_value . \str_repeat('0', $minimum_scale - $x->scale);
            }
        }

        return $x_value;
    }

    /**
     * Align to the same scale
     *
     * @param BigDecimal $x
     * @param BigDecimal $y
     * @return string[]
     */
    private static function sameScaleValues(BigDecimal $x, BigDecimal $y): array
    {

        $x_value = $x->value;
        $y_value = $y->value;

        if (($x->scale > $y->scale)) {
            if ($y->sign !== self::SIGN_POSITIVE_ZERO) {
                $y_value = $y_value . \str_repeat('0', $x->scale - $y->scale);
            }
        } else if (($x->scale < $y->scale)) {
            if ($x->sign !== self::SIGN_POSITIVE_ZERO) {
                $x_value = $x_value . \str_repeat('0', $y->scale - $x->scale);
            }
        }

        return [$x_value, $y_value];
    }

    /**
     * convert sign and correction value
     *
     * @param string $unscaled_value
     * @return array{string,int}
     */
    private static function signValue(string $unscaled_value): array
    {
        if ($unscaled_value === "0") {
            $sign = self::SIGN_POSITIVE_ZERO;
        } else if ($unscaled_value === "-0") {
            $unscaled_value = "0";
            $sign = self::SIGN_POSITIVE_ZERO;
        } else if ($unscaled_value === self::VALUE_NaN) {
            $sign = self::SIGN_NaN;
        } else if ($unscaled_value === self::VALUE_POSITIVE_INFINITE) {
            $sign = self::SIGN_POSITIVE_INFINITE;
        } else if ($unscaled_value === self::VALUE_NEGATIVE_INFINITE) {
            $sign = self::SIGN_NEGATIVE_INFINITE;
        } else {
            $sign = (($unscaled_value[0] === '-') ? self::SIGN_NEGATIVE_FINITE : self::SIGN_POSITIVE_FINITE);
        }
        return [$unscaled_value, $sign];
    }

    /**
     * special value op define
     *
     * @var array<int,array<int,array<string,int|BigDecimal|null>>>
     */
    private static $special_value_defs = [];

    /**
     * special value op
     *
     * @param BigDecimal $val
     * @param string $op
     * @return BigDecimal|null
     */
    private function calcSpecialValue(BigDecimal $val, string $op): ?BigDecimal
    {

        if (empty(self::$special_value_defs)) {
            self::$special_value_defs = [
                self::SIGN_NaN => [
                    self::SIGN_NaN => ['+' => self::NaN(), '-' => self::NaN(), '*' => self::NaN(), '/' => self::NaN(), '%' => self::NaN(),],
                    self::SIGN_POSITIVE_ZERO => ['+' => self::NaN(), '-' => self::NaN(), '*' => self::NaN(), '/' => self::NaN(), '%' => self::NaN(),],
                    self::SIGN_POSITIVE_FINITE => ['+' => self::NaN(), '-' => self::NaN(), '*' => self::NaN(), '/' => self::NaN(), '%' => self::NaN(),],
                    self::SIGN_NEGATIVE_FINITE => ['+' => self::NaN(), '-' => self::NaN(), '*' => self::NaN(), '/' => self::NaN(), '%' => self::NaN(),],
                    self::SIGN_POSITIVE_INFINITE => ['+' => self::NaN(), '-' => self::NaN(), '*' => self::NaN(), '/' => self::NaN(), '%' => self::NaN(),],
                    self::SIGN_NEGATIVE_INFINITE => ['+' => self::NaN(), '-' => self::NaN(), '*' => self::NaN(), '/' => self::NaN(), '%' => self::NaN(),],
                ],
                self::SIGN_POSITIVE_ZERO => [
                    self::SIGN_NaN => ['+' => self::NaN(), '-' => self::NaN(), '*' => self::NaN(), '/' => self::NaN(), '%' => self::NaN(),],
                    // self::SIGN_POSITIVE_ZERO => ['+' => null, '-' => null, '*' => null, '/' => null, '%' => null,],
                    // self::SIGN_POSITIVE_FINITE => ['+' => null, '-' => null, '*' => null, '/' => null, '%' => null,],
                    // self::SIGN_NEGATIVE_FINITE => ['+' => null, '-' => null, '*' => null, '/' => null, '%' => null,],
                    self::SIGN_POSITIVE_INFINITE => ['+' => self::PositiveInfinite(), '-' => self::NegativeInfinite(), '*' => self::NaN(), '/' => self::Zero(), '%' => self::Zero(),],
                    self::SIGN_NEGATIVE_INFINITE => ['+' => self::NegativeInfinite(), '-' => self::PositiveInfinite(), '*' => self::NaN(), '/' => self::Zero(), '%' => self::Zero(),],
                ],
                self::SIGN_POSITIVE_FINITE => [
                    self::SIGN_NaN => ['+' => self::NaN(), '-' => self::NaN(), '*' => self::NaN(), '/' => self::NaN(), '%' => self::NaN(),],
                    // self::SIGN_POSITIVE_ZERO => ['+' => null, '-' => null, '*' => null, '/' => null, '%' => null,],
                    // self::SIGN_POSITIVE_FINITE => ['+' => null, '-' => null, '*' => null, '/' => null, '%' => null,],
                    // self::SIGN_NEGATIVE_FINITE => ['+' => null, '-' => null, '*' => null, '/' => null, '%' => null,],
                    self::SIGN_POSITIVE_INFINITE => ['+' => self::PositiveInfinite(), '-' => self::NegativeInfinite(), '*' => self::PositiveInfinite(), '/' => self::Zero(), '%' => 1,],
                    self::SIGN_NEGATIVE_INFINITE => ['+' => self::NegativeInfinite(), '-' => self::PositiveInfinite(), '*' => self::NegativeInfinite(), '/' => self::Zero(), '%' => 1,],
                ],
                self::SIGN_NEGATIVE_FINITE => [
                    self::SIGN_NaN => ['+' => self::NaN(), '-' => self::NaN(), '*' => self::NaN(), '/' => self::NaN(), '%' => self::NaN(),],
                    // self::SIGN_POSITIVE_ZERO => ['+' => null, '-' => null, '*' => null, '/' => null, '%' => null,],
                    // self::SIGN_POSITIVE_FINITE => ['+' => null, '-' => null, '*' => null, '/' => null, '%' => null,],
                    // self::SIGN_NEGATIVE_FINITE => ['+' => null, '-' => null, '*' => null, '/' => null, '%' => null,],
                    self::SIGN_POSITIVE_INFINITE => ['+' => self::PositiveInfinite(), '-' => self::NegativeInfinite(), '*' => self::NegativeInfinite(), '/' => self::Zero(), '%' => 1,],
                    self::SIGN_NEGATIVE_INFINITE => ['+' => self::NegativeInfinite(), '-' => self::PositiveInfinite(), '*' => self::PositiveInfinite(), '/' => self::Zero(), '%' => 1,],
                ],
                self::SIGN_POSITIVE_INFINITE => [
                    self::SIGN_NaN => ['+' => self::NaN(), '-' => self::NaN(), '*' => self::NaN(), '/' => self::NaN(), '%' => self::NaN(),],
                    self::SIGN_POSITIVE_ZERO => ['+' => self::PositiveInfinite(), '-' => self::PositiveInfinite(), '*' => self::NaN(), '/' => null /* self::PositiveInfinite() */, '%' => null /* self::NaN() */,],
                    self::SIGN_POSITIVE_FINITE => ['+' => self::PositiveInfinite(), '-' => self::PositiveInfinite(), '*' => self::PositiveInfinite(), '/' => self::PositiveInfinite(), '%' => self::NaN(),],
                    self::SIGN_NEGATIVE_FINITE => ['+' => self::PositiveInfinite(), '-' => self::PositiveInfinite(), '*' => self::NegativeInfinite(), '/' => self::NegativeInfinite(), '%' => self::NaN(),],
                    self::SIGN_POSITIVE_INFINITE => ['+' => self::PositiveInfinite(), '-' => self::NaN(), '*' => self::PositiveInfinite(), '/' => self::NaN(), '%' => self::NaN(),],
                    self::SIGN_NEGATIVE_INFINITE => ['+' => self::NaN(), '-' => self::PositiveInfinite(), '*' => self::NegativeInfinite(), '/' => self::NaN(), '%' => self::NaN(),],
                ],
                self::SIGN_NEGATIVE_INFINITE => [
                    self::SIGN_NaN => ['+' => self::NaN(), '-' => self::NaN(), '*' => self::NaN(), '/' => self::NaN(), '%' => self::NaN(),],
                    self::SIGN_POSITIVE_ZERO => ['+' => self::NegativeInfinite(), '-' => self::NegativeInfinite(), '*' => self::NaN(), '/' => null /* self::NegativeInfinite() */, '%' => null /* self::NaN() */,],
                    self::SIGN_POSITIVE_FINITE => ['+' => self::NegativeInfinite(), '-' => self::NegativeInfinite(), '*' => self::NegativeInfinite(), '/' => self::NegativeInfinite(), '%' => self::NaN(),],
                    self::SIGN_NEGATIVE_FINITE => ['+' => self::NegativeInfinite(), '-' => self::NegativeInfinite(), '*' => self::PositiveInfinite(), '/' => self::PositiveInfinite(), '%' => self::NaN(),],
                    self::SIGN_POSITIVE_INFINITE => ['+' => self::NaN(), '-' => self::NegativeInfinite(), '*' => self::NegativeInfinite(), '/' => self::NaN(), '%' => self::NaN(),],
                    self::SIGN_NEGATIVE_INFINITE => ['+' => self::NegativeInfinite(), '-' => self::NaN(), '*' => self::PositiveInfinite(), '/' => self::NaN(), '%' => self::NaN(),],
                ],
            ];
        }

        $left_sign = $this->sign;
        $right_sign = $val->sign;

        $v =  self::$special_value_defs[$left_sign][$right_sign][$op] ?? null;
        if (is_int($v)) {
            return $this;
        }
        return $v;
    }

    /**
     * error action
     *
     * @param MathRuntimeException $e
     * @return BigDecimal|null
     */
    private static function error(MathRuntimeException $e)
    {
        $context = self::getContext();
        $context->setError($e);
        switch ($context->getErrorMode()) {
            case MathContext::ERROR_MODE_RETURN_NAN:
                return self::NaN();
            case MathContext::ERROR_MODE_RETURN_NULL:
                return null;
            case MathContext::ERROR_MODE_THROW_EXCEPTION:
                throw $e;
        }
        throw $e;
    }
}
