<?php

declare(strict_types=1);

namespace Kamiyonanayo\Math\Internals;

use Kamiyonanayo\Math\Exception\ArithmeticException;
use Kamiyonanayo\Math\Exception\MathRuntimeException;
use Kamiyonanayo\Math\Internals\Arithmetic\BCMathArithmetic;
use Kamiyonanayo\Math\Internals\Arithmetic\Arithmetic;
use Kamiyonanayo\Math\RoundingMode;

/**
 * @internal internal utils
 */
class IntegerCalculator
{

    /** @var IntegerCalculator|null */
    private static $calc = null;

    public static function get(): IntegerCalculator
    {
        if (\is_null(self::$calc)) {
            self::$calc = self::create();
        }
        return self::$calc;
    }

    /** @var Arithmetic */
    private $arithmetic;

    private function __construct(Arithmetic $arithmetic)
    {
        $this->arithmetic = $arithmetic;
    }

    // 整数加算
    public function add(string $left_operand, string $right_operand): string
    {
        return $this->arithmetic->add($left_operand, $right_operand);
    }

    // 整数減算
    public function sub(string $left_operand, string $right_operand): string
    {
        return $this->arithmetic->sub($left_operand, $right_operand);
    }

    // 整数乗算
    public function mul(string $left_operand, string $right_operand): string
    {
        return $this->arithmetic->mul($left_operand, $right_operand);
    }

    // 整数除算
    public function div(string $dividend, string $divisor): string
    {
        return $this->arithmetic->div($dividend, $divisor);
    }

    // 整数剰余
    public function mod(string $dividend, string $divisor): string
    {
        return $this->arithmetic->mod($dividend, $divisor);
    }

    public function divRound(string $dividend, string $divisor, RoundingMode $rounding_mode, ?MathRuntimeException &$exception): string
    {
        $quotient = $this->div($dividend, $divisor);
        $remainder = $this->mod($dividend, $divisor);

        $has_discarded_fraction = ($remainder !== '0');
        $is_positive_or_zero = ($dividend[0] === '-') === ($divisor[0] === '-');

        $discarded_fraction_sign = function () use ($remainder, $divisor): int {
            $r = $this->abs($this->mul($remainder, '2'));
            $d = $this->abs($divisor);
            return $this->compare($r, $d);
        };

        $increment = false;

        switch ($rounding_mode->value()) {
            case RoundingMode::UNNECESSARY:
                if ($has_discarded_fraction) {
                    $exception = ArithmeticException::RoundingNecessary();
                    return "";
                }
                break;
            case RoundingMode::UP:
                // 余りがあるかどうか
                $increment = $has_discarded_fraction;
                break;
            case RoundingMode::DOWN:
                // 0に近づくので何もしない
                break;
            case RoundingMode::CEILING:
                // 余りがあって正の場合
                $increment = ($has_discarded_fraction && $is_positive_or_zero);
                break;
            case RoundingMode::FLOOR:
                // 余りがあって負の場合
                $increment = ($has_discarded_fraction && !$is_positive_or_zero);
                break;
            case RoundingMode::HALF_UP:
                // 余りが5以上の場合
                $increment = $discarded_fraction_sign() >= 0;
                break;
            case RoundingMode::HALF_DOWN:
                // 余りが5より大きい場合
                $increment = $discarded_fraction_sign() > 0;
                break;
            case RoundingMode::HALF_EVEN:
                // 繰り上がる可能性のある桁の偶数、奇数判定
                $last_digit = (int) \substr($quotient, -1);
                $last_digit_is_even = ($last_digit % 2 === 0);
                if ($last_digit_is_even) {
                    // 偶数の場合 HALF_DOWN の動き
                    $increment = $discarded_fraction_sign() > 0;
                } else {
                    // 奇数の場合 HALF_UP の動き
                    $increment = $discarded_fraction_sign() >= 0;
                }
                break;
            case RoundingMode::HALF_ODD:
                // 繰り上がる可能性のある桁の偶数、奇数判定
                $last_digit = (int) \substr($quotient, -1);
                $last_digit_is_even = ($last_digit % 2 === 0);
                if ($last_digit_is_even) {
                    // 偶数の場合 HALF_UP の動き
                    $increment = $discarded_fraction_sign() >= 0;
                } else {
                    // 奇数の場合 HALF_DOWN の動き
                    $increment = $discarded_fraction_sign() > 0;
                }
                break;
            case RoundingMode::HALF_CEILING:
                if ($is_positive_or_zero) {
                    // 正の場合 HALF_UP の動き
                    $increment = $discarded_fraction_sign() >= 0;
                } else {
                    // 負の場合 HALF_DOWN の動き
                    $increment = $discarded_fraction_sign() > 0;
                }
                break;
            case RoundingMode::HALF_FLOOR:
                if ($is_positive_or_zero) {
                    // 正の場合 HALF_DOWN の動き
                    $increment = $discarded_fraction_sign() > 0;
                } else {
                    // 負の場合 HALF_UP の動き
                    $increment = $discarded_fraction_sign() >= 0;
                }
                break;
        }

        if ($increment) {
            return $this->add($quotient, $is_positive_or_zero ? '1' : '-1');
        } else {
            return $quotient;
        }
    }

    // 絶対値
    public function abs(string $n): string
    {
        return (($n !== "") && ($n[0] === '-')) ? \substr($n, 1) : $n;
    }

    // 整数比較
    public function compare(string $left_operand, string $right_operand): int
    {

        $left_operand_negative = ($left_operand !== "") && ($left_operand[0] === '-');
        $right_operand_negative = ($right_operand !== "") && ($right_operand[0] === '-');

        if ($left_operand_negative && !$right_operand_negative) {
            // - && +
            return -1;
        }

        if (!$left_operand_negative && $right_operand_negative) {
            // + && -
            return 1;
        }

        $left_value = $this->abs($left_operand);
        $right_value = $this->abs($right_operand);

        $left_len = \strlen($left_value);
        $right_len = \strlen($right_value);

        if ($left_len < $right_len) {
            // "100" < "1000"
            $result = -1;
        } elseif ($left_len > $right_len) {
            // "1000" > "100"
            $result = 1;
        } else {
            // same number of digits "1xx" <=> "1yy"
            // "<=>" operator is convert [-1 or 0 or 1]
            $result = (\strcmp($left_value, $right_value) <=> 0);
        }

        return $left_operand_negative ? -$result : $result;
    }

    /**
     * @codeCoverageIgnore
     * @SuppressWarnings("PMD.MissingImport")
     */
    private static function create(): IntegerCalculator
    {
        if (\function_exists('\\bcscale')) {
            return new IntegerCalculator(new BCMathArithmetic());
        }
        throw new \LogicException("requirements could not be resolved. requires bcmath");
    }
}
