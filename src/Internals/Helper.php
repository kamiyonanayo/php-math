<?php

declare(strict_types=1);

namespace Kamiyonanayo\Math\Internals;

/**
 * Internal Helper Class
 * @internal internal utils
 */
class Helper
{

    /**
     * Returns value to string
     *
     * @param mixed $value
     * @return string
     */
    public static function convertString($value): string
    {

        if (\is_float($value)) {
            return self::float_to_string($value);
        }

        if (\is_array($value)) {
            return "";
        }

        if (\is_object($value) && (!\method_exists($value, "__toString"))) {
            return "";
        }

        // @phpstan-ignore-next-line
        return (string)$value;
    }

    /**
     * Returns float to string
     *
     * @param float $value
     * @return string
     */
    private static function float_to_string(float $value): string
    {

        if (80000 <= PHP_VERSION_ID) {
            return (string) $value;
        }

        $current_locale = \setlocale(LC_NUMERIC, '0');
        \setlocale(LC_NUMERIC, 'C');
        $result = (string) $value;
        if($current_locale !== false){
            \setlocale(LC_NUMERIC, $current_locale);
        }
        return $result;
    }

    /**
     * @SuppressWarnings("PMD.NPathComplexity")
     * @return array{scale:int,value:string}|array{}
     */
    public static function parse(string $value): array
    {

        $value = \trim($value);

        if (\in_array($value, ["NAN", "INF", "+INF", "-INF"], true)) {
            return [
                "scale" => 0,
                "value" => (($value === "+INF") ? "INF" : $value),
            ];
        }

        $matches = [];
        if (!(\preg_match('/\A(?<sign>[-+]?+)(?<integer>[0-9]*+)(?:\.?(?<decimals>[0-9]*+))?+(?:[eE](?<mantissa_sign>[-+]?+)(?<mantissa>[0-9]++))?+\z/', $value, $matches) > 0)) {
            return [];
        }
        if (($matches['integer'] === "") && ($matches['decimals'] === "")) {
            return [];
        }

        $parsed_sign = "";
        $parsed_integer = "";
        $parsed_decimals = "";
        $parsed_mantissa_sign = "";
        $parsed_mantissa = "0";
        $parsed_scale = 0;
        $parsed_value = "";

        if ($matches["sign"] === "") {
            $parsed_sign = "+";
        } else {
            $parsed_sign = $matches["sign"];
        }

        $parsed_integer = \ltrim($matches["integer"], '0');
        $parsed_decimals = $matches["decimals"];
        if ($parsed_decimals === "") {
            $parsed_value = $parsed_integer;
        } else {
            $parsed_value = $parsed_integer . $parsed_decimals;
        }

        $parsed_value = \ltrim($parsed_value, '0');
        if ($parsed_value === "") {
            $parsed_value = "0";
        } else if ($parsed_sign === "-") {
            $parsed_value = "-" . $parsed_value;
        }
        $parsed_scale = \strlen($parsed_decimals);

        $parsed_mantissa_sign = isset($matches["mantissa_sign"]) ? $matches["mantissa_sign"] : "";
        if ($parsed_mantissa_sign === "") {
            $parsed_mantissa_sign = "+";
        }

        $parsed_mantissa = isset($matches["mantissa"]) ? \ltrim($matches["mantissa"], '0') : "";
        if ($parsed_mantissa === "") {
            $parsed_mantissa = 0;
        } else {
            $parsed_mantissa = $parsed_mantissa;
        }

        if ($parsed_mantissa === 0) {
        } else {
            /** @var int $parsed_mantissa */
            if ($parsed_mantissa_sign === "-") {
                $parsed_scale = $parsed_scale + $parsed_mantissa;
            } else {
                $parsed_scale = $parsed_scale - $parsed_mantissa;
            }
        }

        /** @var float|int $parsed_scale */
        if (\is_float($parsed_scale)) {
            // overflow PHP_INT_MAX,PHP_INT_MIN
            return [];
        }

        return [
            // "sign" => $parsed_sign,
            // "integer" => $parsed_integer,
            // "decimals" => $parsed_decimals,
            // "mantissa_sign" => $parsed_mantissa_sign,
            // "mantissa" => $parsed_mantissa,
            "scale" => $parsed_scale,
            "value" => $parsed_value,
        ];
    }
}
