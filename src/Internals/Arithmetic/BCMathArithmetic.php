<?php

declare(strict_types=1);

namespace Kamiyonanayo\Math\Internals\Arithmetic;

class BCMathArithmetic extends Arithmetic
{
    // 整数加算
    public function add(string $left_operand, string $right_operand): string
    {
        return \bcadd($left_operand, $right_operand, 0);
    }

    // 整数減算
    public function sub(string $left_operand, string $right_operand): string
    {
        return \bcsub($left_operand, $right_operand, 0);
    }

    // 整数乗算
    public function mul(string $left_operand, string $right_operand): string
    {
        return \bcmul($left_operand, $right_operand, 0);
    }

    // 整数除算
    public function div(string $dividend, string $divisor): string
    {
        return \bcdiv($dividend, $divisor, 0);
    }

    // 整数剰余
    public function mod(string $dividend, string $divisor): string
    {
        return \bcmod($dividend, $divisor, 0);
    }
}
