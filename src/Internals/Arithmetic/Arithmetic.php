<?php

declare(strict_types=1);

namespace Kamiyonanayo\Math\Internals\Arithmetic;

abstract class Arithmetic
{

    // 整数加算
    abstract public function add(string $left_operand, string $right_operand): string;

    // 整数減算
    abstract public function sub(string $left_operand, string $right_operand): string;

    // 整数乗算
    abstract public function mul(string $left_operand, string $right_operand): string;

    // 整数除算
    abstract public function div(string $dividend, string $divisor): string;

    // 整数剰余
    abstract public function mod(string $dividend, string $divisor): string;

}
