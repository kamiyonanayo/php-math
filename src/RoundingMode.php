<?php

declare(strict_types=1);

namespace Kamiyonanayo\Math;

/**
 * Determines what happens when a result must be rounded in order to fit in the appropriate number of significant digits.
 *
 */
final class RoundingMode
{

    // 	UNNECESSARY
    // 	DOWN 	round towards zero	round away from infinity	truncate	ToZero
    // 	UP 	round away from zero	round towards infinity		AwayFromZero
    // 	CEILING 	round up	round towards positive infinity	ceiling	ToPositiveInf
    // 	FLOOR 	round down	round towards negative infinity	floor	ToNegativeInf
    // 	HALF_DOWN 	round half towards zero	round half away from infinity		ToNearestTowardZero
    // 	HALF_UP 	round half away from zero	round half towards infinity		ToNearestAway
    // 	HALF_CEILING 	round half up	round half towards positive infinity
    // 	HALF_FLOOR 	round half down	round half towards negative infinity
    // 	HALF_EVEN 	round half to even		banker	ToNearestEven
    // 	HALF_ODD 	round half to odd


    // 要求される演算の結果が正確であり、丸めが必要でないことを表す丸めモードです。
    // この丸めモードが結果が正確でない演算で指定される場合は、ArithmeticExceptionがスローされます。
    /** Rounding mode to assert that the requested operation has an exact result, hence no rounding is necessary. */
    public const UNNECESSARY = "unnecessary";

    // 0に近づくように丸めるモードです。破棄される小数部に先行する桁を増分しません(つまり切り捨て)。この丸めモードは、計算された値の絶対値を増やしません。
    /** Indicates that values should be rounded towards zero. */
    public const DOWN = "down";

    // 0から離れるように丸めるモードです。破棄される0以外の小数部に先行する桁を常に増やします。この丸めモードは、計算された値の絶対値を減らしません。
    /** Indicates that values should be rounded away from zero. */
    public const UP = "up";

    // 正の無限大に近づくように丸めるモードです。結果が正の場合はRoundingMode.UPのように動作し、負の場合はRoundingMode.DOWNのように動作します。この丸めモードは、計算された値を減らしません。
    /** Round towards +Infinity.  */
    public const CEILING = "ceiling";

    // 負の無限大に近づくように丸めるモードです。結果が正の場合はRoundingMode.DOWNのように動作し、負の場合はRoundingMode.UPのように動作します。この丸めモードは、計算された値を増やしません。
    /** Round towards -Infinity.  */
    public const FLOOR = "floor";

    // 「もっとも近い数字」に丸める丸めモードです(ただし、両隣りの数字が等距離の場合は切り上げます)。
    // 破棄される小数部が0.5以上の場合はRoundingMode.UPのように、それ以外の場合はRoundingMode.DOWNのように動作します。これは我々の大半が小学校で習った丸めモードのことです。
    /** round towards the nearest neighbor, unless both neighbors are equidistant, in which case round away from zero. */
    public const HALF_UP = "half_up"; // HALF_AWAY_FROM_ZERO

    // 「もっとも近い数字」に丸める丸めモードです(両隣りの数字が等距離の場合は切り捨てます)。
    // 破棄される小数部が0.5を超える場合はRoundingMode.UPのように、それ以外の場合はRoundingMode.DOWNのように動作します。
    /** round towards the nearest neighbor, unless both neighbors are equidistant, in which case round towards zero. */
    public const HALF_DOWN = "half_down";

    // 「もっとも近い数字」に丸める丸めモードです(ただし、両隣りの数字が等距離の場合は偶数側に丸めます)。
    // 破棄する小数部の左側の桁が奇数の場合はRoundingMode.HALF_UPのように、偶数の場合はRoundingMode.HALF_DOWNのように動作します。
    // この丸めモードは、連続する計算で繰返し適用される場合に累積エラーを統計的に最小限にします。
    // これは「銀行方式の丸め」としても知られ、主に米国で使用されます。この丸めモードは、Javaのfloatおよびdouble算術演算で使用される丸め方針と同様です。
    /** round towards the nearest neighbor, unless both neighbors are equidistant, in which case round towards the even neighbor */
    public const HALF_EVEN = "half_even";

    // 「もっとも近い数字」に丸める丸めモードです(ただし、両隣りの数字が等距離の場合は奇数側に丸めます)。
    // 破棄する小数部の左側の桁が奇数の場合はRoundingMode.HALF_DOWNのように、偶数の場合はRoundingMode.HALF_UPのように動作します。
    // この丸めモードは、連続する計算で繰返し適用される場合に累積エラーを統計的に最小限にします。
    /** round towards the nearest neighbor, unless both neighbors are equidistant, in which case round towards the odd neighbor */
    public const HALF_ODD = "half_odd";

    // 結果が正の場合は HALF_UP、負の場合は HALF_DOWN と同じように動作します。
    /** round towards the nearest neighbor, unless both neighbors are equidistant, in which case round towards positive infinity. */
    public const HALF_CEILING = "half_ceiling";

    // 結果が正の場合は HALF_DOWN、負の場合は HALF_UP と同じように動作します。
    /** round towards the nearest neighbor, unless both neighbors are equidistant, in which case round towards negative infinity. */
    public const HALF_FLOOR = "half_floor";

    /** @var array<string, RoundingMode> */
    private static $instances = [];

    /** @var string */
    private $def;

    private function __construct(string $def)
    {
        $this->def = $def;
    }

    public function value(): string
    {
        return $this->def;
    }

    public function __toString()
    {
        return $this->value();
    }

    public static function UNNECESSARY(): self
    {
        $r = self::valueOf(self::UNNECESSARY);
        /** @var RoundingMode $r */
        return $r;
    }
    public static function CEILING(): self
    {
        $r = self::valueOf(self::CEILING);
        /** @var RoundingMode $r */
        return $r;
    }
    public static function DOWN(): self
    {
        $r = self::valueOf(self::DOWN);
        /** @var RoundingMode $r */
        return $r;
    }
    public static function UP(): self
    {
        $r = self::valueOf(self::UP);
        /** @var RoundingMode $r */
        return $r;
    }

    public static function FLOOR(): self
    {
        $r = self::valueOf(self::FLOOR);
        /** @var RoundingMode $r */
        return $r;
    }

    public static function HALF_UP(): self
    {
        $r = self::valueOf(self::HALF_UP);
        /** @var RoundingMode $r */
        return $r;
    }

    public static function HALF_DOWN(): self
    {
        $r = self::valueOf(self::HALF_DOWN);
        /** @var RoundingMode $r */
        return $r;
    }

    public static function HALF_EVEN(): self
    {
        $r = self::valueOf(self::HALF_EVEN);
        /** @var RoundingMode $r */
        return $r;
    }

    public static function HALF_ODD(): self
    {
        $r = self::valueOf(self::HALF_ODD);
        /** @var RoundingMode $r */
        return $r;
    }

    public static function HALF_CEILING(): self
    {
        $r = self::valueOf(self::HALF_CEILING);
        /** @var RoundingMode $r */
        return $r;
    }

    public static function HALF_FLOOR(): self
    {
        $r = self::valueOf(self::HALF_FLOOR);
        /** @var RoundingMode $r */
        return $r;
    }

    /**
     *
     * @param RoundingMode|string $mode
     * @return RoundingMode|null
     */
    public static function valueOf($mode): ?RoundingMode
    {

        if ($mode instanceof RoundingMode) {
            return $mode;
        }

        if (empty(self::$instances)) {
            $defs = [
                self::UNNECESSARY,
                self::CEILING,
                self::DOWN,
                self::UP,
                self::FLOOR,
                self::HALF_UP,
                self::HALF_DOWN,
                self::HALF_EVEN,
                self::HALF_ODD,
                self::HALF_CEILING,
                self::HALF_FLOOR,
            ];
            foreach ($defs as $def) {
                self::$instances[$def] = new static($def);
            }
        }
        return self::$instances[$mode] ?? null;
    }
}
