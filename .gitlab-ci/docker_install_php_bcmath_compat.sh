#!/bin/bash

# We need to install dependencies only for Docker
[[ ! -e /.dockerenv ]] && exit 0

set -eux

apt-get update -yqq

apt-get install -yqq \
    git \
    unzip

pecl install \
    xdebug

docker-php-ext-enable \
    xdebug

curl -sS https://getcomposer.org/installer | php

# php composer.phar update --prefer-dist
php composer.phar require phpseclib/bcmath_compat --prefer-dist

#
php -v
php -m
php composer.phar info
