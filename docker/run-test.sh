#!/usr/bin/env bash

set -ux;

SCRIPT_DIR=$(cd $(dirname $0); pwd)

function main(){

  pushd $SCRIPT_DIR

  docker compose up --remove-orphans
  docker compose ps --all

  popd

}

main "$@"
