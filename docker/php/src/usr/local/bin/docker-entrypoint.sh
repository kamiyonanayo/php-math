#!/bin/bash

set -eux;

function run_test(){
  exec composer run "${COMPOSER_RUN_CMD:-test-phpunit9}"
}

function install_composer(){
  mkdir /tmp/composer_vendor
  cp composer.json /tmp/composer_vendor/
  pushd /tmp/composer_vendor/
  composer update --prefer-dist
  popd
  mv /tmp/composer_vendor/vendor/* ./vendor/
}

function main(){

  if [ -z "$(ls -A ./vendor/)" ]; then
    install_composer
  fi

  php -v
  php -m
  composer info

  run_test
}

main "$@"
